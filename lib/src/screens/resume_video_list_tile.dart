import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class ResumeVideoListTile extends StatefulWidget {
  List<String> playList = [];
  final String question;
  final String subHeading;
  final Function onTap;
  YoutubePlayerController controller;
  ResumeVideoListTile(
      {this.subHeading,
      this.playList,
      this.question,
      this.onTap,
      this.controller});

  @override
  _ResumeVideoListTileState createState() => _ResumeVideoListTileState();
}

class _ResumeVideoListTileState extends State<ResumeVideoListTile> {
  @override
  void dispose() {
    widget.controller.pause();
    widget.controller.dispose();

    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 20)),
      child: Card(
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              child: Container(
                decoration: BoxDecoration(color: Colors.red),
                child: YoutubePlayer(
                  controller: widget.controller,
                  aspectRatio: 16 / 9,
                  bottomActions: [
                    CurrentPosition(
                      controller: widget.controller,
                    ),
                    ProgressBar(
                      controller: widget.controller,
                      isExpanded: true,
                    ),
                    PlaybackSpeedButton(
                      controller: widget.controller,
                    ),
                    RemainingDuration(
                      controller: widget.controller,
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: Column(
                  children: [
                    Text(
                      widget.question,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: screenWidth(context, dividedBy: 23),
                          fontFamily: 'SofiaProSemiBold'),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 150),
                    ),
                    GestureDetector(
                      onTap: () {
                        widget.onTap();
                      },
                      child: Text(
                        widget.subHeading,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: screenWidth(context, dividedBy: 25),
                            fontFamily: 'SofiaProMedium'),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            )
          ],
        ),
      ),
    );
  }
}
