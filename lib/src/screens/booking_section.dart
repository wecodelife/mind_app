import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_service_response.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/resume_preperation.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/booking_selectBox.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class BookingSection extends StatefulWidget {
  @override
  _BookingSectionState createState() => _BookingSectionState();
}

class _BookingSectionState extends State<BookingSection> {
  List<String> title = [
    "Mock Interview - Basic",
    "Mock Interview - Elite",
    "Resume Preparation",
    "Resume & Cover mail Review"
  ];
  UserBloc userBloc = UserBloc();

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  @override
  void initState() {
    // TODO: implement initState
    userBloc.getServices();

    userBloc.getServiceResponse.listen((event) {}).onError((event) {
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark
    ),
    child:WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            actions: [
              CAppBar(
                  title: "Booking",
                  color: Colors.white,
                  onPressedLeftIcon: () {
                    pushAndRemoveUntil(context, BottomBar(), false);
                  },
                  isWhite: false)
            ],
          ),
          // backgroundColor: Colors.amber,
          body: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              width: screenWidth(context, dividedBy: 1),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: screenHeight(context, dividedBy: 30)),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 1),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Text(
                        "Here are our special services to help you increase your chances of getting hired. Explore our services and book an enquiry today. Our service rep will contact you via WhatsApp to help you with more details",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: "SofiaProRegular"),
                      ),
                    ),
                  ),
                  SizedBox(height: screenHeight(context, dividedBy: 20)),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Text(
                      "SELECT A SECTION OF INTEREST",
                      style: TextStyle(
                          color: Color(0xff2C2E39),
                          fontFamily: "SofiaProRegular"),
                    ),
                  ),
                  SizedBox(height: screenHeight(context, dividedBy: 30)),
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    //height: screenHeight(context, dividedBy: 1),
                    //color: Colors.amber,
                    child: StreamBuilder<GetServicesResponse>(
                        stream: userBloc.getServiceResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasError
                              ? Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 2.2),
                                  child: Center(
                                      child: Row(
                                        mainAxisAlignment:MainAxisAlignment.center,
                                        crossAxisAlignment:CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                    "Try Again",
                                    style: TextStyle(
                                          fontSize: 16,
                                          fontWeight:FontWeight.w400,
                                          color: Colors.blueGrey,
                                    ),
                                  ),
                                          Icon(Icons.refresh,color: Colors.blueGrey,size:16)
                                        ],
                                      )),
                                )
                              : snapshot.hasData
                                  ? ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: snapshot.data.data.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            child: BookingSelectionBox(
                                                index: index,
                                                title: snapshot
                                                    .data.data[index].name,
                                                onSelected: () {
                                                  push(
                                                      context,
                                                      ResumePreparation(
                                                        serviceData: snapshot
                                                            .data.data[index],
                                                      ));
                                                }));
                                      },
                                    )
                                  : Container(
                                      height:
                                          screenHeight(context, dividedBy: 2),
                                      width: screenWidth(context, dividedBy: 1),
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  Constants.kitGradients[2]),
                                        ),
                                      ),
                                    );
                        }),
                  ),
                  SizedBox(height: screenHeight(context, dividedBy: 30)),
                ],
              ),
            ),
          ),
        ),
      ),
    ));
  }
}
