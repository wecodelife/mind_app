import 'package:app_template/Icons/mind_app_icons_icons.dart';
import 'package:app_template/src/screens/booking_section.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/job_alert_page.dart';
import 'package:app_template/src/screens/resources_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';

class BottomBar extends StatefulWidget {
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _currentIndex = 0;

  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _currentIndex = index);
          },
          children: <Widget>[
            HomePage(),
            BookingSection(),
            ResourcesPage(),
            JobAlertPage()
          ],
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() => _currentIndex = index);
          _pageController.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
              title: Text(
                '  Home',
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  color: Constants.kitGradients[2],
                ),
              ),
              activeColor: Constants.kitGradients[29],
              icon: Icon(
                MindAppIcons.home_outline,
                size: 20,
                color: Constants.kitGradients[2],
              )),
          BottomNavyBarItem(
              activeColor: Constants.kitGradients[29],
              title: Text(
                'Booking Page',
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  color: Constants.kitGradients[2],
                ),
              ),
              icon: Icon(
                Icons.people_alt_outlined,
                size: 24,
                color: Constants.kitGradients[2],
              )),
          BottomNavyBarItem(
              activeColor: Constants.kitGradients[29],
              title: Text(
                'Resources',
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  color: Constants.kitGradients[2],
                ),
              ),
              icon: Icon(
                MindAppIcons.book_open,
                size: 24,
                color: Constants.kitGradients[2],
              )),
          BottomNavyBarItem(
              activeColor: Constants.kitGradients[29],
              title: Text(
                'Job Alerts',
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  color: Constants.kitGradients[2],
                ),
              ),
              icon: Icon(
                Icons.card_travel,
                size: 24,
                color: Constants.kitGradients[2],
              )),
        ],
      ),
    );
  }
}
