import 'dart:async';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/book_service_request.dart';
import 'package:app_template/src/models/get_subscription_plans_response.dart';
import 'package:app_template/src/models/user_subscription_plan_request.dart';
import 'package:app_template/src/screens/booking_complete.dart';
import 'package:app_template/src/screens/booking_section.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/resume_preperation_page.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentPage extends StatefulWidget {
  // const PaymentPage({Key? key}) : super(key: key);
  final String url;
  final String orderId;
  final String firstName;
  final String lastName;
  final String whatsAppNumber;
  final DateTime bookingDate;
  final String bookingPrice;
  final String currentStatus;
  final List<int> serviceList;
  final List<String> compliments;
  final int subscriptionId;
  final bool successMessage;
  final bool subscription;
  int id;
  PaymentPage(
      {this.url,
      this.orderId,
      this.compliments,
      this.subscriptionId,
      this.successMessage,
      this.id,
      this.subscription = true,
        this.firstName,
        this.lastName,
        this.whatsAppNumber,
        this.bookingDate,
        this.currentStatus,
        this.serviceList,
        this.bookingPrice
      });

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  WebViewController _controller;
  UserBloc userBloc = UserBloc();
  bool isLoading = true;
  bool transactionMessage;
  GetSubscriptionPlansResponse subscriptionPlansResponse;
  String orderId;
  String status;
  // final Completer<WebViewController> _controller =
  // Completer<WebViewController>();

  Timer _timerControl;
  Timer _timerControl2;
  Timer _timer;
  int _start = 45;
  void startTimer() {
    const oneSec = const Duration(seconds: 5);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start = _start-1;
            print("Timer :" + _start.toString());
          });
          userBloc.getOrderPayments(orderId: widget.orderId);
        }
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  // void startTimer() {
  //   _timerControl = Timer.periodic(Duration(minutes: 3), (timer) {
  //     _timerControl.cancel();
  //     print("Timer Section 1");
  //
  //     _timerControl2 = Timer.periodic(Duration(seconds: 5), (timer) {
  //       _timerControl2.cancel();
  //       print("Timer Section 2");
  //       // print("Timer :" + _timerControl.toString());
  //       userBloc.getOrderPayments(orderId: widget.orderId);
  //       userBloc.getOrderPaymentResponse.listen((event) {
  //         if (event.status == 200 || event.status == 201) {
  //           print("Order Payment Responses");
  //           print("Order Payment Responses Successful");
  //           ObjectFactory().appHive.getOrderStatus() == false
  //               ? ObjectFactory().appHive.putOrderStatus(status: true)
  //               : print(ObjectFactory().appHive.getOrderStatus().toString());
  //           userBloc.userSubscriptionPlans(
  //               userSubscriptionRequest: UserSubscriptionRequest(
  //             isActive: true,
  //             orderId: widget.orderId,
  //             status: event.data.txStatus,
  //             compliments: widget.compliments,
  //             subscription: widget.subscriptionId,
  //             user: ObjectFactory().appHive.getId(),
  //           ));
  //           // if (event.data.txStatus == "SUCCESS" ||
  //           //     event.data.orderStatus == "PAID") {
  //           //   print("Order Payment Responses Successful");
  //           //   ObjectFactory().appHive.getOrderStatus() == false
  //           //       ? ObjectFactory().appHive.putOrderStatus(status: true)
  //           //       : print(ObjectFactory().appHive.getOrderStatus().toString());
  //           //   userBloc.userSubscriptionPlans(
  //           //       userSubscriptionRequest: UserSubscriptionRequest(
  //           //         isActive: true,
  //           //         orderId: widget.orderId,
  //           //         status: event.data.txStatus,
  //           //         compliments: widget.compliments,
  //           //         subscription: widget.subscriptionId,
  //           //         user: ObjectFactory().appHive.getId(),
  //           //       ));
  //           // } else {
  //           //   print("Successful condition not satisfied");
  //           // }
  //         }
  //       });
  //     });
  //   });
  // }

  // void statusCheck() {
  //   while (status == "SUCCESS") {
  //     print("While loop");
  //     userBloc.getOrderPayments(orderId: widget.orderId);
  //     userBloc.getOrderPaymentResponse.listen((event) {
  //       if (event.status == 200 || event.status == 201) {
  //         print("Order Payment Responses");
  //         print("Order Payment Responses Successful");
  //         // ObjectFactory().appHive.getOrderStatus() == false
  //         //     ? ObjectFactory().appHive.putOrderStatus(status: true)
  //         //     : print(ObjectFactory().appHive.getOrderStatus().toString());
  //         // userBloc.userSubscriptionPlans(
  //         //     userSubscriptionRequest: UserSubscriptionRequest(
  //         //       isActive: true,
  //         //       orderId: widget.orderId,
  //         //       status: event.data.txStatus,
  //         //       compliments: widget.compliments,
  //         //       subscription: widget.subscriptionId,
  //         //       user: ObjectFactory().appHive.getId(),
  //         //     ));
  //         if (event.data.txStatus == "SUCCESS" ||
  //             event.data.orderStatus == "PAID") {
  //           print("Order Payment Responses Successful");
  //           setState(() {
  //             status = event.data.txStatus;
  //             print(status);
  //           });
  //           ObjectFactory().appHive.getOrderStatus() == false
  //               ? ObjectFactory().appHive.putOrderStatus(status: true)
  //               : print(ObjectFactory().appHive.getOrderStatus().toString());
  //           userBloc.userSubscriptionPlans(
  //               userSubscriptionRequest: UserSubscriptionRequest(
  //             isActive: true,
  //             orderId: widget.orderId,
  //             status: event.data.txStatus,
  //             compliments: widget.compliments,
  //             subscription: widget.subscriptionId,
  //             user: ObjectFactory().appHive.getId(),
  //           ));
  //         } else {
  //           print("Successful condition not satisfied");
  //         }
  //       }
  //     });
  //   }
  // }

  @override
  void initState() {
    // TODO: implement initState
    startTimer();
    // statusCheck();
    print("orderId  " + widget.orderId);
    print("Compliments  " + widget.compliments.toString());
    print("Subscription Id  " + widget.subscriptionId.toString());
    print("User Id  " + ObjectFactory().appHive.getId().toString());
    print("Payment response session");
    userBloc.userSubscriptionPlan.listen((event) {
      if (event.status == 200 || event.status == 201) {
        print("Payment Successful");
        print(event.data.status);
        setState(() {

          ObjectFactory().appHive.putOrderStatus(status: true);
        });
        push(context, BottomBar());
        // ObjectFactory().appHive.putOrderStatus(status: true);
        // ObjectFactory().appHive.getOrderStatus() == false ? ObjectFactory().appHive.putOrderStatus(status: true) : ObjectFactory().appHive.putOrderStatus(status: true);
      }
    });

    userBloc.bookServiceResponse.listen((event) {
      if(event.status == 200 || event.status == 201) {
        print("Booking response successful");
        setState(() {
          isLoading = false;
        });
        push(context, BookingCompletePage());
        print("booked");
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event);
    });


    userBloc.getOrderPaymentResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        print("Order Payment Responses");
        print("Order Payment Responses Successful");
        if (event.data.txStatus == "SUCCESS" ||
            event.data.orderStatus == "PAID") {
          print("Success Order Payment ");
          widget.subscription == false ?
             userBloc.bookService(
                 bookServiceRequest: BookServiceRequest(
                     bookingDate: widget.bookingDate,
                     bookingPrice: widget.bookingPrice,
                     currentStatus: widget.currentStatus,
                     firstName: widget.firstName,
                     lastName: widget.lastName,
                     whatsappNumber: widget.whatsAppNumber,
                     service: widget.serviceList))
          :
             userBloc.userSubscriptionPlans(
                 userSubscriptionRequest: UserSubscriptionRequest(
                   isActive: true,
                   orderId: widget.orderId,
                   status: "Success",
                   compliments: widget.compliments,
                   subscription: widget.subscriptionId,
                   user: ObjectFactory().appHive.getId(),
                 ));


          setState(() {
            // status = "SUCCESS";
            print(status);
            _start = 0;
          });
          showToast("Your payment is Successful");

        } else if (event.data.txStatus == "FAILED") {
          setState(() {
            // status = event.data.txStatus;
            print(status);
            _start = 0;
          });
          showToast("Your payment is not Successful, Please try again");
          widget.subscription == false ? push(context, BookingSection()) : push(context, BottomBar());
        } else {
          print("Payment condition not satisfied");

        }
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading == false
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                heightFactor: 1,
                widthFactor: 1,
                child: SizedBox(
                  height: 16,
                  width: 16,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        Constants.kitGradients[2].withOpacity(0.4)),
                    strokeWidth: 2,
                  ),
                ),
              ),
            )
          : Container(
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        top: screenHeight(context, dividedBy: 20)),
                    child: WebView(
                        initialUrl: widget.url,
                        javascriptMode: JavascriptMode.unrestricted,
                        onWebViewCreated: (controller) {
                          setState(() {
                            _controller = controller;
                          });
                        },
                        onPageFinished: (url) {
                          // _controller.clearCache();
                          // if (status == "SUCCESS") {
                          //   print("Success, User subscription request :" );
                          //   userBloc.userSubscriptionPlans(
                          //       userSubscriptionRequest: UserSubscriptionRequest(
                          //         isActive: true,
                          //         orderId: widget.orderId,
                          //         status: "Success",
                          //         compliments: widget.compliments,
                          //         subscription: widget.subscriptionId,
                          //         user: ObjectFactory().appHive.getId(),
                          //       ));
                          // }
                        }

                        // userBloc.userSubscriptionPlans(
                        //     userSubscriptionRequest: UserSubscriptionRequest(
                        //       isActive: true,
                        //       orderId: widget.orderId,
                        //       status: "Success",
                        //       compliments: widget.compliments,
                        //       subscription: widget.subscriptionId,
                        //       user: ObjectFactory().appHive.getId(),
                        //
                        //
                        // // if (url ==
                        // //     "https://test.cashfree.com/billpay/sim/thankyou/") {
                        // //   print("Success " + url);
                        // //   userBloc.userSubscriptionPlans(
                        // //       userSubscriptionRequest: UserSubscriptionRequest(
                        // //     isActive: true,
                        // //     orderId: widget.orderId,
                        // //     status: "Success",
                        // //     compliments: widget.compliments,
                        // //     subscription: widget.subscriptionId,
                        // //     user: ObjectFactory().appHive.getId(),
                        //   ));
                        // }
                        // else if( url == "https://test.cashfree.com/billpay/sim/thankyou/" ){
                        //   showToast("Your payment is Unsuccessful, please try again");
                        //   push(context, HomePage());
                        // }
                        ),
                  ),
                  // Align(
                  //   alignment: Alignment.bottomCenter,
                  //   child: Container(
                  //     padding: EdgeInsets.symmetric(horizontal: 15),
                  //     margin: EdgeInsets.symmetric(vertical: 15),
                  //     width: screenWidth(context, dividedBy: 1),
                  //     height: screenHeight(context, dividedBy: 10),
                  //     child: CButton(
                  //       title: "Go Home",
                  //       onPressed: () {
                  //         transactionMessage == true
                  //             ? showToast("Payment Successful")
                  //             : push(context, HomePage());
                  //       },
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
    );
  }
}
