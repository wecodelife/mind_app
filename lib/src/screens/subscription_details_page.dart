import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/screens/payment_page.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/alert_box_list_tile.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_template/src/models/get_subscription_plans_response.dart';
import 'package:app_template/src/models/order_plan_request_model.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:flutter/services.dart';

class SubscriptionDetailsPage extends StatefulWidget {
  // final int currentIndex;
  // SubscriptionDetailsPage({this.currrentIndex});
  final int currentIndex;
  SubscriptionDetailsPage({this.currentIndex});
  @override
  _SubscriptionDetailsPageState createState() =>
      _SubscriptionDetailsPageState();
}

class _SubscriptionDetailsPageState extends State<SubscriptionDetailsPage> {
  int selectedItem = 1;
  UserBloc userBloc = UserBloc();
  bool isLoading = true;
  List<String> sliderImages = [];
  // List<Datum> data;
  String orderId;
  bool email;
  bool phoneNumber;
  List<String> compliment = [];
  int subscriptionId;
  TextEditingController dataTextEditingController = TextEditingController();
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  @override
  void initState() {
    setState(() {
      email = ObjectFactory().appHive.getEmail() == null ? true : false;
      phoneNumber =
      ObjectFactory().appHive.getPhoneNumber() == null ? true : false;
    });
    ObjectFactory().appHive.getEmail() == null
        ? print("Email Id No value ")
        : print("Email id " + ObjectFactory().appHive.getEmail());
    print(
        "Phone Number  " + ObjectFactory().appHive.getPhoneNumber().toString());

    userBloc.getSubscriptionPlan();
    userBloc.getSubscriptionPlans.listen((event) {
      if (event.status == 200 || event.status == 201) {
        print("Subscription Plans loaded Successfully");
        setState(() {
          // compliment = event.data[widget.currentIndex].compliments;
          subscriptionId = event.data[widget.currentIndex].id;
          print("Compliments " + compliment.toString());
          print("Subscription plan ID  " + subscriptionId.toString());
          print(event.data[widget.currentIndex].title);
          for(int i=0;i<event.data[widget.currentIndex].packageInclude.data.length;i++){
            print("Detailsss "
            );
            print(event.data[widget.currentIndex].packageInclude.data[i].details);
          }

        });
        print(event.data);
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });

    userBloc.orderPlanResponse.listen((event) async {
      if (event.status == 200 || event.status == 201) {
        print("Subscription order response loaded Successfully");
        print("Payment Link  " + event.data.paymentLink);
        print("OrderId  " + event.data.orderId);
        setState(() {
          orderId = event.data.orderId;
        });
        if (event.data.paymentLink != null) {
          push(
              context,
              PaymentPage(
                url: event.data.paymentLink,
                orderId: event.data.orderId,
                compliments: compliment,
                subscriptionId: subscriptionId,
              ));
        }
        //
        // if (await canLaunch(event.data.paymentLink)) {
        //   await launch(event.data.paymentLink);
        //   print("Payment Link loaded Successfully");
        // } else {
        //   throw 'Could not launch ' + event.data.paymentLink;
        // }
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark),
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Constants.kitGradients[0],
            appBar: AppBar(
              backgroundColor: Colors.white,
              toolbarHeight: screenHeight(context, dividedBy: 15),
              elevation: 0,
              leading: Container(),
              actions: [
                Column(
                  children: [
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 100),
                    // ),
                    CAppBar(
                      title: "Subscription Details",
                      color: Colors.white,
                      isWhite: false,
                      onPressedLeftIcon: () {
                        pop(context);
                      },
                    ),
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 150),
                    // ),
                  ],
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  // mainAxisSize:MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Container(
                    //   width: screenWidth(context, dividedBy: 1),
                    //   child: StreamBuilder<GetSubscriptionPlansResponse>(
                    //     stream: userBloc.getSubscriptionPlans,
                    //     builder: (context, snapshot) {
                    //       return snapshot.hasData
                    //           ? Column(
                    //               crossAxisAlignment: CrossAxisAlignment.start,
                    //               children: [
                    //                 SizedBox(
                    //                   height:
                    //                       screenHeight(context, dividedBy: 20),
                    //                 ),
                    //                 // Container(
                    //                 //   width: screenWidth(context, dividedBy: 1),
                    //                 //   height: screenHeight(context, dividedBy: 10),
                    //                 //   alignment: Alignment.center,
                    //                 //   child: ListView.builder(
                    //                 //     scrollDirection: Axis.horizontal,
                    //                 //     itemCount: snapshot.data.data.length,
                    //                 //     shrinkWrap: true,
                    //                 //     itemBuilder: (BuildContext context, int index) {
                    //                 //       return Column(
                    //                 //         children: [
                    //                 //           Container(
                    //                 //             child: Expanded(
                    //                 //               child: Row(
                    //                 //                 mainAxisAlignment: MainAxisAlignment.center,
                    //                 //                 crossAxisAlignment:
                    //                 //                 CrossAxisAlignment.center,
                    //                 //                 children: [
                    //                 //                   // Spacer(),
                    //                 //                   SizedBox(
                    //                 //                       width: screenWidth(context,
                    //                 //                           dividedBy: 90)),
                    //                 //                   SubscriptionTabHeader(
                    //                 //                     label: snapshot
                    //                 //                         .data.data[index].offerPrice
                    //                 //                         .toString(),
                    //                 //                     indexValue: index,
                    //                 //                     selectedItem: selectedItem,
                    //                 //                     onSelected: () {
                    //                 //                       setState(() {
                    //                 //                         selectedItem = index;
                    //                 //                       });
                    //                 //                     },
                    //                 //                   ),
                    //                 //                   SizedBox(
                    //                 //                       width: screenWidth(context,
                    //                 //                           dividedBy: 90)),
                    //                 //                   // Spacer(),
                    //                 //                 ],
                    //                 //               ),
                    //                 //             ),
                    //                 //           ),
                    //                 //         ],
                    //                 //       );
                    //                 //     },
                    //                 //   ),
                    //                 // ),
                    //               ],
                    //             )
                    //           : Container();
                    //     },
                    //   ),
                    // ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 120),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30),
                        // vertical: screenHeight(context, dividedBy: 50)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: StreamBuilder<GetSubscriptionPlansResponse>(
                              stream: userBloc.getSubscriptionPlans,
                              builder: (context, snapshot) {
                                return snapshot.hasData
                                    ? Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 50),
                                          ),
                                          Row(children: [
                                            Text(
                                              "Rs."+snapshot
                                                  .data.data[selectedItem].price
                                                  .toString(),
                                              style: TextStyle(
                                                  fontFamily: "SofiaProRegular",
                                                  fontSize: 18,
                                                  color: Colors.red,
                                                  fontWeight: FontWeight.w600,
                                                  decoration: TextDecoration
                                                      .lineThrough,
                                                  decorationStyle:
                                                      TextDecorationStyle
                                                          .solid),
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 40),
                                            ),
                                            Text(
                                              "Rs."+snapshot.data.data[widget.currentIndex]
                                                      .offerPrice
                                                      .toString() ,
                                              style: TextStyle(
                                                  fontFamily: "SofiaProRegular",
                                                  fontSize: 26,
                                                  color: Colors.green,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(top:screenHeight(context,
                                                  dividedBy: 90),),
                                              child: Text( "/year",
                                                style: TextStyle(
                                                    fontFamily: "SofiaProRegular",
                                                    fontSize: 15,
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w500),),
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 40),
                                            ),
                                          ]),

                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 60),
                                          ),
                                          Text(
                                snapshot
                                    .data.data[widget.currentIndex].packageInclude.description,
                                            style: TextStyle(
                                                fontFamily: "SofiaProRegular",
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 30),
                                          ),
                                          Text(
                                            "Things we offer in this package :",
                                            style: TextStyle(
                                                fontFamily: "SofiaProRegular",
                                                fontSize: 17,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 30),
                                          ),
                                          Container(
                                            child: ListView.builder(
                                              itemCount: snapshot
                                                  .data
                                                  .data[widget.currentIndex]
                                                  .packageInclude
                                                  .data.length,
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return snapshot
                                                            .data
                                                            .data[widget
                                                                .currentIndex]
                                                            .packageInclude
                                                            .data.length >
                                                        0
                                                    ? Column(
                                                        children: [
                                                          Row(
                                                            children: [
                                                              Text(
                                                                snapshot
                                                                    .data.data[widget.currentIndex].packageInclude.data[index].title == null ? "No data available" : snapshot
                                                                    .data.data[widget.currentIndex].packageInclude.data[index].title,
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                    "SofiaProRegular",
                                                                    fontSize: 17,
                                                                    fontWeight:
                                                                    FontWeight.w700),
                                                              ),
                                                            ],
                                                          ),
                                                          SizedBox(
                                                            height: screenHeight(context,
                                                                dividedBy: 70),
                                                          ),
                                                          AlertBoxListTile(
                                                            icon: Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .only(
                                                                left: screenWidth(
                                                                    context,
                                                                    dividedBy:
                                                                        20),
                                                                top: screenWidth(
                                                                    context,
                                                                    dividedBy:
                                                                        90),
                                                              ),
                                                              child: Container(
                                                                  width: screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          80),
                                                                  height: screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          40),
                                                                  decoration: BoxDecoration(
                                                                      color:
                                                                          Constants.kitGradients[
                                                                              7],
                                                                      shape: BoxShape
                                                                          .circle)),
                                                            ),
                                                            data: snapshot.data.data[widget.currentIndex].packageInclude.data[index].details == null ? "No data available" : snapshot.data.data[widget.currentIndex].packageInclude.data[index].details,
                                                          ),
                                                          SizedBox(
                                                            height:
                                                                screenHeight(
                                                                    context,
                                                                    dividedBy:
                                                                        80),
                                                          ),
                                                        ],
                                                      )
                                                    : Container(
                                                        child: Center(
                                                          heightFactor: 1,
                                                          widthFactor: 1,
                                                          child: SizedBox(
                                                            height: 16,
                                                            width: 16,
                                                            child:
                                                                CircularProgressIndicator(
                                                              valueColor: AlwaysStoppedAnimation(
                                                                  Constants
                                                                      .kitGradients[
                                                                          2]
                                                                      .withOpacity(
                                                                          0.4)),
                                                              strokeWidth: 2,
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                              },
                                            ),
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 30),
                                          ),

                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 40),
                                          ),
                                          Text(
                                            "Compliments:",
                                            style: TextStyle(
                                                fontFamily: "SofiaProRegular",
                                                fontSize: 17,
                                                color:
                                                    Constants.kitGradients[2],
                                                fontWeight: FontWeight.w700),
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 90),
                                          ),
                                          Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: screenWidth(
                                                      context,
                                                      dividedBy: 30),
                                                  vertical: screenHeight(
                                                      context,
                                                      dividedBy: 50)),
                                              decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                color: Constants.kitGradients[2]
                                                    .withOpacity(0.13),
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              child: Column(
                                                children: [
                                                  ListView.builder(
                                                    itemCount: snapshot
                                                        .data
                                                        .data[
                                                            widget.currentIndex]
                                                        .compliments
                                                        .data.length,
                                                    shrinkWrap: true,
                                                    physics:
                                                        NeverScrollableScrollPhysics(),
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return snapshot
                                                                  .data
                                                                  .data[widget
                                                                      .currentIndex]
                                                                  .compliments
                                                                  .data.length >
                                                              0
                                                          ? Column(
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Text(
                                                                      snapshot
                                                                          .data.data[widget.currentIndex].compliments.data[index].title == null ? "No data available" : snapshot
                                                                          .data.data[widget.currentIndex].compliments.data[index].title,
                                                                      style: TextStyle(
                                                                          fontFamily:
                                                                          "SofiaProRegular",
                                                                          fontSize: 15,
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .w700),
                                                                    ),
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  height: screenHeight(
                                                                      context,
                                                                      dividedBy: 70),
                                                                ),
                                                                AlertBoxListTile(
                                                                  icon:    Padding(
                                                                    padding:
                                                                    EdgeInsets
                                                                        .only(
                                                                      left: screenWidth(
                                                                          context,
                                                                          dividedBy:
                                                                          20),
                                                                      top: screenWidth(
                                                                          context,
                                                                          dividedBy:
                                                                          40),
                                                                    ),
                                                                    child:
                                                                    Container(
                                                                      width:
                                                                      4,
                                                                      height:
                                                                      4,
                                                                      decoration:
                                                                      BoxDecoration(
                                                                        shape:
                                                                        BoxShape.circle,
                                                                        color:
                                                                        Constants.kitGradients[7],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  data: snapshot.data.data[widget.currentIndex].compliments.data[index].details == null ? "No data available" : snapshot.data.data[widget.currentIndex].compliments.data[index].details,
                                                                ),
                                                                // Row(
                                                                //   children: [
                                                                //     Padding(
                                                                //       padding:
                                                                //           EdgeInsets
                                                                //               .only(
                                                                //         left: screenWidth(
                                                                //             context,
                                                                //             dividedBy:
                                                                //                 20),
                                                                //         top: screenWidth(
                                                                //             context,
                                                                //             dividedBy:
                                                                //                 90),
                                                                //       ),
                                                                //       child:
                                                                //           Container(
                                                                //         width:
                                                                //             4,
                                                                //         height:
                                                                //             4,
                                                                //         decoration:
                                                                //             BoxDecoration(
                                                                //           shape:
                                                                //               BoxShape.circle,
                                                                //           color:
                                                                //               Constants.kitGradients[7],
                                                                //         ),
                                                                //       ),
                                                                //     ),
                                                                //     SizedBox(
                                                                //       width: screenWidth(
                                                                //           context,
                                                                //           dividedBy:
                                                                //               40),
                                                                //     ),
                                                                //     Expanded(
                                                                //         child:
                                                                //             Text(
                                                                //       snapshot
                                                                //           .data
                                                                //           .data[
                                                                //               widget.currentIndex]
                                                                //           .compliments.data[index].title == null ? "No data available" :snapshot
                                                                //           .data
                                                                //           .data[
                                                                //       widget.currentIndex]
                                                                //           .compliments.data[index].title,
                                                                //       style: TextStyle(
                                                                //           fontFamily:
                                                                //               "SofiaProRegular",
                                                                //           fontSize:
                                                                //               14,
                                                                //           fontWeight:
                                                                //               FontWeight.w500),
                                                                //     ))
                                                                //   ],
                                                                // ),
                                                                SizedBox(
                                                                  height: screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          120),
                                                                ),
                                                              ],
                                                            )
                                                          : Container(
                                                              child: Center(
                                                                heightFactor: 1,
                                                                widthFactor: 1,
                                                                child: SizedBox(
                                                                  height: 16,
                                                                  width: 16,
                                                                  child:
                                                                      CircularProgressIndicator(
                                                                    valueColor: AlwaysStoppedAnimation(Constants
                                                                        .kitGradients[
                                                                            2]
                                                                        .withOpacity(
                                                                            0.4)),
                                                                    strokeWidth:
                                                                        2,
                                                                  ),
                                                                ),
                                                              ),
                                                            );
                                                    },
                                                  ),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 40),
                                                  ),
                                                  // Row(
                                                  //   children: [
                                                  //     Text(
                                                  //       "2. ATS-resume Template Bundle",
                                                  //       style: TextStyle(
                                                  //           fontFamily:
                                                  //               "SofiaProRegular",
                                                  //           fontSize: 15,
                                                  //           fontWeight:
                                                  //               FontWeight
                                                  //                   .w700),
                                                  //     ),
                                                  //   ],
                                                  // ),
                                                  // SizedBox(
                                                  //   height: screenHeight(
                                                  //       context,
                                                  //       dividedBy: 70),
                                                  // ),
                                                ],
                                              )),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 50),
                                          ),
                                          Text(
                                            "Notes",
                                            style: TextStyle(
                                                fontFamily: "SofiaProRegular",
                                                fontSize: 15,
                                                color: Colors.green,
                                                fontWeight: FontWeight.w700),
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 130),
                                          ),
                                          ListView.builder(
                                              itemCount: snapshot
                                                  .data
                                                  .data[
                                              widget.currentIndex]
                                                  .compliments
                                                  .notes.length,
                                              shrinkWrap: true,
                                              physics:
                                              NeverScrollableScrollPhysics(),
                                            itemBuilder:(BuildContext context, int index){
                                              return   snapshot
                                                  .data
                                                  .data[widget
                                                  .currentIndex]
                                                  .compliments
                                                  .notes.length >
                                                  0
                                                  ? Row(
                                                mainAxisAlignment:MainAxisAlignment.start,
                                                crossAxisAlignment:CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                      left: screenWidth(context,
                                                          dividedBy: 20),
                                                      top: screenWidth(context,
                                                          dividedBy: 60),
                                                    ),
                                                    child: Container(
                                                      width: 4,
                                                      height: 4,
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Colors.green,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: screenWidth(context,
                                                        dividedBy: 40),
                                                  ),

                                                  Expanded(
                                                      child: Text(
                                                  snapshot.data.data[widget.currentIndex].compliments.notes[index] == null ? "No data available" : snapshot.data.data[widget.currentIndex].compliments.notes[index]
                                                        ,
                                                        style: TextStyle(
                                                            fontFamily:
                                                            "SofiaProRegular",
                                                            fontSize: 14,
                                                            color: Colors.green,
                                                            fontWeight:
                                                            FontWeight.w500),
                                                      ))
                                                ],
                                              ):
                                              Container(
                                                child: Center(
                                                  heightFactor: 1,
                                                  widthFactor: 1,
                                                  child: SizedBox(
                                                    height: 16,
                                                    width: 16,
                                                    child:
                                                    CircularProgressIndicator(
                                                      valueColor: AlwaysStoppedAnimation(Constants
                                                          .kitGradients[
                                                      2]
                                                          .withOpacity(
                                                          0.4)),
                                                      strokeWidth:
                                                      2,
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }
                                          ),

                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 50),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 15),
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            height: screenHeight(context,
                                                dividedBy: 10),
                                            child: CButton(
                                              title: "Subscribe Now",
                                              onPressed: () async {
                                                if (email == true ||
                                                    phoneNumber == true) {
                                                  showAddEmailAlert(
                                                    context,
                                                    isEmail: email == true
                                                        ? true
                                                        : false,
                                                    textEditingController:
                                                    dataTextEditingController,
                                                    onPressed: () {
                                                      phoneNumber == true
                                                          ? ObjectFactory()
                                                          .appHive
                                                          .putPhoneNumber(
                                                          phoneNumber:
                                                          dataTextEditingController
                                                              .text)
                                                          : email == true
                                                          ? ObjectFactory()
                                                          .appHive
                                                          .putEmail(
                                                          dataTextEditingController
                                                              .text)
                                                          : setState(() {});
                                                      email == true
                                                          ? print(
                                                          "Email missing, added to hive " +
                                                              ObjectFactory()
                                                                  .appHive
                                                                  .getEmail())
                                                          : print(
                                                          "Email is not missing");
                                                      phoneNumber == true
                                                          ? print("PhoneNumber missing, added to hive " +
                                                          ObjectFactory()
                                                              .appHive
                                                              .getPhoneNumber())
                                                          : print(
                                                          "Phone Number is not missing");

                                                      userBloc.orderPlans(
                                                          orderPlanRequest: OrderPlanRequest(
                                                              customerEmail:
                                                              ObjectFactory()
                                                                  .appHive
                                                                  .getEmail(),
                                                              customerPhone:
                                                              ObjectFactory()
                                                                  .appHive
                                                                  .getPhoneNumber()
                                                                  .toString(),
                                                              customerName:
                                                              ObjectFactory()
                                                                  .appHive
                                                                  .getName(),
                                                              orderAmount: snapshot
                                                                  .data
                                                                  .data[
                                                              selectedItem]
                                                                  .offerPrice
                                                                  .toString(),
                                                              orderCurrency:
                                                              "INR",
                                                              orderNote:
                                                              "test"));
                                                    },
                                                  );
                                                } else if (email != true &&
                                                    phoneNumber != true) {
                                                  print("Subscription ID of " +
                                                      selectedItem.toString() +
                                                      " is " +
                                                      snapshot.data
                                                          .data[selectedItem].id
                                                          .toString());
                                                  print(
                                                      "___________________________________________");
                                                  userBloc.orderPlans(
                                                      orderPlanRequest: OrderPlanRequest(
                                                          customerEmail: ObjectFactory()
                                                              .appHive
                                                              .getEmail() ==
                                                              null
                                                              ? dataTextEditingController
                                                              .text
                                                              : ObjectFactory()
                                                              .appHive
                                                              .getEmail(),
                                                          customerPhone: ObjectFactory()
                                                              .appHive
                                                              .getPhoneNumber() ==
                                                              null
                                                              ? dataTextEditingController
                                                              .text
                                                              : ObjectFactory()
                                                              .appHive
                                                              .getPhoneNumber()
                                                              .toString(),
                                                          customerName:
                                                          ObjectFactory()
                                                              .appHive
                                                              .getName(),
                                                          orderAmount: snapshot
                                                              .data
                                                              .data[
                                                          selectedItem]
                                                              .offerPrice
                                                              .toString(),
                                                          orderCurrency: "INR",
                                                          orderNote: "test"));
                                                }

                                                // if (email == true ||
                                                //     phoneNumber == true) {
                                                //   showAddEmailAlert(context,
                                                //       isEmail:
                                                //           email == true ? true : false,
                                                //       textEditingController:
                                                //           dataTextEditingController,
                                                //       onPressed: () {
                                                //     phoneNumber == true
                                                //         ? ObjectFactory()
                                                //             .appHive
                                                //             .putPhoneNumber(
                                                //                 phoneNumber: int.parse(
                                                //                     dataTextEditingController
                                                //                         .text))
                                                //         : setState(() {});
                                                //     print("Subscription ID of " +
                                                //         selectedItem.toString() +
                                                //         " is " +
                                                //         snapshot
                                                //             .data.data[selectedItem].id
                                                //             .toString());
                                                //     userBloc.orderPlans(
                                                //         orderPlanRequest: OrderPlanRequest(
                                                //             customerEmail: ObjectFactory()
                                                //                         .appHive
                                                //                         .getEmail() ==
                                                //                     null
                                                //                 ? dataTextEditingController
                                                //                     .text
                                                //                 : ObjectFactory()
                                                //                     .appHive
                                                //                     .getEmail(),
                                                //             customerPhone: ObjectFactory()
                                                //                         .appHive
                                                //                         .getPhoneNumber() ==
                                                //                     null
                                                //                 ? dataTextEditingController
                                                //                     .text
                                                //                 : ObjectFactory()
                                                //                     .appHive
                                                //                     .getPhoneNumber()
                                                //                     .toString(),
                                                //             customerName: ObjectFactory()
                                                //                 .appHive
                                                //                 .getName(),
                                                //             orderAmount: snapshot
                                                //                 .data
                                                //                 .data[selectedItem]
                                                //                 .offerPrice
                                                //                 .toString(),
                                                //             orderCurrency: "INR",
                                                //             orderNote: "test"));
                                                //   });
                                                //   print("Add data");
                                                // }
                                                // pop(context);
                                              },
                                            ),
                                          ),
                                        ],
                                      )
                                    : Container(
                                        width:
                                            screenWidth(context, dividedBy: 1),
                                        height:
                                            screenHeight(context, dividedBy: 2),
                                        child: Center(
                                          heightFactor: 1,
                                          widthFactor: 1,
                                          child: SizedBox(
                                            height: 16,
                                            width: 16,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Constants.kitGradients[2]
                                                          .withOpacity(0.4)),
                                              strokeWidth: 2,
                                            ),
                                          ),
                                        ),
                                      );
                              },
                            ),
                          ),

                          SizedBox(
                            height: screenHeight(context, dividedBy: 70),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
