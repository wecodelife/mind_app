import 'package:app_template/src/screens/onboarding_screen2.dart';
import 'package:app_template/src/screens/slide_transition.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swipedetector/swipedetector.dart';

class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Color(0xffD9E7FD),
    ),
    child: DoubleBack(
      message: "Press back again to quit",
      child: SafeArea(
        child: SwipeDetector(
          onSwipeLeft: () {
            Navigator.of(context)
                .push(SlideRightRoute(page: OnBoardingScreen2()));
          },
          child: Scaffold(
            // extendBodyBehindAppBar: true,
            // backgroundColor: Colors.amber,
            body: Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: OnBoardingContent(
                head: "Supreme Bundle!",
                data:
                    "Get access to Fully Packed curated video & text resources to "
                    "prepare for aptitude, Resume, Linkedin, GD and Interviews.",
                image: "assets/images/on_boarding1.svg",
                buttonData: "Next",
                index: 0,
                onPressed: () {
                  Navigator.of(context)
                      .push(SlideRightRoute(page: OnBoardingScreen2()));
                  //Navigator.push(context, SlideRightRoute(page: OnBoardingScreen2()));
                  //push(context, OnBoardingScreen2());
                },
              ),
            ),
          ),
        ),
      ),
    ));
  }
}
