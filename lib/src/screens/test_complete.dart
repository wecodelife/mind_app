import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_result_response_model.dart';
import 'package:app_template/src/screens/apptitude.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/widgets/app_bar.dart';

class TestCompletePage extends StatefulWidget {
  @override
  _TestCompletePageState createState() => _TestCompletePageState();
}

class _TestCompletePageState extends State<TestCompletePage> {
  UserBloc userBloc = new UserBloc();
  int questionCount;
  int correctAnswerCount;
  int examCompletedId;
  Future<bool> _willPopCallback() async {
    pushAndReplacement(context, ApptitudePage());
    return true;
  }

  @override
  void initState() {
    // TODO: implement initState
    userBloc.getResult();
    userBloc.getResultResponse.listen((event) {
      if (event.status == 200) {
        correctAnswerCount = event.data.answerCount;
        questionCount = event.data.qaCount;
      } else {
        showToast("Network Error");
      }
    }).onError((event) {
      // showToast(event.toString());
    });
    // userBloc.getResultResponse.listen((event) {
    //   if (event.status == 200) {
    //     // ObjectFactory().appHive.putExamCompletedStatus(
    //     //     ObjectFactory().appHive.getExamCompletedStatus() + 1);
    //
    //     print(ObjectFactory().appHive.getExamCompletedStatus());
    //   }
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Color(0xffD9E7FD),
          statusBarIconBrightness: Brightness.dark),
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              toolbarHeight: screenHeight(context, dividedBy: 14),
              elevation: 0,
              leading: Container(),
              actions: [
                Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 150),
                    ),
                    CAppBar(
                      title: "Test Complete",
                      color: Color(0xffD9E7FD),
                      isWhite: false,
                      onPressedLeftIcon: () {
                        pushAndReplacement(context, ApptitudePage());
                      },
                    ),
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 150),
                    // ),
                  ],
                ),
              ],
              // leading: IconButton(
              //   icon: Icon(Icons.arrow_back, color: Colors.black),
              //   onPressed: () {
              //     pushAndReplacement(context, ApptitudePage());
              //   },
              // ),
              // title: Text(
              //   "Test Complete",
              //   style: TextStyle(
              //       fontSize: 18,
              //       fontWeight: FontWeight.bold,
              //       color: Colors.black,
              //       fontFamily: "SofiaProRegular"),
              // ),
              // centerTitle: true,
              backgroundColor: Color(0xffD9E7FD),
            ),
            body: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: StreamBuilder<GetResultResponse>(
                  stream: userBloc.getResultResponse,
                  builder: (context, snapshot) {
                    return snapshot.hasError
                        ? Container(
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 1.2),
                            child: Center(
                                child: Text(
                              snapshot.error.toString(),
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.blueGrey,
                              ),
                            )))
                        : snapshot.hasData
                            ? Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 1),
                                //color: Colors.red,
                                child: Stack(
                                  //crossAxisAlignment: CrossAxisAlignment.center,
                                  //alignment: Alignment.center,
                                  children: [
                                    Container(
                                      width: screenWidth(context, dividedBy: 1),
                                      height:
                                          screenHeight(context, dividedBy: 4.5),
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/booking_img1.png")),
                                        color: Color(0xffD9E7FD),
                                      ),
                                    ),
                                    new Positioned(
                                      top: screenHeight(context, dividedBy: 6),
                                      left: screenWidth(context, dividedBy: 3.2),
                                      right: screenWidth(context, dividedBy: 3.2),
                                      child: Container(
                                        width:
                                            screenWidth(context, dividedBy: 2.7),
                                        height:
                                            screenHeight(context, dividedBy: 4.8),
                                        //padding: EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: Constants.kitGradients[2],
                                                width: 6)),
                                        child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                snapshot.data.data.marksScored
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: 35,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily:
                                                        "SofiaProRegular"),
                                              ),
                                              Text(
                                                snapshot.data.data.answerCount
                                                        .toString() +
                                                    "of " +
                                                    snapshot.data.data.qaCount
                                                        .toString(),
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w400,
                                                    color: Color(0xff7C7C7C),
                                                    fontFamily:
                                                        "SofiaProRegular"),
                                              )
                                            ]),
                                      ),
                                    ),
                                    new Positioned(
                                      top: screenHeight(context, dividedBy: 6.5),
                                      left: screenWidth(context, dividedBy: 2.37),
                                      right:
                                          screenWidth(context, dividedBy: 2.37),
                                      child: Container(
                                        width:
                                            screenWidth(context, dividedBy: 18),
                                        height:
                                            screenHeight(context, dividedBy: 20),
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Color(0xff172A88),
                                            shape: BoxShape.circle),
                                        child: SvgPicture.asset(
                                            "assets/icons/test_tick.svg"),
                                      ),
                                    ),
                                    new Positioned(
                                      top: screenHeight(context, dividedBy: 2.5),
                                      child: Container(
                                        width: screenWidth(context, dividedBy: 1),
                                        padding: EdgeInsets.all(10),
                                        //alignment: Alignment.center,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 20),
                                            ),
                                            Text(
                                              snapshot.data.data.grade.toString(),
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: "SofiaProRegular"),
                                            ),
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 20),
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 1.5),
                                              child: Text(
                                                questionCount ==
                                                        correctAnswerCount
                                                    ? "Congratulations for getting all the answers correct."
                                                    : "",
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily:
                                                        "SofiaProRegular"),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 6.5),
                                            ),
                                            Container(
                                              width: screenWidth(context,
                                                  dividedBy: 1.2),
                                              alignment: Alignment.center,
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 15),
                                              child: CButton(
                                                onPressed: () {
                                                  push(context, BottomBar());
                                                },
                                                title: "Take me Home",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                height: screenHeight(context, dividedBy: 1.3),
                                width: screenWidth(context, dividedBy: 1),
                                child: Center(
                                  child: CircularProgressIndicator(
                                    valueColor: new AlwaysStoppedAnimation<Color>(
                                        Constants.kitGradients[2]),
                                  ),
                                ),
                              );
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
