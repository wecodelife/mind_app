import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/terms_list_item.dart';
import 'package:flutter/material.dart';

class PrivacyPolicyPage extends StatefulWidget {
  //const TermsAndConditions({Key? key}) : super(key: key);

  @override
  _PrivacyPolicyPageState createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  List<String> headings = [
    "Privacy Policy",
    "Information Collection and Use",
    "Log Data",
    "Cookies",
    "Service Providers",
    "Security",
    "Links to Other Sites",
    "Children’s Privacy",
    "Changes to This Privacy Policy",
    "Contact Us"
  ];
  List<String> subHeading = [
    "Thoufeek. k built the UPYOU app as an Ad Supported app. This SERVICE is provided"
        " by Thoufeek. k at no cost and is intended for use as is."
        " This page is used to inform visitors regarding my policies with the collection, use, and"
        " disclosure of Personal Information if anyone decided to use my Service."
        " If you choose to use my Service, then you agree to the collection and use of"
        " information in relation to this policy. The Personal Information that I collect is used for"
        " providing and improving the Service. I will not use or share your information with"
        " anyone except as described in this Privacy Policy."
        " If you do not agree to this privacy policy, you shall not access or use our application."
        " Your continued access or use of our application following the posting of changes to"
        " this privacy policy will be deemed to be the acceptance of these changes by the user."
        " The terms used in this Privacy Policy have the same meanings as in our Terms and"
        " Conditions, which is accessible at UPYOU unless otherwise defined in this Privacy"
        " Policy.",
    "For a better experience, while using our Service, I may require you to provide us with"
        " certain personally identifiable information, including but not limited to Name,"
        " username, Email address, Phone number. The personal data that we take from you are"
        " safe with us and it never will be redistributed and shared with anyone. The app does"
        " use third-party services that may collect information used to identify you. Link to the"
        " privacy policy of third party service providers used by the app\n\n"
        "        • AdMob\n\n"
        "        • Google Analytics for Firebase\n\n"
        "        • Firebase Crashlytics\n",
    "I want to inform you that whenever you use my Service, in a case of an error in the app"
        " I collect data and information (through third party products) on your phone called Log"
        " Data. This Log Data may include information such as your device Internet Protocol"
        " (“IP”) address, device name, operating system version, the configuration of the app"
        " when utilizing my Service, the time and date of your use of the Service, and other"
        " statistics.",
    "Cookies are files with a small amount of data that are commonly used as anonymous"
        " unique identifiers. These are sent to your browser from the websites that you visit and"
        " are stored on your device's internal memory."
        " This Service does not use these “cookies” explicitly. However, the app may use third"
        " party code and libraries that use “cookies” to collect information and improve their"
        " services. You have the option to either accept or refuse these cookies and know when"
        " a cookie is being sent to your device. If you choose to refuse our cookies, you may not"
        " be able to use some portions of this Service.",
    "I may employ third-party companies and individuals due to the following reasons:\n\n"
        "            • To facilitate our Service;\n\n"
        "            • To provide the Service on our behalf;\n\n"
        "            • To perform Service-related services; or\n\n"
        "            • To assist us in analyzing how our Service is used.\n"
        " I want to inform users of this Service that these third parties have access to your"
        " Personal Information. The reason is to perform the tasks assigned to them on our"
        " behalf. However, they are obligated not to disclose or use the information for any other"
        " purpose.",
    "I value your trust in providing us with your Personal Information, thus we are striving"
        " to use commercially acceptable means of protecting it. But remember that no method"
        " of transmission over the internet, or method of electronic storage is 100% secure and"
        " reliable, and I cannot guarantee its absolute security.",
    "This Service may contain links to other sites. If you click on a third-party link, you will"
        " be directed to that site. Note that these external sites are not operated by me."
        " Therefore, I strongly advise you to review the Privacy Policy of these websites. I have"
        " no control over and assume no responsibility for the content, privacy policies, or"
        " practices of any third-party sites or services.",
    "These Services do not address anyone under the age of 13. I do not knowingly collect"
        " personally identifiable information from children under 13 years of age. In the case I"
        " discover that a child under 13 has provided me with personal information, I"
        " immediately delete this from our servers. If you are a parent or guardian and you are"
        " aware that your child has provided us with personal information, please contact me so"
        " that I will be able to do necessary actions.",
    "I may update our Privacy Policy from time to time. Thus, you are advised to review this"
        " page periodically for any changes. I will notify you of any changes by posting the new"
        " Privacy Policy on this page."
        "This policy is effective as of 2021-07-04.",
    "If you have any questions or suggestions about my Privacy Policy, do not hesitate to"
        " contact me at upyoulearnings@gmail.com."
  ];

  final String termsAndCondition = "";
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Constants.kitGradients[0],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[0],
          elevation: 0,
          leading: Container(),
          actions: [
            CAppBar(
              onPressedLeftIcon: () {
                pop(context);
              },
              title: "Privacy Policy",
              isWhite: false,
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
              width: screenWidth(context, dividedBy: 1),
              //height: screenHeight(context, dividedBy: 1),
              decoration: BoxDecoration(
                color: Constants.kitGradients[0],
              ),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Container(
                      child: ListView.builder(
                    itemCount: headings.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          Container(
                            child: TermsListItems(
                                index: index,
                                title: headings[index],
                                data: subHeading[index]),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                        ],
                      );
                    },
                  )),

                  // Spacer(),
                  //Center(child: CreatorName()),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
