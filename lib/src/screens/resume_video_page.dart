import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/test_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:app_template/src/utils/object_factory.dart';

class ResumeVideoPage extends StatefulWidget {
  final int id;
  final String url;
  final String title;
  final String description;
  final bool button;
  final int videoIndex;
  ResumeVideoPage(
      {this.id,
      this.url,
      this.title,
      this.button,
      this.description,
      this.videoIndex = 2});
  @override
  _ResumeVideoPageState createState() => _ResumeVideoPageState();
}

class _ResumeVideoPageState extends State<ResumeVideoPage> {
  Size size = Size(200, 200);
  YoutubePlayerController _controller;

  // Future disposePlayer() {
  //   _controller.dispose();
  // }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);

    return true;
  }

  ChewieController _chewieController;
  Future<void> _initializeVideoPlayerFuture;
  bool isPlaying = false;
  bool isComplete = false;
  bool showControl = false;
  VideoPlayerController videoPlayerController;
  Widget networkPlayerWidget;
  FlickManager flickManager;

  @override
  void initState() {
    flickManager = FlickManager(
      autoPlay: false,
      videoPlayerController: VideoPlayerController.network(widget.url),
    );

    // TODO: implement initState
    // _initializeVideoPlayerFuture = videoPlayerController.
    // initialize();
    videoPlayerController = VideoPlayerController.network(widget.url);
    // ..initialize().then((_) {
    //   // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
    //   setState(() {});
    // });
    _chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 16 / 9,
        autoInitialize: true,
        showControlsOnInitialize: false,
        autoPlay: false,
        showControls: true,
        // allowPlaybackSpeedChanging: false,
        errorBuilder: (context, errorMessage) {
          return Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 3.4),
            child: Center(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    print("Try again");
                    _chewieController.play();
                  });
                },
                child: Text(
                  "Something went wrong, Please try again",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontFamily: 'SofiaProSemiBold'),
                ),
              ),
            ),
          );
        });
    // widget.videoPlayerController.addListener(() {
    //   widget.videoPlayerController.value.position ==
    //           widget.videoPlayerController.value.duration
    //       ? setState(() {
    //           print("Video completed");
    //           isComplete = true;
    //         })
    //       : setState(() {
    //           isComplete = false;
    //           print("Video not complete");
    //         });
    // });

    //
    // networkPlayerWidget = AspectRatio(
    //   aspectRatio: 16 / 9, // default aspect ratio
    //   child: BetterPlayer.network(
    //     widget.url,
    //     // 'https://assets.mixkit.co/videos/preview/mixkit-forest-stream-in-the-sunlight-529-large.mp4',  // video url
    //     betterPlayerConfiguration: BetterPlayerConfiguration(
    //       aspectRatio: 16 / 9, // default aspect ratio
    //       fullScreenAspectRatio: 16 / 9, // fullscreen mode aspect ratio
    //       autoPlay: false,
    //       // looping: widget.looping,
    //       placeholder: Center(child: CircularProgressIndicator()),
    //       allowedScreenSleep: false,
    //       controlsConfiguration: BetterPlayerControlsConfiguration(
    //         textColor: Colors.white,
    //         iconsColor: Colors.white,
    //         skipForwardIcon: Icons.forward_10,
    //         skipBackIcon: Icons.replay_10,
    //         forwardSkipTimeInMilliseconds: 10000,
    //         // controlBarColor: Colors.transparent,
    //         showControlsOnInitialize: false,
    //       ),
    //     ),
    //   ),
    // );
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoPlayerController.dispose();
    _chewieController.dispose();
    // // _chewieController.pause();
    flickManager.dispose();
    super.dispose();
  }
  //
  // @override
  // void dispose() {
  //   _controller.dispose();
  //   // TODO: implement dispose
  //   super.dispose();
  // }
  //
  // @override
  // void initState() {
  //   // print(ObjectFactory().appHive.getExamId());
  //   // print("question id" + widget.id.toString());
  //   // print("widget" + widget.url.toString());
  //   _controller = YoutubePlayerController(
  //     initialVideoId: widget.url,
  //     flags: YoutubePlayerFlags(
  //         autoPlay: true,
  //         mute: false,
  //         hideControls: false,
  //         hideThumbnail: false),
  //   );
  //   // TODO: implement initState
  //   super.initState();
  // }

  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark),
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child:
            // YoutubePlayerBuilder(
            //   onExitFullScreen: () {
            //     SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
            //   },
            //   onEnterFullScreen: () {
            //     SystemChrome.setPreferredOrientations([
            //       DeviceOrientation.landscapeRight,
            //       DeviceOrientation.landscapeLeft
            //     ]);
            //   },
            //   player: YoutubePlayer(
            //     controller: _controller,
            //     aspectRatio: 1,
            //   ),
            //   builder: (context, player) =>
            SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            body:
                // Stack(
                //   children: [
                Column(
              children: [
                // Container(
                //     height: screenHeight(context, dividedBy: 2),
                //     child: player),

                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: GestureDetector(
                      onTap: () {
                        pop(context);
                        // pushAndRemoveUntil(context, BottomBar(), false);
                      },
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.black,
                        size: 25,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  child: Stack(
                    children: <Widget>[
                      // AspectRatio(
                      //   aspectRatio: 16 / 9,
                      //   child: Container(
                      //     width: screenWidth(context, dividedBy: 1),
                      //     height: screenHeight(context, dividedBy: 3.4),
                      //     decoration: BoxDecoration(
                      //       color: Colors.white,
                      //     ),
                      //     child: FlickVideoPlayer(flickManager: flickManager),
                      //   ),
                      // ),
                      //

                      // _betterPlayerController != null ? AspectRatio(
                      //   aspectRatio: 16 / 9,
                      //   child: Container(
                      //     width: screenWidth(context, dividedBy: 1),
                      //       height: screenHeight(context, dividedBy: 3.4),
                      //       decoration: BoxDecoration(
                      //           color: Colors.white,
                      //       ),
                      //     child: BetterPlayer(
                      //       controller: _betterPlayerController,
                      //     ),
                      //   ),
                      // ): Container(),

                      _chewieController != null
                          ? AspectRatio(
                              aspectRatio: 16 / 9,
                              child: Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 3.4),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                ),
                                padding: EdgeInsets.symmetric(
                                    // horizontal: screenWidth(context, dividedBy: 70),
                                    ),
                                child:

                                    // networkPlayerWidget,
                                    Theme(
                                        data: Theme.of(context).copyWith(
                                          dialogBackgroundColor: Constants
                                              .kitGradients[0]
                                              .withOpacity(0.35),
                                          // Colors.transparent,
                                          accentColor:
                                              _chewieController.isPlaying
                                                  ? Constants.kitGradients[0]
                                                  : Constants.kitGradients[2],
                                          iconTheme: IconThemeData(
                                            color: Constants.kitGradients[2],
                                          ),
                                        ),
                                        child: Chewie(
                                            controller: _chewieController)),
                              ),
                            )
                          : Container(),

                      Positioned(
                        // top: screenHeight(context, dividedBy: 11),
                        // left: screenWidth(context, dividedBy: 7),
                        // right: screenWidth(context, dividedBy: 7),
                        child: GestureDetector(
                          onTap: () {
                            print("Tapped");
                            ObjectFactory().appHive.getOrderStatus() == true
                                ? print("Subscribed")
                                : widget.videoIndex == 0
                                    ? print("Vide Index is 0")
                                    : widget.videoIndex == 1
                                        ? print("Vide Index is 1")
                                        : showSubscriptionAlertDialog(context);
                            setState(() {
                              ObjectFactory().appHive.getOrderStatus() == true
                                  ? _chewieController.play()
                                  : _chewieController.pause();
                              isPlaying = true;
                            });
                          },
                          child: isPlaying == true
                              ? Container()
                              : Container(
                                  child: Center(
                                    child: Container(
                                      width: screenWidth(context, dividedBy: 1),
                                      height:
                                          screenHeight(context, dividedBy: 3.4),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        // border: Border.all(color: Colors.white, width: 10.0),
                                        // shape: BoxShape.circle
                                      ),
                                      // child: Center(
                                      //   child: Icon(
                                      //        Icons.play_arrow,
                                      //           // : isComplete == true
                                      //           //     ? Icons.refresh
                                      //           //     : Icons.pause_outlined,
                                      //       size: 30,
                                      //       color:  Constants.kitGradients[0]),
                                      // ),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                      // Positioned(
                      //   // bottom: screenHeight(context, dividedBy: 100),
                      //   // left: screenWidth(context, dividedBy: 70),
                      //   bottom: screenHeight(context, dividedBy: 90),
                      //   left: screenWidth(context, dividedBy: 25),
                      //   // right: screenWidth(context, dividedBy: 7),
                      //   child: GestureDetector(
                      //     onTap: () {
                      //       print("Tapped");
                      //       ObjectFactory().appHive.getOrderStatus() == true
                      //           ? print("Subscribed")
                      //           : showSubscriptionAlertDialog(context);
                      //       setState(() {
                      //         ObjectFactory().appHive.getOrderStatus() == true
                      //             ? _chewieController.play()
                      //             : _chewieController.pause();
                      //         isPlaying = true;
                      //       });
                      //     },
                      //     child: isPlaying == true
                      //         ? Container()
                      //         : Container(
                      //             child: Center(
                      //               child: Container(
                      //                 width: screenWidth(context, dividedBy: 10),
                      //                 height: screenWidth(context, dividedBy: 10),
                      //                 decoration: BoxDecoration(
                      //                     color: Colors.blue.withOpacity(0.5),
                      //                     // border: Border.all(color: Colors.white, width: 10.0),
                      //                     shape: BoxShape.circle
                      //                 ),
                      //                 // child: Center(
                      //                 //   child: Icon(
                      //                 //        Icons.play_arrow,
                      //                 //           // : isComplete == true
                      //                 //           //     ? Icons.refresh
                      //                 //           //     : Icons.pause_outlined,
                      //                 //       size: 30,
                      //                 //       color:  Constants.kitGradients[0]),
                      //                 // ),
                      //               ),
                      //             ),
                      //           ),
                      //   ),
                      // ),
                    ],
                  ),
                ),

                // _chewieController != null
                //     ? AspectRatio(
                //         aspectRatio: 16 / 9,
                //         child: Container(
                //           width: screenWidth(context, dividedBy: 1),
                //           height: screenHeight(context, dividedBy: 3.4),
                //           decoration: BoxDecoration(
                //             color: Colors.white,
                //           ),
                //           padding: EdgeInsets.symmetric(
                //               // horizontal: screenWidth(context, dividedBy: 70),
                //               ),
                //           child:
                //
                //               // networkPlayerWidget,
                //               Theme(
                //                   data: Theme.of(context).copyWith(
                //                     dialogBackgroundColor: Constants
                //                         .kitGradients[0]
                //                         .withOpacity(0.35),
                //                     // Colors.transparent,
                //                     accentColor: _chewieController.isPlaying
                //                         ? Constants.kitGradients[0]
                //                         : Constants.kitGradients[2],
                //                     iconTheme: IconThemeData(
                //                       color: Constants.kitGradients[2],
                //                     ),
                //                   ),
                //                   child: Chewie(
                //                       controller: _chewieController)),
                //         ),
                //       )
                //     : Container(),
                // Positioned(
                //   // top: screenHeight(context, dividedBy: 11),
                //   // left: screenWidth(context, dividedBy: 7),
                //   // right: screenWidth(context, dividedBy: 7),
                //   // top: screenHeight(context, dividedBy: 15),
                //   // left: screenWidth(context, dividedBy: 7),
                //   // right: screenWidth(context, dividedBy: 7),
                //   child: GestureDetector(
                //     onTap: () {
                //       print("Tapped");
                //       ObjectFactory().appHive.getOrderStatus() == true
                //           ? print("Subscribed")
                //           : showSubscriptionAlertDialog(context);
                //       setState(() {
                //         ObjectFactory().appHive.getOrderStatus() == true
                //             ? _chewieController.play()
                //             : _chewieController.pause();
                //         isPlaying = true;
                //       });
                //     },
                //     child: isPlaying == true
                //         ? Container()
                //         : Container(
                //             child: Center(
                //               child: Container(
                //                 width: screenWidth(context, dividedBy: 1),
                //                 height:
                //                     screenHeight(context, dividedBy: 3.4),
                //                 decoration: BoxDecoration(
                //                   color: Colors.transparent,
                //                   // border: Border.all(color: Colors.white, width: 10.0),
                //                   // shape: BoxShape.circle
                //                 ),
                //                 // child: Center(
                //                 //   child: Icon(
                //                 //        Icons.play_arrow,
                //                 //           // : isComplete == true
                //                 //           //     ? Icons.refresh
                //                 //           //     : Icons.pause_outlined,
                //                 //       size: 30,
                //                 //       color:  Constants.kitGradients[0]),
                //                 // ),
                //               ),
                //             ),
                //           ),
                //   ),
                // ),

                // Stack(
                //   children: [
                //     AspectRatio(
                //       aspectRatio: 16 / 9,
                //       child: Container(
                //         width: screenWidth(context, dividedBy: 1),
                //         height: screenHeight(context, dividedBy: 3.4),
                //         decoration: BoxDecoration(
                //           color: Colors.white,
                //         ),
                //         child: FlickVideoPlayer(flickManager: flickManager),
                //       ),
                //     ),
                //     Positioned(
                //       top: screenHeight(context, dividedBy: 10),
                //       left: screenWidth(context, dividedBy: 7),
                //       right: screenWidth(context, dividedBy: 7),
                //       child: GestureDetector(
                //         onTap: () {
                //           print("Tapped");
                //           ObjectFactory().appHive.getOrderStatus() == true
                //               ? print("Subscribed")
                //               : showSubscriptionAlertDialog(context);
                //           setState(() {
                //             ObjectFactory().appHive.getOrderStatus() == true
                //                 ? _chewieController.play()
                //                 : _chewieController.pause();
                //             isPlaying = true;
                //           });
                //         },
                //         child: isPlaying == true
                //             ? Container()
                //             : Container(
                //           child: Center(
                //             child: Container(
                //               width: screenWidth(context, dividedBy: 5),
                //               height: screenWidth(context, dividedBy: 5),
                //               decoration: BoxDecoration(
                //                   color: Colors.transparent,
                //                   // border: Border.all(color: Colors.white, width: 10.0),
                //                   shape: BoxShape.circle),
                //               // child: Center(
                //               //   child: Icon(
                //               //        Icons.play_arrow,
                //               //           // : isComplete == true
                //               //           //     ? Icons.refresh
                //               //           //     : Icons.pause_outlined,
                //               //       size: 30,
                //               //       color:  Constants.kitGradients[0]),
                //               // ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     ),
                //     Positioned(
                //       bottom: screenHeight(context, dividedBy: 100),
                //       left: screenWidth(context, dividedBy: 70),
                //       // right: screenWidth(context, dividedBy: 7),
                //       child: GestureDetector(
                //         onTap: () {
                //           print("Tapped");
                //           ObjectFactory().appHive.getOrderStatus() == true
                //               ? print("Subscribed")
                //               : showSubscriptionAlertDialog(context);
                //           setState(() {
                //             ObjectFactory().appHive.getOrderStatus() == true
                //                 ? _chewieController.play()
                //                 : _chewieController.pause();
                //             isPlaying = true;
                //           });
                //         },
                //         child: isPlaying == true
                //             ? Container()
                //             : Container(
                //           child: Center(
                //             child: Container(
                //               width: screenWidth(context, dividedBy: 13),
                //               height: screenWidth(context, dividedBy: 13),
                //               decoration: BoxDecoration(
                //                   color: Colors.transparent,
                //                   // border: Border.all(color: Colors.white, width: 10.0),
                //                   shape: BoxShape.circle),
                //               // child: Center(
                //               //   child: Icon(
                //               //        Icons.play_arrow,
                //               //           // : isComplete == true
                //               //           //     ? Icons.refresh
                //               //           //     : Icons.pause_outlined,
                //               //       size: 30,
                //               //       color:  Constants.kitGradients[0]),
                //               // ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     ),
                //   ],
                // ),

                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 30)),
                  child: Container(
                    //color: Colors.yellow,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        GestureDetector(
                          onTap: () {
                            // _controller.setSize(size);
                          },
                          child: Text(
                            widget.title,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontFamily: 'SofiaProSemiBold'),
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 90),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              // horizontal: screenWidth(context, dividedBy: 30),
                              ),
                          child: Text(
                            widget.description,
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontFamily: 'SofiaProSemiBold'),
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        widget.button != false
                            ? Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 10),
                                child: CButton(
                                  title: "Let us practice",
                                  onPressed: () {
                                    pushAndReplacement(
                                        context,
                                        TestPage(
                                          id: widget.id,
                                          firstTime: true,
                                        ));
                                  },
                                ))
                            : Container(),
                      ],
                    ),
                  ),
                )
              ],
            ),
            //     new Positioned(
            //       top: screenHeight(context, dividedBy: 60),
            //       left: screenWidth(context, dividedBy: 35),
            //       child: Container(
            //         child: GestureDetector(
            //           onTap: () {
            //             pop(context);
            //             // pushAndRemoveUntil(context, BottomBar(), false);
            //           },
            //           child: Icon(
            //             Icons.arrow_back_ios_rounded,
            //             color: Colors.black,
            //             size: 25,
            //           ),
            //         ),
            //       ),
            //     ),
            //   ],
            // ),
          ),
        ),
        // ),
      ),
    );

    // new Positioned(
    //   top: screenHeight(context, dividedBy: 20),
    //   child: Container(
    //     child: IconButton(
    //       icon: Icon(
    //         Icons.arrow_back,
    //         color: Colors.white,
    //         size: 30,
    //       ),
    //       onPressed: () {
    //         pop(context);
    //       },
    //     ),
    //   ),

    //   WillPopScope(
    //   onWillPop: _willPopCallback,
    //   child: Scaffold(
    //     backgroundColor: Colors.white,
    //     appBar: AppBar(
    //       backgroundColor: Colors.white,
    //       toolbarHeight: screenHeight(context, dividedBy: 15),
    //       elevation: 0,
    //       leading: Container(),
    //       actions: [
    //         Column(
    //           children: [
    //             // SizedBox(
    //             //   height: screenHeight(context, dividedBy: 150),
    //             // ),
    //             CAppBar(
    //               title: widget.title,
    //               color: Colors.white,
    //               isWhite: false,
    //               onPressedLeftIcon: () {
    //                 pushAndRemoveUntil(context, BottomBar(), false);
    //               },
    //             ),
    //             // SizedBox(
    //             //   height: screenHeight(context, dividedBy: 150),
    //             // ),
    //           ],
    //         ),
    //       ],
    //     ),
    //     body: Stack(
    //       children: [
    //         Column(
    //           children: [
    //             Container(
    //                 height: screenHeight(context, dividedBy: 2),
    //                 child: Theme(
    //                     data: Theme.of(context).copyWith(
    //                       dialogBackgroundColor:
    //                       Constants.kitGradients[0].withOpacity(0.35),
    //                       // Colors.transparent,
    //                       accentColor: Constants.kitGradients[0],
    //                       iconTheme: IconThemeData(
    //                         color: Constants.kitGradients[0],
    //                       ),
    //                     ),
    //                     child: Chewie(controller: _chewieController))
    //                 // player
    //             ),
    //             Expanded(
    //               flex: 1,
    //               child: Padding(
    //                 padding: EdgeInsets.symmetric(
    //                     horizontal: screenWidth(context, dividedBy: 15)),
    //                 child: Container(
    //                   //color: Colors.yellow,
    //                   child: Column(
    //                     children: [
    //                       SizedBox(
    //                         height: screenHeight(context, dividedBy: 20),
    //                       ),
    //                       GestureDetector(
    //                         onTap: () {
    //                           // _controller.setSize(size);
    //                         },
    //                         child: Text(
    //                           widget.title,
    //                           style: TextStyle(
    //                               color: Colors.black,
    //                               fontSize: 24,
    //                               fontFamily: 'SofiaProSemiBold'),
    //                         ),
    //                       ),
    //                       SizedBox(
    //                         height: screenHeight(context, dividedBy: 10),
    //                       ),
    //                       widget.button != false
    //                           ? Container(
    //                           alignment: Alignment.center,
    //                           padding:
    //                           EdgeInsets.symmetric(horizontal: 15),
    //                           width: screenWidth(context, dividedBy: 1),
    //                           height:
    //                           screenHeight(context, dividedBy: 10),
    //                           child: CButton(
    //                             title: "Let us practice",
    //                             onPressed: () {
    //                               pushAndReplacement(
    //                                   context,
    //                                   TestPage(
    //                                     id: widget.id,
    //                                     firstTime: true,
    //                                   ));
    //                             },
    //                           ))
    //                           : Container(),
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             )
    //           ],
    //         ),
    //         new Positioned(
    //           top: screenHeight(context, dividedBy: 30),
    //           child: Container(
    //             child: Row(
    //               children: [
    //                 IconButton(
    //                   onPressed: () {
    //                     pushAndRemoveUntil(context, BottomBar(), false);
    //                   },
    //                   icon: Icon(
    //                     Icons.arrow_back,
    //                     color: Colors.white,
    //                     size: 30,
    //                   ),
    //                 ),
    //                 Text(
    //                   widget.title,
    //                   style: TextStyle(
    //                       color: Colors.black,
    //                       fontSize: 24,
    //                       fontFamily: 'SofiaProSemiBold'),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //       ],
    //     ),
    //   ),
    //   // YoutubePlayerBuilder(
    //   //   onExitFullScreen: () {
    //   //     SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    //   //   },
    //   //   onEnterFullScreen: () {
    //   //     SystemChrome.setPreferredOrientations([
    //   //       DeviceOrientation.landscapeRight,
    //   //       DeviceOrientation.landscapeLeft
    //   //     ]);
    //   //   },
    //   //   player: YoutubePlayer(
    //   //     controller: _controller,
    //   //     aspectRatio: 1,
    //   //   ),
    //   //   builder: (context, player) => Scaffold(
    //   //     body: Stack(
    //   //       children: [
    //   //         Column(
    //   //           children: [
    //   //             Container(
    //   //                 height: screenHeight(context, dividedBy: 2),
    //   //                 child: player),
    //   //             Expanded(
    //   //               flex: 1,
    //   //               child: Padding(
    //   //                 padding: EdgeInsets.symmetric(
    //   //                     horizontal: screenWidth(context, dividedBy: 15)),
    //   //                 child: Container(
    //   //                   //color: Colors.yellow,
    //   //                   child: Column(
    //   //                     children: [
    //   //                       SizedBox(
    //   //                         height: screenHeight(context, dividedBy: 20),
    //   //                       ),
    //   //                       GestureDetector(
    //   //                         onTap: () {
    //   //                           // _controller.setSize(size);
    //   //                         },
    //   //                         child: Text(
    //   //                           widget.title,
    //   //                           style: TextStyle(
    //   //                               color: Colors.black,
    //   //                               fontSize: 24,
    //   //                               fontFamily: 'SofiaProSemiBold'),
    //   //                         ),
    //   //                       ),
    //   //                       SizedBox(
    //   //                         height: screenHeight(context, dividedBy: 10),
    //   //                       ),
    //   //                       widget.button != false
    //   //                           ? Container(
    //   //                               alignment: Alignment.center,
    //   //                               padding:
    //   //                                   EdgeInsets.symmetric(horizontal: 15),
    //   //                               width: screenWidth(context, dividedBy: 1),
    //   //                               height:
    //   //                                   screenHeight(context, dividedBy: 10),
    //   //                               child: CButton(
    //   //                                 title: "Let us practice",
    //   //                                 onPressed: () {
    //   //                                   pushAndReplacement(
    //   //                                       context,
    //   //                                       TestPage(
    //   //                                         id: widget.id,
    //   //                                         firstTime: true,
    //   //                                       ));
    //   //                                 },
    //   //                               ))
    //   //                           : Container(),
    //   //                     ],
    //   //                   ),
    //   //                 ),
    //   //               ),
    //   //             )
    //   //           ],
    //   //         ),
    //   //         new Positioned(
    //   //           top: screenHeight(context, dividedBy: 20),
    //   //           child: Container(
    //   //             child: IconButton(
    //   //               onPressed: () {
    //   //                 pushAndRemoveUntil(context, BottomBar(), false);
    //   //               },
    //   //               icon: Icon(
    //   //                 Icons.arrow_back,
    //   //                 color: Colors.white,
    //   //                 size: 30,
    //   //               ),
    //   //             ),
    //   //           ),
    //   //         ),
    //   //       ],
    //   //     ),
    //   //   ),
    //   // ),
    // );

    // new Positioned(
    //   top: screenHeight(context, dividedBy: 20),
    //   child: Container(
    //     child: IconButton(
    //       icon: Icon(
    //         Icons.arrow_back,
    //         color: Colors.white,
    //         size: 30,
    //       ),
    //       onPressed: () {
    //         pop(context);
    //       },
    //     ),
    //   ),
  }
}
// _chewieController != null
//     ? AspectRatio(
//         aspectRatio: 16 / 9,
//         child: Container(
//           // width: screenWidth(context, dividedBy: 1),
//           height: screenHeight(context, dividedBy: 3.4),
//           decoration: BoxDecoration(
//             color: Colors.white,
//           ),
//           // padding: EdgeInsets.symmetric(
//           //   horizontal: screenWidth(context, dividedBy: 70),
//           // ),
//           child:
//               // ClipRRect(
//               //     borderRadius:BorderRadius.circular(8),
//               //     child: networkPlayerWidget),
//               Theme(
//                   data: Theme.of(context).copyWith(
//                     dialogBackgroundColor: Constants
//                         .kitGradients[0]
//                         .withOpacity(0.35),
//                     // Colors.transparent,
//                     accentColor: _chewieController.isPlaying
//                         ? Constants.kitGradients[0]
//                         : Constants.kitGradients[2],
//                     iconTheme: IconThemeData(
//                       color: Constants.kitGradients[0],
//                     ),
//                   ),
//                   child: Chewie(
//                       controller: _chewieController)),
//         ))
//     : Container(),
