import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/utils/constants.dart';

class BookingCompletePage extends StatefulWidget {
  @override
  _BookingCompletePageState createState() => _BookingCompletePageState();
}

class _BookingCompletePageState extends State<BookingCompletePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            child: Stack(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 3.5),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/booking_img1.png")),
                    color: Color(0xffD9E7FD),
                  ),
                ),
                new Positioned(
                  left: screenWidth(context, dividedBy: 4),
                  top: screenHeight(context, dividedBy: 5),
                  child: Container(
                    width: screenWidth(context, dividedBy: 2),
                    height: screenHeight(context, dividedBy: 7),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[2],
                      shape: BoxShape.circle,
                      boxShadow:[
                        BoxShadow(
                          color:Colors.blue,
                          // offset: const Offset(
                          //   3.0,
                          //   3.0,
                          // ),
                          blurRadius: 30.0,
                          spreadRadius: 0.0,

                        ),
                      ],
                    ),
                    child: SvgPicture.asset("assets/images/tick.svg"),
                  ),
                ),
                new Positioned(
                  // left: screenWidth(context, dividedBy: 100),
                  top: screenHeight(context, dividedBy: 2.5),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    padding: EdgeInsets.all(10),
                    //alignment: Alignment.center,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Booking Complete",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              fontFamily: "SofiaProRegular"),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Text(
                          "Wonderful!!! that’s a great step to upgrade you. Our representative will contact you Shortly via WhatsApp. They can guide you to experts.",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SofiaProRegular"),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                new Positioned(
                  //left: screenWidth(context, dividedBy: 12),
                  top: screenHeight(context, dividedBy: 1.3),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    padding:EdgeInsets.symmetric(horizontal: 15),
                    child: Center(
                      child: CButton(
                        onPressed: () {
                          push(context, BottomBar());
                        },
                        title: "Go Home",
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

