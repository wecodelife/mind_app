import 'package:app_template/src/screens/onboarding_screen3.dart';
import 'package:app_template/src/screens/slide_transition.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_2_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swipedetector/swipedetector.dart';

class OnBoardingScreen2 extends StatefulWidget {
  @override
  _OnBoardingScreen2State createState() => _OnBoardingScreen2State();
}

class _OnBoardingScreen2State extends State<OnBoardingScreen2> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Color(0xffD9E7FD),
    ),
    child:SafeArea(
      child: SwipeDetector(
        onSwipeLeft: () {
          Navigator.of(context)
              .push(SlideRightRoute(page: OnBoardingScreen3()));
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          // extendBodyBehindAppBar: true,
          body: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            child: OnBoardingContent2(
              head: "Let’s Practice!",
              dataHead: "Attend a Mock interview from our experts.",
              data:
                  "Why mock interview: \n - Helps you identify your Mistakes.\n - Gets instant feedback from experts.\n - Boosts Your confidence. \n - it reduces the chance of rejection.\n - it takes you closer to your dream job.",
              buttonData: "Next",
              image: "assets/images/on_boarding2.svg",
              index: 1,
              onPressed: () {
                Navigator.of(context)
                    .push(SlideRightRoute(page: OnBoardingScreen3()));
                //Navigator.push(context, SlideRightRoute(page: OnBoardingScreen3()));
                //push(context, OnBoardingScreen3());
              },
            ),
            // OnBoardingContent(
            //   head: "Yes, You should !!",
            //   data:
            //       "Attend a Mock interview from our experts.\n\n Why mock interview: \n- Helps you identify Your weak areas.\n - Gets Feedback from experts to Improve.\n - Boosts Your confidence. \n- It takes you closer to your dream job.\n - It reduces the chance of rejection during first interviews.",
            //   image: "assets/images/on_boarding2.svg",
            //   buttonData: "Next",
            //    index:1,
            //   onPressed: (){push(context, OnBoardingScreen3());},
            // ),
          ),
        ),
      ),
    ));
  }
}
