import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/job_list_response_model.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/view_jobAlert_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/job_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class JobAlertPage extends StatefulWidget {
  @override
  _JobAlertPageState createState() => _JobAlertPageState();
}

class _JobAlertPageState extends State<JobAlertPage> {
  UserBloc userBloc = new UserBloc();
  @override
  void initState() {
    userBloc.getJobList();

    userBloc.getJobListResponse.listen((event) {}).onError((event) {
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
    });

    // TODO: implement initState
    super.initState();
  }

  List<String> selectedElementArray = [];
  int i;
  bool idExist = false;
  void addingSelection(int id) {
    if (selectedElementArray.isNotEmpty) {
      for (i = 0; i < selectedElementArray.length; i++) {
        print(i);
        if (selectedElementArray[i] == id.toString()) {
          idExist = true;
          selectedElementArray.removeAt(i);
        }
        if (idExist == false && i == (selectedElementArray.length - 1)) {
          selectedElementArray.add(id.toString());

          break;
        }
      }
    } else {
      selectedElementArray.add(id.toString());
    }
  }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark),
    child:WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: Container(),
            actions: [
              CAppBar(
                title: "Job Alert",
                color: Colors.white,
                isWhite: false,
                onPressedLeftIcon: () {
                  pushAndRemoveUntil(context, BottomBar(), false);
                },
              ),
            ],
          ),
          body: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            // color:Colors.greenAccent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Text("SELECT ONE OR MORE SECTIONS",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: "SofiaProRegular",
                          color: Color(0xff2C2E39))),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                StreamBuilder<JobListResponseModel>(
                    stream: userBloc.getJobListResponse,
                    builder: (context, snapshot) {
                      return snapshot.hasError
                          ? Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 1.7),
                              child: Center(
                                  child: Text(
                                snapshot.error.toString(),
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.blueGrey,
                                ),
                              )),
                            )
                          : snapshot.hasData
                              ? Expanded(
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: snapshot.data.results.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Container(
                                          width: screenWidth(context,
                                              dividedBy: 10),
                                          //height: screenHeight(context, dividedBy:20),
                                          //color:Colors.blue,
                                          child: JobTile(
                                            data: snapshot
                                                .data.results[index].title,
                                            onPressed: () {
                                              idExist = false;
                                              addingSelection(snapshot
                                                  .data.results[index].id);
                                            },
                                          ));
                                    },
                                  ),
                                )
                              : Container(
                                  height: screenHeight(context, dividedBy: 1.7),
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Constants.kitGradients[2]),
                                    ),
                                  ),
                                );
                    }),
                Center(
                  child: CButton(
                    title: "View Jobs",
                    onPressed: () {
                      print(selectedElementArray.toString() +
                          "selectedarrayemement");
                      if (selectedElementArray.isNotEmpty) {
                        push(
                            context,
                            ViewJobAlerts(
                              jobList: selectedElementArray,
                            ));
                      } else {
                        showToast("Select Any One Job Type.");
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
