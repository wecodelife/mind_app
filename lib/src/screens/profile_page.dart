import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/screens/about_us_page.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/app_list_tile.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    userBloc.logoutResponse.listen((event) {
      if (event.status == 200) {
        signOut();
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Color(0xffD9E7FD),
        statusBarIconBrightness: Brightness.dark),
    child: WillPopScope(
    onWillPop: _willPopCallback,
    child:SafeArea(
    child: Scaffold(
    backgroundColor:Colors.white,
        body: SingleChildScrollView(
          child: Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              child: Stack(
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 3.2),
                    color: Color(0xffD9E7FD),
                  ),
                  CAppBar(
                      title: "Profile",
                      color: Color(0xffD9E7FD),
                      onPressedLeftIcon: () {
                        pop(context);
                      },
                      isWhite: false),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 6.3),
                    left: screenWidth(context, dividedBy: 6),
                    right: screenWidth(context, dividedBy: 6),
                    child: Container(
                      width: screenWidth(context, dividedBy: 2),
                      height: screenHeight(context, dividedBy: 3.5),
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xff4385F5), width: 20),
                        //border: Border(top: BorderSide(color:Colors.green, width: 10), left: BorderSide(color:Colors.white, width: 10), right:BorderSide(color:Colors.white, width: 10)),
                        color: Color(0xff4385F5),
                        shape: BoxShape.circle,
                        //borderRadius: BorderRadius.only(topLeft: Radius.circular((screenWidth(context, dividedBy: 2))/2), topRight: Radius.circular((screenWidth(context, dividedBy: 2))/2))
                      ),
                    ),
                  ),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 3.3),
                    child: Container(
                      height: screenHeight(context, dividedBy: 1),
                      width: screenWidth(context, dividedBy: 1),
                      color: Colors.white,
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 2),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        //color: Colors.blue,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 8),
                            ),
                            Text(
                              "FULL NAME",
                              style: TextStyle(
                                  color: Color(0xff888D99),
                                  fontSize: 12,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 80),
                            ),
                            Text(
                              ObjectFactory().appHive.getName() != null
                                  ? ObjectFactory().appHive.getName()
                                  : "Not Updated",
                              style: TextStyle(
                                  color: Color(0xff2C2E39),
                                  fontSize: 15,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                            Text(
                              "EMAIL / PHONE NUMBER",
                              style: TextStyle(
                                  color: Color(0xff888D99),
                                  fontSize: 12,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 80),
                            ),
                            Text(
                              ObjectFactory().appHive.getEmail() != null
                                  ? ObjectFactory().appHive.getEmail()
                                  : ObjectFactory().appHive.getPhoneNumber() != null ? ObjectFactory().appHive.getPhoneNumber() : "Not Updated",
                              style: TextStyle(
                                  color: Color(0xff2C2E39),
                                  fontSize: 15,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 20),
                            ),
                            AppListTile(
                              text: "About Us",
                              divider: false,
                              onTap: () {
                                push(context, AboutUsPage());
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 4.6),
                    right: screenWidth(context, dividedBy: 2.95),
                    child: CircularProfileAvatar(
                      null,
                      backgroundColor: Constants.kitGradients[0],
                      borderColor: Color(0xff4385F5),
                      borderWidth: screenWidth(context, dividedBy: 100),
                      child: ObjectFactory().appHive.getPhotoUrl() != null
                          ? Image.network(
                              ObjectFactory().appHive.getPhotoUrl(),
                              fit: BoxFit.fill,
                            )
                          : Image.asset(
                              "assets/images/user.png",
                              fit: BoxFit.fill,
                            ),
                      elevation: 0,
                      radius: screenWidth(context, dividedBy: 6),
                    ),
                  ),
                  new Positioned(
                      top: screenHeight(context, dividedBy: 1.3),
                      //left: screenWidth(context, dividedBy: 10),
                      //right: screenWidth(context, dividedBy: 7),
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 14),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        //color:Colors.red,
                        child: RaisedButton(
                          color: Colors.white,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                            side: BorderSide(color: Color(0xff4385F5)),
                          ),
                          onPressed: () async {
                            setState(() {
                              isLoading = true;
                            });
                            await userBloc.logout();
                          },
                          child: isLoading == false
                              ? Text(
                                  "Logout",
                                  style: TextStyle(
                                      color: Color(0xff4385F5),
                                      fontSize: 15,
                                      fontFamily: "SofiaProRegular",
                                      fontWeight: FontWeight.w500),
                                )
                              : Container(
                                  height: screenHeight(context, dividedBy: 1.3),
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Constants.kitGradients[2]),
                                    ),
                                  ),
                                ),
                        ),
                      )),
                ],
              )),
        ),
      ),
    ),),);
  }

  Future<String> signOut() async {
    await GoogleSignIn().signOut().whenComplete(() {
      setState(() {
        isLoading = false;
      });
      ObjectFactory().appHive.putSignedInCheck(false);
      ObjectFactory().appHive.putUserId(userId: null);
      pushAndRemoveUntil(context, LoginPage(), false);
      print("Success");
      String status = "Success";
    }).catchError((e) {
      setState(() {
        isLoading = false;
      });
      showToast(e.toString());
    });
    await FirebaseAuth.instance.signOut().whenComplete(() {
      setState(() {
        isLoading = false;
      });
      ObjectFactory().appHive.putSignedInCheck(false);
      ObjectFactory().appHive.putUserId(userId: null);
      pushAndRemoveUntil(context, LoginPage(), false);
      print("Success");
      String status = "Success";
    }).catchError((e) {
      setState(() {
        isLoading = false;
      });
      showToast(e.toString());
    });
  }
}
