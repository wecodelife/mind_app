import 'dart:io';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/update_user_details_request_model.dart';
import 'package:app_template/src/screens/onboarding_screen1.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/textfield.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UpdateProfilePage extends StatefulWidget {
  final String userName;
  final String bio;
  final String imgUrl;
  UpdateProfilePage({this.userName, this.bio, this.imgUrl});
  @override
  _UpdateProfilePageState createState() => _UpdateProfilePageState();
}

class _UpdateProfilePageState extends State<UpdateProfilePage> {
  final picker = ImagePicker();
  File imagePicked;
  UserBloc userBloc = UserBloc();
  bool loading = false;
  bool isValid = false;
  TextEditingController firstNameTextEditingController =
      new TextEditingController();
  TextEditingController lastNameTextEditingController =
      new TextEditingController();
  Future getImageGallery() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        // loading = true;
      });
      // userBloc.uploadImage(
      //     uploadImageRequest: UploadImageRequest(image: imagePicked));
    }
  }

  void emailCheck(email) {
    isValid = EmailValidator.validate(email);
    if (isValid == true) {
    } else {
      showToast("Enter A valid Email");
    }
  }

  Future getImageCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        // loading = true;
      });
      // userBloc.uploadImage(
      //     uploadImageRequest: UploadImageRequest(image: imagePicked));
    }
  }

  @override
  void initState() {
    print("Email ID:" + ObjectFactory().appHive.getEmail());

    userBloc.updateUserDetailsResponse.listen((event) {
      if (event.status == 200) {
        setState(() {
          loading = false;
        });
        ObjectFactory()
            .appHive
            .putName(name: event.firstName + " " + event.lastName);
        ObjectFactory()
            .appHive
            .putPhotoUrl(Urls.imageBaseUrl + event.profileImageUrl);
        ObjectFactory().appHive.putSignedInCheck(true);
        pushAndRemoveUntil(context, OnBoardingScreen(), false);
      } else {
        setState(() {
          loading = false;
        });
        showToast(event.errorMessage);
      }
    }).onError((event){
         showToast(event.toString());
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      message: "Press back again to quit",
      child: Scaffold(
        backgroundColor: Constants.kitGradients[0],
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenHeight(context, dividedBy: 30)),
          child: Column(
            // mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(
                flex: 2,
              ),
              Text(
                "Update Your Profile",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
              ),
              Spacer(
                flex: 2,
              ),
              Row(
                children: [
                  Spacer(
                    flex: 1,
                  ),
                  GestureDetector(
                      onTap: () {
                        bottomSheetAddPhotos(context);
                      },
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: widget.imgUrl != null
                              ? Container(
                                  height: screenHeight(context, dividedBy: 4),
                                  width: screenHeight(context, dividedBy: 4),
                                  decoration: BoxDecoration(
                                      color: Constants.kitGradients[2]
                                          .withOpacity(0.6),
                                      borderRadius: BorderRadius.circular(120),
                                      image: DecorationImage(
                                          image: NetworkImage(widget.imgUrl),
                                          fit: BoxFit.fill)),
                                )
                              : imagePicked != null
                                  ? Container(
                                      height:
                                          screenHeight(context, dividedBy: 4),
                                      width:
                                          screenHeight(context, dividedBy: 4),
                                      decoration: BoxDecoration(
                                          color: Constants.kitGradients[2]
                                              .withOpacity(0.1),
                                          borderRadius:
                                              BorderRadius.circular(120),
                                          image: DecorationImage(
                                              image: FileImage(imagePicked),
                                              fit: BoxFit.fill)),
                                    )
                                  : Container(
                                      height:
                                          screenHeight(context, dividedBy: 4),
                                      width:
                                          screenHeight(context, dividedBy: 4),
                                      decoration: BoxDecoration(
                                        color: Constants.kitGradients[2]
                                            .withOpacity(0.6),
                                        borderRadius:
                                            BorderRadius.circular(120),
                                      ),
                                      child: Center(
                                        child: Icon(
                                          Icons.add_a_photo_outlined,
                                          color: Constants.kitGradients[0],
                                          size: screenHeight(context,
                                              dividedBy: 8),
                                        ),
                                      ),
                                    ))),
                  Spacer(
                    flex: 1,
                  ),
                ],
              ),
              Spacer(
                flex: 1,
              ),
              CTextField(
                  isNumber: false,
                  label: "First Name ",
                  textEditingController: firstNameTextEditingController),
              Spacer(
                flex: 1,
              ),
              CTextField(
                  isNumber: false,
                  label: "Last Name",
                  textEditingController: lastNameTextEditingController),

              // Spacer(
              //   flex: 1,
              // ),
              // FormFieldUserDetails(
              //     labelText: "Last Name",
              //     hintText: "Doe",
              //     textEditingController: lastNameTextEditingController),
              Spacer(
                flex: 4,
              ),
              CButton(
                isLoading: loading,
                onPressed: () async {
                  if (firstNameTextEditingController != null &&
                      lastNameTextEditingController.text != null &&
                      imagePicked != null) {
                    setState(() {
                      loading = true;
                    });
                    userBloc.updateUserDetails(
                        updateUserDetailsRequest: UpdateUserDetailsRequestModel(
                            firstName: firstNameTextEditingController.text,
                            lastName: lastNameTextEditingController.text,
                            phoneNumber: ObjectFactory().appHive.getEmail(),
                            profileImage: imagePicked));
                  } else {}
                  // if (imagePicked != null) {
                },
                title: "Update",
              ),
              Spacer(
                flex: 2,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void bottomSheetAddPhotos(context) {
    showModalBottomSheet<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
                color: Colors.white,
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 10)),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              // color: Constants.kitGradients[3],
                              // fontFamily: "Poppins",
                              fontSize: 12,
                            ),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                            onTap: () {
                              getImageCamera();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.camera,
                              size: 50,
                              color: Constants.kitGradients[2].withOpacity(0.6),
                            )),
                        GestureDetector(
                            onTap: () {
                              getImageGallery();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.image,
                              size: 50,
                              color: Constants.kitGradients[2].withOpacity(0.6),
                            )),
                      ],
                    ),
                  ],
                ));
          });
        });
  }
}
