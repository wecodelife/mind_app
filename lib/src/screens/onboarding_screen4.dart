import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/slide_transition.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swipedetector/swipedetector.dart';

class OnBoardingScreen4 extends StatefulWidget {
  @override
  _OnBoardingScreen4State createState() => _OnBoardingScreen4State();
}

class _OnBoardingScreen4State extends State<OnBoardingScreen4> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Color(0xffD9E7FD),
    ),
    child:SafeArea(
      child: SwipeDetector(
        onSwipeLeft: () {
          Navigator.of(context).pushAndRemoveUntil(
              SlideRightRoute(page: BottomBar()), (Route route) => false);
        },
        child: Scaffold(
            // extendBodyBehindAppBar: true,
            // backgroundColor: Colors.amber,
            body: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 1),
          child: OnBoardingContent(
            head: "Daily Alert!",
            index: 3,
            data: "Get daily job alerts to find the right job.",
            image: "assets/images/on_boarding4.svg",
            buttonData: "Let's Start",
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  SlideRightRoute(page: BottomBar()), (Route route) => false);
              // Navigator.push(context, SlideRightRoute(page: HomePage()));
              //push(context, LoginPage());
            },
          ),
        )),
      ),
    ));
  }
}
