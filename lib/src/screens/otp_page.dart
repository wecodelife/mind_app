import 'dart:async';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/screens/onboarding_screen1.dart';
import 'package:app_template/src/screens/update_profile_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/animated_otpfeilds.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OtpPage extends StatefulWidget {
  String verificationId;
  String phoneNumber;
  OtpPage({this.verificationId, this.phoneNumber});
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController _otpTextEditingController = new TextEditingController();
  bool isLoading = false;
  String userId;
  String userName;
  String appSignature = "{{ app signature }}";
  String _code = "";
  bool keyBoard = false;

  UserBloc userBloc = new UserBloc();

  var keyboardVisibilityController = KeyboardVisibilityController();
  StreamSubscription<bool> keyBoardController;

  @override
  void dispose() {
    keyBoardController.cancel();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    print("Id" + widget.verificationId);
    super.initState();

    keyBoardController = keyBoardController =
        keyboardVisibilityController.onChange.listen((bool visible) {
      keyBoard = visible;
      setState(() {});
    });

    userBloc.loginResponse.listen((event) async {
      print(event.message.toString());
      if (event.status == 200) {
        setState(() {
          isLoading = false;
          // ObjectFactory().appHive.putPhoneNumber(phoneNumber:event.data.userData.phoneNumber.toString());
          print("User Phone number"+event.data.userData.phoneNumber.toString());
        });
        print("Phone Number added to Hive " + ObjectFactory().appHive.getPhoneNumber());
        print("Subscription Plann  " +
            event.data.userData.userSubscriptionPlan.toString());
        event.data.userData.userSubscriptionPlan == null
            ? ObjectFactory().appHive.putOrderStatus(status: false)
            : ObjectFactory().appHive.putOrderStatus(status: true);
        await ObjectFactory()
            .appHive
            .putUserId(userId: event.data.userData.uid);
        ObjectFactory().appHive.putId(id: event.data.userData.id);

        // print(ObjectFactory().appHive.getUserId());

        ObjectFactory().appHive.putToken(token: event.data.token);

        if (event.data.userData.firstName != "") {
          ObjectFactory().appHive.putName(
              name: event.data.userData.firstName +
                  " " +
                  event.data.userData.lastName);
          ObjectFactory().appHive.putPhotoUrl(
              Urls.imageBaseUrl + event.data.userData.profileImage);
          ObjectFactory().appHive.putSignedInCheck(true);

          pushAndRemoveUntil(context, OnBoardingScreen(), false);
        } else {
          // setState(() {
          //   isLoading = false;
          // });
          pushAndRemoveUntil(context, UpdateProfilePage(), false);
        }
      } else {
        print("Network Error");
        print(event.status);
      }
    });
    // TODO: implement initState
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 16),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                  height: keyBoard == true
                      ? screenHeight(context, dividedBy: 20)
                      : screenHeight(context, dividedBy: 6)),
              Center(
                child: Container(
                  width: screenWidth(context, dividedBy: 1.8),
                  height: screenHeight(context, dividedBy: 7),
                  child: SvgPicture.asset(
                    "assets/images/mind_logo.svg",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 10),
              ),
              Row(
                children: [
                  Text(
                    "Login to ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                      color: Constants.kitGradients[7],
                      fontFamily: "SofiaProRegular",
                    ),
                  ),
                  Text(
                    "Upyou !",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Constants.kitGradients[2],
                      fontFamily: "Montserrat-ExtraBold",
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 90),
              ),
              Text(
                "Please enter the One Time Password Send to the given Mobile Number, and Login to UpYou",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Constants.kitGradients[7].withOpacity(0.4),
                  fontFamily: "SofiaProRegular",
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              animated_otp_fields(
                _otpTextEditingController,
                fieldHeight: screenHeight(context, dividedBy: 12),
                fieldWidth: screenWidth(context, dividedBy: 9),
                OTP_digitsCount: 6,
                keyboardType: TextInputType.number,
                animation: TextAnimation.Fading,
                border:
                    Border.all(width: 1.5, color: Constants.kitGradients[2]),
                borderRadius: BorderRadius.all(Radius.circular(6)),
                contentPadding:
                    EdgeInsets.only(top: screenHeight(context, dividedBy: 100)),
                forwardCurve: Curves.linearToEaseOut,
                textStyle: TextStyle(
                  color: Constants.kitGradients[7],
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
                onFieldSubmitted: (text) {
                  verifyOtp(text, widget.verificationId, widget.phoneNumber);
                },
                spaceBetweenFields: 14,
                autoFocus: true,
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 10)),
                child: CButton(
                  onPressed: () {
                    setState(() {
                      isLoading = true;
                    });
                    if (widget.verificationId != null) {
                      verifyOtp(_otpTextEditingController.text,
                          widget.verificationId, widget.phoneNumber);
                    } else {
                      showToast("Try Again with your phone number.");
                    }
                  },
                  title: "Login",
                  isLoading: isLoading,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void verifyOtp(String otp, String verificationId, String phoneNumber) {
    setState(() {
      isLoading = true;
    });
    // emit(AuthLoading());
    AuthCredential authCredential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: otp,
    );
    FirebaseAuth.instance
        .signInWithCredential(authCredential)
        .then((value) async {
      userId = value.user.uid;

      // ObjectFactory().appHive.putName(name: null);
      // ObjectFactory().appHive.putEmail(value.user.phoneNumber);
      userBloc.login(
          loginRequest: LoginRequestModel(
        uid: userId,
      ));

      print("success");
      // pushAndRemoveUntil(context, BottomBar(), false);
    }).catchError((onError) {
      setState(() {
        isLoading = false;
      });

      showToast("Error:" + onError.toString());
      // showSnackbar(onError.toString());

      // emit(AuthError());
    });
  }

  // void showSnackbar(String message) {
  //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //     backgroundColor: Colors.grey.withOpacity(0.6),
  //     content: Text(
  //       message,
  //       style: TextStyle(color: Colors.black),
  //     ),
  //     duration: const Duration(seconds: 5),
  //   ));
  // }
}
