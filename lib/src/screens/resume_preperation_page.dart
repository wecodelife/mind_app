import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_resume_preparation.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/resume_video_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/resume_prep_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'dart:async';

class ResumePreperationPage extends StatefulWidget {
  final int id;
  final String title;
  // final bool paymentStatus;
  const ResumePreperationPage(
      {Key key, this.id, this.title,
        // this.paymentStatus
      })
      : super(key: key);

  @override
  _ResumePreperationPageState createState() => _ResumePreperationPageState();
}

class _ResumePreperationPageState extends State<ResumePreperationPage> {
  UserBloc userBloc = UserBloc();

  List<String> sampleVideos = [
    'https://assets.mixkit.co/videos/preview/mixkit-forest-stream-in-the-sunlight-529-large.mp4',
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
  ];

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    // pop(context);
    // push(context, HomePage());
    return true;
  }

  @override
  void initState() {
    userBloc.getResumePreparation(videoId: widget.id.toString());
    userBloc.getGetResumePreparationResponse
        .listen((event) {})
        .onError((event) {});

    // userBloc.getSingleUserSubscriptionPlans(
    //     userId: ObjectFactory().appHive.getId());
    // userBloc.getSingleUserSubscriptionPlan.listen((event) {
    //   Future.delayed(
    //       Duration.zero,
    //       () => event.status == "Success"
    //           ? print("Subscribed")
    //           : showSubscriptionAlertDialog(context));
    //
    //   // if (event.status == 200 || event.status == 201) {
    //   //   print("Payment Successful");
    //   //   print(event.data.status);
    //   //   showToast("Your payment is Successful");
    //   //   // ObjectFactory().appHive.putOrderStatus(status: true);
    //   //   push(context, HomePage(paid :true));
    //   // }
    // });
    // ObjectFactory().appHive.getOrderStatus() == true
    //     ? print("Subscribed")
    //     : print(" no subscription");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark),
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              toolbarHeight: screenHeight(context, dividedBy: 15),
              elevation: 0,
              leading: Container(),
              actions: [
                Column(
                  children: [
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 150),
                    // ),
                    CAppBar(
                      title: widget.title,
                      color: Colors.white,
                      isWhite: false,
                      onPressedLeftIcon: () {
                        pushAndRemoveUntil(context, BottomBar(), false);
                      },
                    ),
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 150),
                    // ),
                  ],
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  children: <Widget>[
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 30),
                    // ),
                    StreamBuilder<GetResumePreparationResponse>(
                        stream: userBloc.getGetResumePreparationResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasError
                              ? Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 1.2),
                                  child: Center(
                                      child: Text(
                                    snapshot.error.toString(),
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.blueGrey,
                                    ),
                                  )),
                                )
                              : snapshot.hasData
                                  ? snapshot.data.data.length >= 1
                                      ? Container(
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          child: ListView.builder(
                                              padding: EdgeInsets.only(
                                                  top: screenHeight(context,
                                                      dividedBy: 50)),
                                              shrinkWrap: true,
                                              // scrollDirection: Axis.vertical,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount:
                                                  snapshot.data.data.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return Column(
                                                  children: [
                                                    ResumePreperationListItem(
                                                      vdoUrl:
                                                      // sampleVideos[index],
                                                      // "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
                                                      //     "http://3.142.247.21:8000" +
                                                              snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .videoUrl,
                                                      // 'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4'), .split("=")
                                                      //                                                             .last
                                                      looping: true,
                                                      videoIndex: index,
                                                      description: snapshot
                                                          .data
                                                          .data[index]
                                                          .description,
                                                      onTap: () {
                                                        pushAndReplacement(
                                                            context,
                                                            ResumeVideoPage(
                                                              id: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .id,
                                                              description: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .description,
                                                              title: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .title,
                                                              videoIndex:index,
                                                              url:
                                                              // sampleVideos[index],
                                                              // "http://3.142.247.21:8000" +
                                                                  snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .videoUrl,
                                                              button:
                                                                  widget.id == 0
                                                                      ? true
                                                                      : false,
                                                            ),);
                                                      },
                                                      // "Lorem ipsum dolor sit amet, consectetur piscing elit, sed do eiusmod tempor incididunt  labore et dolore magna aliqua. Ut enim ad minim am, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                                                    ),
                                                    SizedBox(
                                                      height: screenHeight(
                                                          context,
                                                          dividedBy: 70),
                                                    )
                                                  ],
                                                );
                                              }),
                                        )
                                      : Container(
                                          height: screenHeight(context,
                                              dividedBy: 1.3),
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          child: Center(
                                            child: Text("No Videos Available",
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.blueGrey,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                          ),
                                        )
                                  : Container(
                                      height:
                                          screenHeight(context, dividedBy: 1.3),
                                      width: screenWidth(context, dividedBy: 1),
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  Constants.kitGradients[2]),
                                        ),
                                      ),
                                    );
                        }),
                    SizedBox(
                      height: screenHeight(
                          context,
                          dividedBy: 30),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
// Container(
//     child: ListView.builder(
//   itemCount: 3,
//   shrinkWrap: true,
//   physics: NeverScrollableScrollPhysics(),
//   itemBuilder: (BuildContext context, int index) {
//     return Column(children: [
//       ResumePreperationListItem(
//         videoPlayerController: VideoPlayerController.network(
//             'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4'),
//         looping: true,
//         description:
//             "Lorem ipsum dolor sit amet, consectetur piscing elit, sed do eiusmod tempor incididunt  labore et dolore magna aliqua. Ut enim ad minim am, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
//       ),
//     ]);
//   },
// )),
