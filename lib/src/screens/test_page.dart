import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_questions.dart';
import 'package:app_template/src/models/update_answer_request.dart';
import 'package:app_template/src/screens/apptitude.dart';
import 'package:app_template/src/screens/test_complete.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/radio_button.dart';
import 'package:flutter/material.dart';

class TestPage extends StatefulWidget {
  final int id;
  final bool firstTime;

  TestPage({
    this.id,
    this.firstTime,
  });
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  int value;
  bool Answered = false;

  UserBloc userBloc = UserBloc();
  String message = "";
  List<String> answers = [
    "Because I need a job",
    "Because its the only chance the employer gets to know your skills",
    "Because my friend has one."
  ];
  bool clickable = true;
  String answer = "";
  bool quitTest = false;
  bool noQuestions = false;
  bool answerLoading = true;
  Future<bool> _willPopCallback() async {
    if (noQuestions == true) {
      push(context, ApptitudePage());
    } else {
      cancelAlertBox(
          context: context,
          button1Name: "Yes",
          button2Name: "No",
          msg: "Do you wish to quit test?",
          titlePadding: 3,
          text2: "",
          text1: "",
          contentPadding: 20,
          insetPadding: 2.7,
          onPressedNo: () {
            pop(context);
          },
          onPressedYes: () {
            quitTest = true;
            userBloc.deleteQuestionAnswer();
          });

      return true;
    }
  }

  @override
  void initState() {
    // count = widget.count + 1;
    userBloc.getQuestions(id: widget.id);
    userBloc.getQuestionsResponse.listen((event) {
      if (event.status == 200) {
        if (event.data != null) {
          if (event.data.questionNo > 10) {
            pushAndReplacement(context, TestCompletePage());
          } else {
            // if (widget.firstTime == true && event.data.questionNo > 1) {
            //   cancelAlertBox(
            //       context: context,
            //       msg: "Do you wish to  resume the test?",
            //       button1Name: "Yes",
            //       button2Name: "No",
            //       titlePadding: 3,
            //       text2: "",
            //       text1: "",
            //       contentPadding: 20,
            //       insetPadding: 2.7,
            //       onPressedNo: () {
            //         userBloc.deleteQuestionAnswer();
            //       },
            //       onPressedYes: () {
            //         pushAndReplacement(
            //             context,
            //             TestPage(
            //               id: 1,
            //               firstTime: false,
            //             ));
            //       });
            // }
          }
          // count = widget.count + 1;
          for (int i = 0; i < event.data.options.length; i++) {
            if (event.data.options[i].isAnswer == true) {
              answer = event.data.options[i].option;
              // count = widget.count + 1;
            }
          }
        } else {
          if (widget.firstTime == false) {
            pushAndReplacement(context, TestCompletePage());
          } else {
            noQuestions = true;
            setState(() {
              message = "No Questions Available.";
            });
          }
        }
      }
    }).onError((event) {
      noQuestions = true;
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
      // print("Error" + event.toString());
      // showToast(event.toString());
    });

    userBloc.deleteQuestionAnswerResponse.listen((event) {
      if (event.status == 200) {
        if (quitTest == true) {
          pushAndReplacement(context, ApptitudePage());
        } else {
          pushAndReplacement(
            context,
            TestPage(
              firstTime: false,
              id: widget.id,
            ),
          );
        }
      }
    }).onError((event) {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            leading: Container(),
            actions: [
              CAppBar(
                  title: "Test",
                  color: Colors.white,
                  isWhite: false,
                  onPressedLeftIcon: () {
                    if (noQuestions == true) {
                      push(context, ApptitudePage());
                    } else {
                      cancelAlertBox(
                          context: context,
                          button1Name: "Yes",
                          button2Name: "No",
                          msg: "Do you wish to quit test?",
                          titlePadding: 3,
                          text2: "",
                          text1: "",
                          contentPadding: 20,
                          insetPadding: 2.7,
                          onPressedNo: () {
                            pop(context);
                          },
                          onPressedYes: () {
                            quitTest = true;
                            userBloc.deleteQuestionAnswer();
                          });
                    }
                  })
            ],
          ),
          body: SingleChildScrollView(
            child: StreamBuilder<GetQuestionResponse>(
                stream: userBloc.getQuestionsResponse,
                builder: (context, snapshot) {
                  return snapshot.hasError
                      ? Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 1.2),
                          child: Center(
                              child: Text(
                            snapshot.error.toString(),
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.blueGrey,
                            ),
                          )))
                      : snapshot.hasData
                          ? snapshot.data.data != null
                              ? snapshot.data.data.questionNo <= 10
                                  ? snapshot.data.data.questionNo > 1 &&
                                          widget.firstTime == true
                                      ? Container(
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          height: screenHeight(context,
                                              dividedBy: 1),
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  new AlwaysStoppedAnimation<
                                                          Color>(
                                                      Constants
                                                          .kitGradients[2]),
                                            ),
                                          ),
                                        )
                                      : Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              //questionBox
                                              Container(
                                                width: screenWidth(context,
                                                    dividedBy: 1),
                                                height: screenHeight(context,
                                                    dividedBy: 6.5),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 15),
                                                color: Color(0xffC3C5CB),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      width: 23,
                                                      padding:
                                                          EdgeInsets.all(4),
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color: Constants
                                                              .kitGradients[2]),
                                                      child: Text(
                                                        snapshot.data.data
                                                            .questionNo
                                                            .toString(),
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 12,
                                                            fontFamily:
                                                                "SofiaProRegular"),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 30),
                                                    ),
                                                    Container(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 1.5),
                                                      //height: screenHeight(context, dividedBy: 9),
                                                      child: Text(
                                                        // "What is a resume? and why is it so importance to make one?",
                                                        snapshot
                                                            .data.data.question,
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "SofiaProRegular",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              //selection label
                                              Container(
                                                //padding: EdgeInsets.symmetric(horizontal: 15),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    SizedBox(
                                                      height: screenHeight(
                                                          context,
                                                          dividedBy: 30),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 15),
                                                      child: Text(
                                                        "SELECT YOUR ANSWER",
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            color: Color(
                                                                0xff888D99),
                                                            fontFamily:
                                                                "SofiaProRegular"),
                                                      ),
                                                    ),
                                                    snapshot.data.data.options
                                                                .length ==
                                                            0
                                                        ? Container(
                                                      height: screenHeight(context,
                                                          dividedBy: 2),
                                                      width: screenWidth(context,
                                                          dividedBy: 1),
                                                      child: Center(
                                                        child: Text("No options Available",
                                                            style: TextStyle(
                                                                fontSize: 20,
                                                                color: Colors.blueGrey,
                                                                fontWeight:
                                                                FontWeight.w600)),
                                                      ),
                                                    )
                                                        : RadioButtonTile(
                                                            id: widget.id,
                                                            count: snapshot
                                                                .data
                                                                .data
                                                                .questionNo,
                                                            answer: answer,
                                                            option1:
                                                                // "Because its the only chance the employer gets to know your skills",
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[0],
                                                            option2:
                                                                // "Because my friend has one.",
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[1],
                                                            option3: snapshot
                                                                .data
                                                                .data
                                                                .options[2],
                                                            // "Because I need a job",
                                                            isRightAnswer1:
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[0]
                                                                    .isAnswer,
                                                            isRightAnswer2:
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[1]
                                                                    .isAnswer,
                                                            isRightAnswer3:
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[2]
                                                                    .isAnswer,
                                                            answerExplanation1:
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[0]
                                                                    .answerExplanation,
                                                            answerExplanation2:
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[1]
                                                                    .answerExplanation,
                                                            answerExplanation3:
                                                                snapshot
                                                                    .data
                                                                    .data
                                                                    .options[2]
                                                                    .answerExplanation,

                                                            onChanged: (value) {
                                                              print(ObjectFactory()
                                                                  .appHive
                                                                  .getExamId());
                                                              userBloc.updateAnswer(
                                                                  updateAnswerRequest: UpdateAnswerRequest(
                                                                      answer:
                                                                          value,
                                                                      question: snapshot
                                                                          .data
                                                                          .data
                                                                          .id,
                                                                      examId: ObjectFactory()
                                                                          .appHive
                                                                          .getExamId()));
                                                              print(value
                                                                  .toString());
                                                            },
                                                          ),
                                                    SizedBox(
                                                      height: screenHeight(
                                                          context,
                                                          dividedBy: 3.5),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                  : Container(
                                      width: screenWidth(context, dividedBy: 1),
                                      height:
                                          screenHeight(context, dividedBy: 1),
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  Constants.kitGradients[2]),
                                        ),
                                      ),
                                    )
                              : Container(
                                  height: screenHeight(context, dividedBy: 1.3),
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Center(
                                    child: Text(message,
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.blueGrey,
                                            fontWeight: FontWeight.w600)),
                                  ),
                                )
                          : Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 1),
                              child: Center(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Constants.kitGradients[2]),
                                ),
                              ),
                            );
                }),
          ),
        ),
      ),
    );
  }
}
