import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/book_service_request.dart';
import 'package:app_template/src/models/get_service_response.dart';
import 'package:app_template/src/models/order_plan_request_model.dart';
import 'package:app_template/src/screens/booking_complete.dart';
import 'package:app_template/src/screens/payment_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/check_box.dart';
import 'package:app_template/src/widgets/textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/utils/object_factory.dart';

class BookingPage extends StatefulWidget {
  final String servicePrice;
  BookingPage({this.servicePrice});
  @override
  _BookingPageState createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  List<String> _status = ['Student', 'Working']; // Option 2
  String _selectedStatus;
  UserBloc userBloc = UserBloc();
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController lastNameTextEditingController =
      new TextEditingController();
  TextEditingController numberTextEditingController =
      new TextEditingController();
  List<int> idList = [];
  bool isLoading = false;
  bool email;
  bool phoneNumber;
  TextEditingController dataTextEditingController = TextEditingController();

  checkDataEntered() {
    if (nameTextEditingController != null &&
        lastNameTextEditingController != null &&
        _selectedStatus != null &&
        numberTextEditingController != null &&
        idList.isNotEmpty) {
      if (numberTextEditingController.text.length == 10) {
        setState(() {
          isLoading = true;
        });
        // print("Missing data " + dataTextEditingController.text);
        if (email == true || phoneNumber == true) {
          showAddEmailAlert(
            context,
            isEmail: email == true ? true : false,
            textEditingController: dataTextEditingController,
            onPressed: () {
              phoneNumber == true
                  ? ObjectFactory().appHive.putPhoneNumber(
                      phoneNumber: dataTextEditingController.text)
                  : email == true
                      ? ObjectFactory()
                          .appHive
                          .putEmail(dataTextEditingController.text)
                      : setState(() {});
              email == true
                  ? print("Email missing, added to hive " +
                      ObjectFactory().appHive.getEmail())
                  : print("Email is not missing");
              phoneNumber == true
                  ? print("PhoneNumber missing, added to hive " +
                      ObjectFactory().appHive.getPhoneNumber())
                  : print("Phone Number is not missing");

              userBloc.orderPlans(
                  orderPlanRequest: OrderPlanRequest(
                      customerEmail: ObjectFactory().appHive.getEmail() == null ? dataTextEditingController.text : ObjectFactory().appHive.getEmail(),
                      customerPhone:
                          ObjectFactory().appHive.getPhoneNumber().toString() == null ? dataTextEditingController.text :  ObjectFactory().appHive.getPhoneNumber().toString(),
                      customerName: ObjectFactory().appHive.getName(),
                      orderAmount: widget.servicePrice,

                      orderCurrency: "INR",
                      orderNote: "test"));
            },
          );
        } else if (email != true && phoneNumber != true) {
          print("___________________________________________");
          print("Missing data " + dataTextEditingController.text);
          userBloc.orderPlans(
              orderPlanRequest: OrderPlanRequest(
                  customerEmail: ObjectFactory().appHive.getEmail(),
                  customerPhone: ObjectFactory().appHive.getPhoneNumber(),
                  customerName: ObjectFactory().appHive.getName(),
                  orderAmount: widget.servicePrice,
                  orderCurrency: "INR",
                  orderNote: "test"));
        }
      } else {
        showToast("Please enter a valid number");
      }
    } else {
      showToast("Please fill all the fields");
    }
  }

  @override
  void initState() {
    setState(() {
      email = ObjectFactory().appHive.getEmail() == null ? true : false;
      phoneNumber =
          ObjectFactory().appHive.getPhoneNumber() == null ? true : false;
    });
    ObjectFactory().appHive.getEmail() == null
        ? print("Email Id No value ")
        : print("Email id " + ObjectFactory().appHive.getEmail());
    print(
        "Phone Number  " + ObjectFactory().appHive.getPhoneNumber().toString());
    print("Service price "+ widget.servicePrice);
    userBloc.getServices();
    userBloc.getServiceResponse.listen((event) {}).onError((event) {
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
    });

    userBloc.orderPlanResponse.listen((event) async {
      if (event.status == 200 || event.status == 201) {
        print("Booking order response loaded Successfully");
        print("Payment Link  " + event.data.paymentLink);
        print("OrderId  " + event.data.orderId);
        if (event.data.paymentLink != null) {
          push(
            context,
            PaymentPage(
              url: event.data.paymentLink,
              orderId: event.data.orderId,
              bookingDate: DateTime.now(),
              bookingPrice: widget.servicePrice,
              currentStatus: _selectedStatus,
              firstName: nameTextEditingController.text,
              lastName: lastNameTextEditingController.text,
              whatsAppNumber: numberTextEditingController.text,
              serviceList: idList,
              subscription: false,
              // id:widget.id
            ),
          );
        }
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.white,
            statusBarIconBrightness: Brightness.dark),
        child: SafeArea(
          child: Scaffold(
            // resizeToAvoidBottomPadding: false,
            backgroundColor: Colors.white,
            appBar: AppBar(
              elevation: 0,
              leading: Container(),
              actions: [
                CAppBar(
                  title: "Booking",
                  color: Colors.white,
                  isWhite: false,
                  onPressedLeftIcon: () {
                    pop(context);
                  },
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                //color: Colors.blueAccent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CTextField(
                      label: "FIRST NAME",
                      isNumber: false,
                      textEditingController: nameTextEditingController,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    CTextField(
                      label: "LAST NAME",
                      isNumber: false,
                      textEditingController: lastNameTextEditingController,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    //DropDown List
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 15),
                        //alignment: Alignment.center,
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 6),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 40),
                            ),
                            Text(
                              "CURRENT STATUS",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff888D99),
                                  fontFamily: "SofiaProRegular"),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 60),
                            ),
                            Container(
                              // width: screenWidth(context, dividedBy: .9),
                              child: DropdownButton(
                                underline: Container(
                                  height: 1,
                                  color: Colors.grey,
                                ),
                                hint: Text('Please choose current Status'),
                                value: _selectedStatus,
                                onChanged: (newValue) {
                                  setState(() {
                                    _selectedStatus = newValue;
                                  });
                                },
                                items: _status.map((status) {
                                  return DropdownMenuItem(
                                    child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(9),
                                        ),
                                        height: screenHeight(context,
                                            dividedBy: 40),
                                        width: screenWidth(context,
                                            dividedBy: 1.2),
                                        child: Text(status)),
                                    value: status,
                                  );
                                }).toList(),
                              ),
                            )
                          ],
                        )),

                    SizedBox(
                      height: screenHeight(context, dividedBy: 60),
                    ),
                    CTextField(
                      label: "WHATSAPP NUMBER",
                      isNumber: true,
                      textEditingController: numberTextEditingController,
                    ),
                    Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                        child: Text(
                            "Our service rep will contact you via whatsapp",
                            style: TextStyle(
                                fontSize: 11,
                                color: Color(0xff888D99),
                                fontFamily: "SofiaProRegular"))),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Text("Select all the services you need :",
                          style: TextStyle(
                              fontFamily: "SofiaProRegular",
                              fontSize: 15,
                              color: Color(0xff2C2E39))),
                    ),
                    StreamBuilder<GetServicesResponse>(
                        stream: userBloc.getServiceResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasError
                              ? Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height:
                                      screenHeight(context, dividedBy: 2.25),
                                  child: Center(
                                      child: Text(
                                    snapshot.error.toString(),
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.blueGrey,
                                    ),
                                  )),
                                )
                              : snapshot.hasData
                                  ? Container(
                                      // height: screenHeight(context, dividedBy: 4),
                                      child: ListView.builder(
                                          // padding: EdgeInsets.only(
                                          //     top: screenHeight(context, dividedBy: 20)),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          itemCount: snapshot.data.data.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                CCheckBox(
                                                  label: snapshot
                                                      .data.data[index].name,
                                                  onPressed: () {
                                                    if (idList.contains(snapshot
                                                        .data.data[index].id)) {
                                                      idList.remove(snapshot
                                                          .data.data[index].id);
                                                    } else {
                                                      idList.add(snapshot
                                                          .data.data[index].id);
                                                    }
                                                    print(DateTime.now()
                                                        .toString());
                                                  },
                                                  // "Mock Interview- Basic",
                                                ),
                                                SizedBox(
                                                  height: screenHeight(context,
                                                      dividedBy: 100),
                                                ),
                                              ],
                                            );
                                          }),
                                    )
                                  : Container(
                                      height:
                                          screenHeight(context, dividedBy: 1.3),
                                      width: screenWidth(context, dividedBy: 1),
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  Constants.kitGradients[2]),
                                        ),
                                      ),
                                    );
                        }),

                    CButton(
                      isLoading: isLoading,
                      onPressed: () {
                        checkDataEntered();
                      },
                      title: "Submit",
                    ),
                    // Container(
                    //   width: screenWidth(context, dividedBy: 1),
                    //   // height: screenHeight(context, dividedBy: 6),
                    //   child: Center(
                    //     child: CButton(
                    //       onPressed: () {
                    //         push(context, BookingCompletePage());
                    //       },
                    //       title: "Submit",
                    //     ),
                    //   ),
                    // ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
