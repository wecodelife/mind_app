import 'package:app_template/src/models/get_service_response.dart';
import 'package:app_template/src/screens/booking_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ResumePreparation extends StatefulWidget {
  final Datum serviceData;
  ResumePreparation({this.serviceData});
  @override
  _ResumePreparationState createState() => _ResumePreparationState();
}

class _ResumePreparationState extends State<ResumePreparation> {

  void initState() {
    print("Service Images List");
    print("images url " + widget.serviceData.imageUrl);
    print("Service offerprice "+ widget.serviceData.offerPrice);
    print("Service price "+ widget.serviceData.price);



    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark
    ),
    child:Scaffold(
      body: Container(
        width: screenWidth(context, dividedBy: 1),
        height:screenHeight(context, dividedBy:1),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      child: CachedNetworkImage(
                        height: screenHeight(context, dividedBy: 2),
                        imageUrl:
                         widget.serviceData.imageUrl,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        placeholder: (context, url) => Center(
                          child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                Constants.kitGradients[28],
                              )),
                        ),
                        errorWidget: (context, url, error) =>
                            Icon(Icons.error),
                      ),
                    ),

                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      //height:screenHeight(context, dividedBy:3),
                      //color:Colors.red,

                      // child: Stack(
                      //   children: [
                      //     ClipRRect(
                      //       child: CachedNetworkImage(
                      //         height: screenHeight(context, dividedBy: 2),
                      //         imageUrl:
                      //             Urls.imageBaseUrl + widget.serviceData.imageUrl,
                      //         imageBuilder: (context, imageProvider) => Container(
                      //           decoration: BoxDecoration(
                      //             image: DecorationImage(
                      //               image: imageProvider,
                      //               fit: BoxFit.fill,
                      //             ),
                      //           ),
                      //         ),
                      //         placeholder: (context, url) => Center(
                      //           child: CircularProgressIndicator(
                      //               valueColor: AlwaysStoppedAnimation<Color>(
                      //             Constants.kitGradients[28],
                      //           )),
                      //         ),
                      //         errorWidget: (context, url, error) =>
                      //             Icon(Icons.error),
                      //       ),
                      //     ),
                      //     CAppBar(
                      //         title: widget.serviceData.name,
                      //         color: Colors.transparent,
                      //         onPressedLeftIcon: () {
                      //           pop(context);
                      //         },
                      //         isWhite: true),
                      //   ],
                      // ),
                    ),
                    Container(
                        width: screenWidth(context, dividedBy: 1),
                        // height: screenHeight(context, dividedBy: 1),
                        color: Colors.white,
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              Text(
                                widget.serviceData.description,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: "SofiaProRegular"),
                              ),
                              // SizedBox(
                              //   height: screenHeight(context, dividedBy: 30),
                              // ),
                              // Text(
                              //   "Lorem ipsum dolor sit amet, consectetur piscing elit, sed do eiusmod tempor incididunt  labore et dolore magna aliqua. Ut enim ad minim am, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                              //   style: TextStyle(
                              //       fontSize: 14,
                              //       fontWeight: FontWeight.w400,
                              //       fontFamily: "SofiaProRegular"),
                              // ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 100),
                              ),
                              Text(
                                "Things we help you with",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "SofiaProRegular"),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              Container(
                                width: screenWidth(context, dividedBy: 1),
                                child: Row(children: [
                                  Text(
                                    widget.serviceData.name.split("-").first,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "SofiaProRegular"),
                                  ),
                                  Spacer(),
                                  Text(
                                    widget.serviceData.plan,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "SofiaProRegular"),
                                  )
                                ]),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: widget.serviceData.content.data.length,
                                  itemBuilder: (BuildContext context, int index) {
                                    return Container(
                                      child: Column(
                                        children: [
                                          Row(children: [
                                            Container(
                                              width: 23,
                                              padding: EdgeInsets.all(4),
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: Constants.kitGradients[2]),
                                              child: Text(
                                                (index + 1).toString(),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 12,
                                                    fontFamily: "SofiaProRegular"),
                                              ),
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 30),
                                            ),
                                            Text(
                                              widget.serviceData.content.data[index]
                                                  .entries.single.value
                                                  .toString(),
                                              style: TextStyle(
                                                  //color: Colors.white,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: "SofiaProRegular"),
                                            ),
                                          ]),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 150),
                                          ),
                                        ],
                                      ),
                                    );
                                  }),
                            ])),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 15),
                      alignment: Alignment.center,
                      color: Color(0xffECECEC),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Rs ${widget.serviceData.offerPrice}",
                            style: TextStyle(
                                color: Constants.kitGradients[2],
                                fontWeight: FontWeight.bold,
                                fontSize: 28,
                                fontFamily: "SofiaProRegular"),
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 30),
                          ),
                          Text(
                            "Rs " + widget.serviceData.price,
                            style: TextStyle(
                                decoration: TextDecoration.lineThrough,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                fontFamily: "SofiaProRegular"),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    CButton(
                      title: "Book Now",
                      onPressed: () {
                        push(
                            context,
                            BookingPage(
                              servicePrice: widget.serviceData.offerPrice,
                            ));
                      },
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top:screenHeight(context, dividedBy: 15),
              left:screenWidth(context, dividedBy:30),
              child: GestureDetector(
                onTap:(){
                  pop(context);
                },
                child:Icon(Icons.arrow_back, color:Colors.white, size:30)
              )
            ),
          ],
        ),
      ),
    ));
  }
}
