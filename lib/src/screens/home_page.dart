import 'package:app_template/src/screens/apptitude.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/resume_preperation_page.dart';
import 'package:app_template/src/screens/resume_video_list_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/home_page_tile.dart';
import 'package:app_template/src/widgets/home_tile1.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:app_template/src/widgets/image_slider.dart';
import 'package:app_template/src/models/get_slider_image_response.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:shimmer/shimmer.dart';
import 'package:awesome_loader/awesome_loader.dart';

class HomePage extends StatefulWidget {
  // final bool paid;
  // HomePage({this.paid = false});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> images = [
    "https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTZ8fGxlYXJuaW5nfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1501504905252-473c47e087f8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTl8fGxlYXJuaW5nfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1599008633840-052c7f756385?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjJ8fGxlYXJuaW5nfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1603205431090-8dcdfcb54897?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NDF8fGxlYXJuaW5nfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
  ];

  UserBloc userBloc = UserBloc();
  bool isLoading = true;
  List<String> sliderImages = [];
  bool scrolling =  false;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    // ObjectFactory().appHive.putOrderStatus(status:false);
    print("UserId   " + ObjectFactory().appHive.getId().toString());
    print("phone   " + ObjectFactory().appHive.getPhoneNumber().toString());
    print("email   " + ObjectFactory().appHive.getEmail().toString());
    print("User Subscription Status   " + ObjectFactory().appHive.getOrderStatus().toString());
    // ObjectFactory().appHive.getEmail() == null
    //     ? print("No email Id added")
    //     : print("Email ID   " + ObjectFactory().appHive.getEmail());
    // print("----------------------------------");
    print("images length " + sliderImages.length.toString());
    userBloc.getSliderImages();
    userBloc.getSliderImageResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        print("Slider Images loaded Successfully");
        setState(() {
          isLoading = false;
        });
        for (int i = 0; i < event.data.length; i++) {
          setState(() {
            sliderImages.add(event.data[i].image);
          });
        }
        print("Slider Images List");
        print("images length " + sliderImages.length.toString());
        print(sliderImages);
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });

    // userBloc.getOrderPayments(orderId: widget.orderId);
    // userBloc.getOrderPaymentResponse.listen((event) {
    //   if (event.status == 200 || event.status == 201) {
    //     setState(() {
    //       print("Order Payment checks");
    //       ObjectFactory().appHive.putOrderStatus(
    //           status:event.data.txStatus == "SUCCESS"
    //               ? true
    //               : false);
    //     });
    //   }
    // });

    // _scrollController.position.addListener(() {
    //   setState((){
    //     scrolling = true;
    //   });
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Constants.kitGradients[2],
            statusBarIconBrightness: Brightness.light),
        child: DoubleBack(
          message: "Press back again to exit app",
          child: Scaffold(
            backgroundColor: Constants.kitGradients[2],
            // appBar: AppBar(
            //   toolbarHeight: screenHeight(context, dividedBy: 5),
            //   actions: [
            //     Container(
            //       height: screenHeight(context, dividedBy: 6),
            //       width: screenWidth(context, dividedBy: 1),
            //       child: Stack(
            //         children: [
            //           Container(
            //             height: screenHeight(context, dividedBy: 6),
            //             width: screenWidth(context, dividedBy: 1),
            //             color: Constants.kitGradients[2],
            //             child: Column(
            //               mainAxisAlignment: MainAxisAlignment.start,
            //               crossAxisAlignment: CrossAxisAlignment.start,
            //               children: [
            //                 SizedBox(
            //                   height: screenHeight(context, dividedBy: 50),
            //                 ),
            //                 Row(
            //                   mainAxisAlignment: MainAxisAlignment.start,
            //                   crossAxisAlignment: CrossAxisAlignment.start,
            //                   children: [
            //                     SizedBox(
            //                       width: screenWidth(context, dividedBy: 30),
            //                     ),
            //                     Column(
            //                       mainAxisAlignment: MainAxisAlignment.start,
            //                       crossAxisAlignment: CrossAxisAlignment.start,
            //                       children: [
            //                         Container(
            //                           width:
            //                               screenWidth(context, dividedBy: 1.17),
            //                           child: Text("Upyou Learnings",
            //                               style: TextStyle(
            //                                   fontSize: screenWidth(context,
            //                                       dividedBy: 14),
            //                                   fontWeight: FontWeight.bold,
            //                                   fontFamily: "SofiaProRegular",
            //                                   color: Colors.white)),
            //                         ),
            //                         Padding(padding: EdgeInsets.all(2)),
            //                         Text(
            //                             "Hi friend, welcome to upyou learnings !!",
            //                             style: TextStyle(
            //                                 fontSize: screenWidth(context,
            //                                     dividedBy: 24),
            //                                 fontWeight: FontWeight.w200,
            //                                 fontFamily: "SofiaProRegular",
            //                                 color: Colors.white)),
            //                         SizedBox(
            //                           width: screenWidth(context, dividedBy: 30),
            //                         ),
            //                       ],
            //                     ),
            //                     Column(
            //                       mainAxisAlignment: MainAxisAlignment.start,
            //                       children: [
            //                         SizedBox(
            //                           height:
            //                               screenHeight(context, dividedBy: 400),
            //                         ),
            //                         GestureDetector(
            //                             onTap: () {
            //                               push(context, ProfilePage());
            //                             },
            //                             child: Container(
            //                               width:
            //                                   screenWidth(context, dividedBy: 10),
            //                               child: Icon(
            //                                 Icons.person_outline_outlined,
            //                                 size: screenWidth(context,
            //                                     dividedBy: 12),
            //                                 color: Colors.white,
            //                               ),
            //                             )),
            //                       ],
            //                     )
            //                   ],
            //                 ),
            //               ],
            //             ),
            //           ),
            //           Positioned(
            //             top: screenHeight(context, dividedBy: 9),
            //             child: Container(
            //               height: screenHeight(context, dividedBy: 13),
            //               width: screenWidth(context, dividedBy: 1),
            //               decoration: BoxDecoration(
            //                 color: Colors.white,
            //                 borderRadius: BorderRadius.only(
            //                   topRight: Radius.circular(30),
            //                   topLeft: Radius.circular(30),
            //                 ),
            //               ),
            //             ),
            //           ),
            //         ],
            //       ),
            //     ),
            //   ],
            // ),
            body: Container(
              width: screenWidth(context, dividedBy: 1),
              // height: screenHeight(context, dividedBy: 1),
              child: Stack(
                children: [
                  Container(
                    height: screenHeight(context, dividedBy: 8),
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[2],
                      border: Border.all(
                          color: Constants.kitGradients[2], width: 1.0),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        // Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 80),
                                ),
                                // Spacer(),
                                Container(
                                  width: screenWidth(context, dividedBy: 1.17),
                                  child: Text("Upyou Learnings",
                                      style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "SofiaProRegular",
                                          color: Colors.white)),
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 200),
                                ),
                                // Spacer(),
                                Text("Hi friend, welcome to upyou learnings !!",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w200,
                                        fontFamily: "SofiaProRegular",
                                        color: Colors.white)),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 30),
                                ),
                              ],
                            ),
                            // Column(
                            //   mainAxisAlignment: MainAxisAlignment.start,
                            //   children: [
                            //     SizedBox(
                            //       height: screenHeight(context, dividedBy: 400),
                            //     ),
                            //   ],
                            // )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: screenHeight(context, dividedBy: 15),
                    right: screenWidth(context, dividedBy: 25),
                    child: GestureDetector(
                        onTap: () {
                          print("Profile navigation");
                          push(context, ProfilePage());
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 10),
                          child: Icon(
                            Icons.person_outline_outlined,
                            size: screenWidth(context, dividedBy: 11),
                            color: Colors.white,
                          ),
                        )),
                  ),
                  Positioned(
                    // top:screenHeight(context, dividedBy: 8),
                    child: SingleChildScrollView(
                      controller :_scrollController,
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        // height: screenHeight(context, dividedBy: 1),
                        // color: Colors.red,
                        child: Column(
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 7.2),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1),
                              // height: screenHeight(context, dividedBy: 1),
                              decoration: BoxDecoration(
                                color: Colors.grey[100],
                                // border:Border.fromBorderSide(top:BorderSide(color:Constants.kitGradients[2], width: 1.0)),
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(30),
                                  topLeft: Radius.circular(30),
                                ),
                              ),
                              padding: EdgeInsets.only(
                                  // top: screenWidth(context, dividedBy: 2.8),
                                  ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  sliderImages.length == 0
                                      ? Container(
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          height: screenHeight(context,
                                              dividedBy: 3.2),
                                          decoration: BoxDecoration(
                                            // color: Colors.red,
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(30),
                                              topLeft: Radius.circular(30),
                                            ),
                                          ),
                                          child: Center(
                                            heightFactor: 1,
                                            widthFactor: 1,
                                            child: SizedBox(
                                              height: 16,
                                              width: 16,
                                              child: AwesomeLoader(
                                                loaderType: AwesomeLoader
                                                    .AwesomeLoader3,
                                                color: Constants.kitGradients[2]
                                                    .withOpacity(0.3),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container(
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          height: screenHeight(context,
                                              dividedBy: 3.2),
                                          decoration: BoxDecoration(
                                            color: Colors.grey[100],
                                            // border:Border(top:BorderSide(color:Constants.kitGradients[2], width: 1.0)),
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(30),
                                              topLeft: Radius.circular(30),
                                            ),
                                          ),
                                          // color: Colors.transparent,
                                          child: Center(
                                              child: CustomImageSlider(
                                                  sliderImages: sliderImages)),
                                        ),
                                  Container(
                                    color: Constants.kitGradients[0],
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 50),
                                        ),
                                        Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            // color: Constants.kitGradients[0],
                                            padding: EdgeInsets.symmetric(
                                                horizontal: screenWidth(context,
                                                    dividedBy: 23)),
                                            child: GestureDetector(
                                              onTap: () {
                                                // showSubscriptionAlertDialog(
                                                //     context);
                                              },
                                              child: Text("Explore ",
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily:
                                                          "SofiaProRegular",
                                                      color: Colors.black)),
                                            )),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 60),
                                        ),
                                        Container(
                                          // color: Constants.kitGradients[0],
                                          padding: EdgeInsets.symmetric(
                                              horizontal: screenWidth(context,
                                                  dividedBy: 50)),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Column(children: [
                                                HomeTile(
                                                    title:
                                                        "Aptitude Preparation",
                                                    boxColor: Color(0xffEFECF4),
                                                    image:
                                                        "assets/images/aptitude_preparation.svg",
                                                    boxHeight: 4.8,
                                                    onTap: () {
                                                      push(context,
                                                          ApptitudePage());
                                                    }),
                                                SizedBox(
                                                  height: screenHeight(context,
                                                      dividedBy: 300),
                                                ),
                                                HomeTile(
                                                    title:
                                                        "Interview Preparation",
                                                    boxColor: Color(0xffDFE3FF),
                                                    image:
                                                        "assets/images/interview_preparation.svg",
                                                    boxHeight: 4.8,
                                                    onTap: () {
                                                      push(
                                                        context,
                                                        ResumePreperationPage(
                                                          id: 3,
                                                          // paymentStatus:
                                                          //     widget.paid,
                                                          title:
                                                              "Interview Preparation",
                                                        ),
                                                        // ResumePreparationVideoListPage(
                                                        //   id: 3,
                                                        //   title:
                                                        //       "Interview Preparation",
                                                        // )
                                                      );
                                                    }),
                                                HomeTile(
                                                  title: "Bonus Points",
                                                  boxColor: Color(0xffF4ECED),
                                                  image:
                                                      "assets/images/bonustips.svg",
                                                  boxHeight: 4.8,
                                                  onTap: () {
                                                    push(
                                                        context,
                                                        ResumePreperationPage(
                                                          id: 2,
                                                          title: "Bonus Tips",
                                                          // paymentStatus:
                                                          //     widget.paid,
                                                        )
                                                        // ResumePreparationVideoListPage(
                                                        //   id: 2,
                                                        //   title: "Bonus Tips",
                                                        // )
                                                        );
                                                  },
                                                ),
                                              ]),
                                              SizedBox(
                                                width: screenWidth(context,
                                                    dividedBy: 30),
                                              ),
                                              Column(
                                                children: [
                                                  //Spacer(flex: 1),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 300),
                                                  ),
                                                  HomeTile1(
                                                      title:
                                                          "Resume Preparation",
                                                      boxColor:
                                                          Color(0xffF5F4EE),
                                                      image:
                                                          "assets/images/right_tile1.png",
                                                      // "assets/images/right_tile1.png",
                                                      boxHeight: 4.8,
                                                      onTap: () {
                                                        push(
                                                            context,
                                                            ResumePreperationPage(
                                                              id: 1,
                                                              title:
                                                                  "Resume Preparation",
                                                              // paymentStatus:
                                                              //     widget.paid,
                                                            )
                                                            // ResumePreparationVideoListPage(
                                                            //   id: 1,
                                                            //   title:
                                                            //       "Resume Preparation",
                                                            // )
                                                            );
                                                      }),

                                                  //Spacer(flex: 1),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 300),
                                                  ),
                                                  HomeTile(
                                                      title: "Group Discussion",
                                                      boxColor:
                                                          Color(0xffF4ECED),
                                                      image:
                                                          "assets/images/group_discussion.svg",
                                                      boxHeight: 4.8,
                                                      onTap: () {
                                                        push(
                                                            context,
                                                            ResumePreperationPage(
                                                              id: 4,
                                                              title:
                                                                  "Group Discussion",
                                                              // paymentStatus:
                                                              //     widget.paid,
                                                            )
                                                            // ResumePreparationVideoListPage(
                                                            //   id: 4,
                                                            //   title:
                                                            //       "G2roup Discussion",
                                                            // )
                                                            );
                                                      }),

                                                  //Spacer(flex: 1),
                                                  // SizedBox(
                                                  //   height: screenHeight(context, dividedBy: 10),
                                                  // )
                                                ],
                                              ),
                                              SizedBox(
                                                width: screenWidth(context,
                                                    dividedBy: 30),
                                              ),
                                              Column(
                                                children: [
                                                  // SizedBox(
                                                  //   height: screenHeight(context, dividedBy: 50),
                                                  // ),
                                                  HomeTile(
                                                    title:
                                                        "Cover Letter Preparation",
                                                    boxColor: Color(0xffF4ECED),
                                                    image:
                                                        "assets/images/cover_letter.svg",
                                                    boxHeight: 4.8,
                                                    onTap: () {
                                                      push(
                                                          context,
                                                          ResumePreperationPage(
                                                            id: 2,
                                                            title:
                                                                "Cover Letter Preparation",
                                                            // paymentStatus:
                                                            //     widget.paid,
                                                          )
                                                          // ResumePreparationVideoListPage(
                                                          //   id: 2,
                                                          //   title:
                                                          //       "Cover Letter Preparation",
                                                          // )
                                                          );
                                                    },
                                                  ),
                                                  //Spacer(),

                                                  //Spacer(),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 300),
                                                  ),
                                                  HomeTile(
                                                    title:
                                                        "LinkedIn Management ",
                                                    boxColor: Color(0xffF4ECED),
                                                    image:
                                                        "assets/images/linked_in.svg",
                                                    boxHeight: 4.8,
                                                    onTap: () {
                                                      push(
                                                          context,
                                                          ResumePreperationPage(
                                                            id: 5,
                                                            title:
                                                                "LinkedIn Management",
                                                            // paymentStatus:
                                                            //     widget.paid,
                                                          )
                                                          // ResumePreparationVideoListPage(
                                                          //   id: 5,
                                                          //   title:
                                                          //       "LinkedIn Management",
                                                          // )
                                                          );
                                                    },
                                                  ),
                                                ],
                                              ),
                                              Column(children: []),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 30),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            // Positioned(
                            //   top:screenHeight(context, dividedBy:9),
                            //   child: Container(
                            //     width: screenWidth(context, dividedBy: 1),
                            //     height: screenHeight(context, dividedBy: 3.2),
                            //     // color: Constants.kitGradients[24],
                            //     child: Center(
                            //         child: CustomImageSlider(sliderImages: images)),
                            //   ),
                            // )
                            // ClipPath(
                            //   clipper: WaveClipperTwo(),
                            //   child: Container(
                            //     height: screenHeight(context, dividedBy: 5.5),
                            //     width: screenWidth(context, dividedBy: 1),
                            //     // color: Constants.kitGradients[0],
                            //     decoration: ShapeDecoration(
                            //         shape: AppBarBorder(context),
                            //         color: Constants.kitGradients[2]),
                            //     child: Column(
                            //       mainAxisAlignment: MainAxisAlignment.start,
                            //       crossAxisAlignment: CrossAxisAlignment.start,
                            //       children: [
                            //         SizedBox(
                            //           height: screenHeight(context, dividedBy: 50),
                            //         ),
                            //         Row(
                            //           mainAxisAlignment: MainAxisAlignment.start,
                            //           crossAxisAlignment: CrossAxisAlignment.start,
                            //           children: [
                            //             SizedBox(
                            //               width: screenWidth(context, dividedBy: 30),
                            //             ),
                            //             Column(
                            //               mainAxisAlignment: MainAxisAlignment.start,
                            //               crossAxisAlignment: CrossAxisAlignment.start,
                            //               children: [
                            //                 Container(
                            //                   width: screenWidth(context, dividedBy: 1.17),
                            //                   child: Text("Upyou Learnings",
                            //                       style: TextStyle(
                            //                           fontSize:
                            //                               screenWidth(context, dividedBy: 14),
                            //                           fontWeight: FontWeight.bold,
                            //                           fontFamily: "SofiaProRegular",
                            //                           color: Colors.white)),
                            //                 ),
                            //                 Padding(padding: EdgeInsets.all(2)),
                            //                 Text("Hi friend, welcome to upyou learnings !!",
                            //                     style: TextStyle(
                            //                         fontSize:
                            //                         screenWidth(context, dividedBy: 24),
                            //                         fontWeight: FontWeight.w200,
                            //                         fontFamily: "SofiaProRegular",
                            //                         color: Colors.white)),
                            //
                            //                 SizedBox(
                            //                   width: screenWidth(context, dividedBy: 30),
                            //                 ),
                            //               ],
                            //             ),
                            //             Column(
                            //               mainAxisAlignment: MainAxisAlignment.start,
                            //               children: [
                            //                 SizedBox(
                            //                   height: screenHeight(context, dividedBy: 400),
                            //                 ),
                            //                 GestureDetector(
                            //                     onTap: () {
                            //                       push(context, ProfilePage());
                            //                     },
                            //                     child: Container(
                            //                       width: screenWidth(context, dividedBy: 10),
                            //                       child: Icon(
                            //                         Icons.person_outline_outlined,
                            //                         size: screenWidth(context, dividedBy: 12),
                            //                         color: Colors.white,
                            //                       ),
                            //                     )),
                            //               ],
                            //             )
                            //           ],
                            //         ),
                            //       ],
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // scrolling == true ? Container () :
                  Positioned(
                    top: screenHeight(context, dividedBy: 15),
                    right: screenWidth(context, dividedBy: 25),
                    child: GestureDetector(
                        onTap: () {
                          print("Profile navigation");
                          push(context, ProfilePage());
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 10),
                          height: screenWidth(context, dividedBy: 10),
                          color: Colors.transparent,
                          // child: Icon(
                          //   Icons.person_outline_outlined,
                          //   size: screenWidth(context, dividedBy: 12),
                          //   color: Colors.white,
                          // ),
                        ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class AppBarBorder extends ShapeBorder {
  final BuildContext context;
  AppBarBorder(this.context);
  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    Offset controllPoint1 =
        Offset(screenWidth(context, dividedBy: 4.9), rect.size.height);
    Offset endPoint1 =
        Offset(screenWidth(context, dividedBy: 1.9), rect.size.height / 1.2);
    Offset controllPoint2 =
        Offset(screenWidth(context, dividedBy: 1.1), rect.size.height / 1.5);
    Offset endPoint2 = Offset(rect.size.width,
        rect.size.height - screenWidth(context, dividedBy: 12));

    return Path()
      ..lineTo(0, rect.size.height - 20)
      ..quadraticBezierTo(
          controllPoint1.dx, controllPoint1.dy, endPoint1.dx, endPoint1.dy)
      // ..lineTo(rect.size.width - 100, rect.size.height - 100)
      ..quadraticBezierTo(
          controllPoint2.dx, controllPoint2.dy, endPoint2.dx, endPoint2.dy)
      ..lineTo(rect.size.width, 0);
  }

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.only(bottom: 0);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) => null;

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {}

  @override
  ShapeBorder scale(double t) => this;
}
