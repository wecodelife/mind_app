import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/terms_list_item.dart';
import 'package:flutter/material.dart';

class TermsAndConditions extends StatefulWidget {
  //const TermsAndConditions({Key? key}) : super(key: key);

  @override
  _TermsAndConditionsState createState() => _TermsAndConditionsState();
}

class _TermsAndConditionsState extends State<TermsAndConditions> {
  List<String> headings = [
    "Terms & Conditions",
    "USAGE",
    "What choices a user has regarding the use of his/her information",
    "Changes to This Terms and Conditions",
    "Contact Us",
  ];
  List<String> subHeading = [
    "By downloading or using the app, these terms will automatically apply to you – you"
        " should make sure therefore that you read them carefully before using the app. You’re"
        " not allowed to copy, or modify the app, any part of the app, or our trademarks in any"
        " way. You’re not allowed to attempt to extract the source code of the app, and you also"
        " shouldn’t try to translate the app into other languages, or make derivative versions."
        " The app itself, and all the trademarks, copyright, database rights and other intellectual"
        " property rights related to it, still belong to Thoufeek. k. On this site, if you find any"
        " information that is owned by you or any content that violates your intellectual property"
        " rights, please contact us with all necessary documents/information that authenticates"
        " your authority on your property."
        " Thoufeek. k is committed to ensuring that the app is as useful and efficient as possible."
        " For that reason, we reserve the right to make changes to the app or to charge for its"
        " services, at any time and for any reason. We will never charge you for the app or its"
        " services without making it very clear to you exactly what you’re paying for."
        " The UPYOU app stores and processes personal data that you have provided to us, in"
        " order to provide my Service. It’s your responsibility to keep your phone and access to"
        " the app secure. We therefore recommend that you do not jailbreak or root your phone,"
        "  which is the process of removing software restrictions and limitations imposed by the"
        " official operating system of your device. It could make your phone vulnerable to"
        " malware/viruses/malicious programs, compromise your phone’s security features and"
        " it could mean that the UPYOU app won’t work properly or at all."
        " The app does use third party services that declare their own Terms and Conditions."
        " Link to Terms and Conditions of third party service providers used by the app.\n\n"
        "                  • AdMob\n\n"
        "                  • Google Analytics for Firebase\n\n"
        "                  • Firebase Crashlytics\n\n"
        "You should be aware that there are certain things that Thoufeek.k will not take"
        " responsibility for. Certain functions of the app will require the app to have an active"
        " internet connection. The connection can be Wi-Fi, or provided by your mobile network"
        " provider, but Thoufeek.k cannot take responsibility for the app not working at full"
        " functionality if you don’t have access to Wi-Fi, and you don’t have any of your data"
        " allowance left."
        " If you’re using the app outside of an area with Wi-Fi, you should remember that your"
        " terms of the agreement with your mobile network provider will still apply. As a result,"
        " you may be charged by your mobile provider for the cost of data for the duration of"
        " the connection while accessing the app, or other third party charges. In using the app,"
        " you’re accepting responsibility for any such charges, including roaming data charges"
        " if you use the app outside of your home territory (i.e. region or country) without"
        " turning off data roaming. If you are not the bill payer for the device on which you’re"
        " using the app, please be aware that we assume that you have received permission"
        " from the bill payer for using the app."
        " Along the same lines, Thoufeek. k cannot always take responsibility for the way you"
        " use the app i.e. You need to make sure that your device stays charged – if it runs out"
        " of battery and you can’t turn it on to avail the Service, Thoufeek. k cannot accept"
        " responsibility."
        " With respect to Thoufeek. k’s responsibility for your use of the app, when you’re using"
        " the app, it’s important to bear in mind that although we endeavour to ensure that it is"
        " updated and correct at all times, we do rely on third parties to provide information to"
        " us so that we can make it available to you. Thoufeek.k accepts no liability for any loss,"
        " direct or indirect, you experience as a result of relying wholly on this functionality of"
        " the app."
        " At some point, we may wish to update the app. The app is currently available on"
        " Android – the requirements for the system(and for any additional systems we decide"
        " to extend the availability of the app to) may change, and you’ll need to download the"
        " updates if you want to keep using the app. Thoufeek. k does not promise that it will"
        " always update the app so that it is relevant to you and/or works with the Android"
        " version that you have installed on your device. However, you promise to always accept"
        " updates to the application when offered to you, We may also wish to stop providing"
        " the app, and may terminate use of it at any time without giving notice of termination"
        " to you. Unless we tell you otherwise, upon any termination, (a) the rights and licenses"
        " granted to you in these terms will end; (b) you must stop using the app, and (if needed)"
        " delete it from your device.",
    "Thoufeek. k through the application upyou provides various job vacancy alerts, video"
        " contents, text Resources and aptitude practise questions with the best possible"
        " answers. The Job vacancy alerts provided through this application are collected and"
        " shared from various other sources, so Thoufeek. K does not guarantee the authenticity,"
        " accuracy and reliability of the job vacancies sharing through this application. Thoufeek."
        " k is also not liable to any loss caused to you on applying to the job vacancies provided"
        " through the application."
        " Thoufeek. k do not own the rights of the aptitude MCQ questions and answers"
        " provided in this application, it is collected and shared from other sources. Thoufeek"
        " also does not guarantee 100% correctness of questions and their answers. Though,"
        " Thoufeek. k has taken all possible steps to make all data 100% correct. Thoufeek. k also"
        " assumes no liability for disputes regarding ownership, copyright, or trademarks of the"
        " data submitted to this application."
        " Thoufeek. K doesn't guarantee any success in job interviews following any contents"
        " provided through this application, interview successes are subjective only to your"
        " performance and the recruiter's needs. Also Thoufeek. K does not guarantee 100 %"
        " accuracy and correctness of any information provided through this application."
        " Recruiters may select you or reject you based on their preferences and the practices"
        " they follow in different rounds of a recruitment process, Thoufeek. k is not liable for"
        " any losses to you regarding any recruiters decisions. Thoufeek can also remove or add"
        " data without any individual notice. All trademarks and company names published in"
        " this application are subjected to their respected owners and companies.",
    "In case, if the user is receiving marketing or promotional emails from Upyou Learnings"
        " you have the option to opt out of such emails by emailing Upyou Learnings at"
        " upyoulearnings@gmail.com. If the user opts out he/ she may still receive nonpromotional emails from Upyou, such as educational materials, emails about his/her"
        " accounts, or any ongoing business relations entered into by upyou learnings.",
    "I may update our Terms and Conditions from time to time. Thus, you are advised to"
        " review this page periodically for any changes. I will notify you of any changes by"
        " posting the new Terms and Conditions on this page."
        " These terms and conditions are effective as of 2021-07-04.",
    "If you have any questions or suggestions about my Terms and Conditions, do not"
        " hesitate to contact me at upyoulearnings@gmail.com.",
  ];

  final String termsAndCondition = "";
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Constants.kitGradients[0],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[0],
          elevation: 0,
          leading: Container(),
          actions: [
            CAppBar(
              onPressedLeftIcon: () {
                pop(context);
              },
              title: "Terms and Conditions",
              isWhite: false,
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
              width: screenWidth(context, dividedBy: 1),
              //height: screenHeight(context, dividedBy: 1),
              decoration: BoxDecoration(
                color: Constants.kitGradients[0],
              ),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Container(
                      child: ListView.builder(
                    itemCount: headings.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          Container(
                            child: TermsListItems(
                                index: index,
                                title: headings[index],
                                data: subHeading[index]),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                        ],
                      );
                    },
                  )),

                  // Spacer(),
                  //Center(child: CreatorName()),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
