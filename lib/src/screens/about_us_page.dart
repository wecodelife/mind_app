import 'package:app_template/src/screens/privacy_policy_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/terms_and_conditions_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/app_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AboutUsPage extends StatefulWidget {
  //const AboutUsPage({Key? key}) : super(key: key);

  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, ProfilePage(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark),
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Constants.kitGradients[0],
              elevation: 0,
              leading: Container(),
              actions: [
                CAppBar(
                  isWhite: false,
                  title: "About Us",
                  onPressedLeftIcon: () {
                    pop(context);
                  },
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                    horizontal: screenHeight(context, dividedBy: 30)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 70),
                    ),
                    Text(
                      "We thrive to make real education reachable to every candidate out there breaking the economic barriers, In Upyou learnings we are building a platform to bridge the gap"
                      " between the school syllabus and real-life syllabus. We believe bridging this gap can"
                      " result in building a more Informed, Responsible, Productive, United and happy young"
                      " population."
                      " Upyou is a job placement preparation application from Upyou Learnings. A one-stop"
                      " solution for the recent graduates to prepare for the interview processes and get"
                      " notified of job vacancies."
                      " Getting placed in a good company is a dream of every graduate, however, when"
                      " appears to interview everybody struggles. Because they are not taught in their schools"
                      " or colleges, On how to get equipped for an interview. Moreover, in this internet era,"
                      " we are being flooded with zillions of content. it has become a complicated process for"
                      " the recent graduates to filter the right pieces of information and get prepared for the"
                      " interview processes."
                      " Thus, through Upyou, we bring curated video and text content on Aptitude, Resume"
                      " building, cover letter drafting, Linkedin profile building and tackling the personal"
                      " interview rounds."
                      " Let's give wings to your dreams through Upyou Learnings.",
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: "Prompt-Light",
                        color: Constants.kitGradients[7],
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    AppListTile(
                      text: "Terms and Conditions",
                      onTap: () {
                        push(context, TermsAndConditions());
                      },
                    ),

                    AppListTile(
                      text: "Privacy Policy",
                      onTap: () {
                        push(context, PrivacyPolicyPage());
                      },
                    ),
                    //CreatorName(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
