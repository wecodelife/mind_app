import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_resouce_type_response_model.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/interview_questions_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/resources_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ResourcesPage extends StatefulWidget {
  @override
  _ResourcesPageState createState() => _ResourcesPageState();
}

class _ResourcesPageState extends State<ResourcesPage> {
  List<String> resources = [
    "General Interview Questions",
    "Behavioral Interview Questions",
    "Group Discussion",
    "Cover Mail Example",
    "Admission Interview Questions",
    "Experienced Interview Questions"
  ];
  List<String> images = [
    "assets/images/resource_bg1.png",
    "assets/images/resource_bg2.png",
    "assets/images/resource_bg1.png",
    "assets/images/resource_bg1.png",
    "assets/images/resource_bg3.png",
    "assets/images/resource_bg1.png",
  ];
  List<Color> colors = [
    Color(0xff4385F5),
    Color(0xff8F98FF),
    Color(0xff4385F5),
    Color(0xff8F98FF),
    Color(0xff172A88),
    Color(0xff4884E9),
  ];
  int backgroundImageIndex = 0;
  int colorValue;
  int countSelected = 1000;
  UserBloc userBloc = new UserBloc();
  @override
  void initState() {
    // TODO: implement initState
    userBloc.getResourceType();
    userBloc.getResourceTypeResponse.listen((event) {}).onError((event) {
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return  AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark),
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              elevation: 0,
              leading: Container(),
              actions: [
                CAppBar(
                  title: "Resource",
                  color: Colors.white,
                  isWhite: false,
                  onPressedLeftIcon: () {
                    pushAndRemoveUntil(context, BottomBar(), false);
                  },
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Text(
                        "SELECT A SECTION OF INTEREST",
                        style: TextStyle(
                            color: Color(0xff2C2E39),
                            fontFamily: "SofiaProRegular"),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    Container(
                      child: StreamBuilder<GetResouceTypeResponse>(
                          stream: userBloc.getResourceTypeResponse,
                          builder: (context, snapshot) {
                            return snapshot.hasError
                                ? Container(
                                    width: screenWidth(context, dividedBy: 1),
                                    height: screenHeight(context, dividedBy: 1.5),
                                    child: Center(
                                        child: Text(
                                      snapshot.error.toString(),
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.blueGrey,
                                      ),
                                    )),
                                  )
                                : snapshot.hasData
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: snapshot.data.data.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          if (index == (images.length - 1) ||
                                              index == 0) {
                                            backgroundImageIndex = 0;
                                            colorValue = 0;
                                          } else {
                                            backgroundImageIndex =
                                                backgroundImageIndex + 1;
                                            colorValue = colorValue + 1;
                                          }

                                          return ResourceCard(
                                            title: snapshot.data.data[index].name,
                                            bgImage: images[backgroundImageIndex],
                                            boxColor: colors[colorValue],
                                            index: index,
                                            countSelected: countSelected,
                                            onSelected: () {
                                              setState(() {
                                                countSelected = index;
                                              });

                                              push(
                                                  context,
                                                  InterviewQuestions(
                                                    pageTitle: snapshot
                                                        .data.data[index].name,
                                                    id: snapshot
                                                        .data.data[index].id,
                                                  ));
                                            },
                                          );
                                        },
                                      )
                                    : Container(
                                        height:
                                            screenHeight(context, dividedBy: 1.3),
                                        width: screenWidth(context, dividedBy: 1),
                                        child: Center(
                                          child: CircularProgressIndicator(
                                            valueColor:
                                                new AlwaysStoppedAnimation<Color>(
                                                    Constants.kitGradients[2]),
                                          ),
                                        ),
                                      );
                          }),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
