import 'dart:async';

import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  Timer _timerControl;
  void startTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 3), (timer) {
      _timerControl.cancel();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  ObjectFactory().appHive.getSignedInCheck() == false ||
                          ObjectFactory().appHive.getSignedInCheck() == null
                      ? LoginPage()
                      : BottomBar()),
          (route) => false);
    });
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Constants.kitGradients[0],
        ),
        child: Scaffold(
            backgroundColor: Colors.white,
            body: Builder(
                builder: (context) => SafeArea(
                        child: Container(
                      height: screenHeight(context, dividedBy: 1),
                      width: screenWidth(context, dividedBy: 1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Center(
                            child: SvgPicture.asset(
                              "assets/images/mind_logo.svg",
                              width: screenWidth(context, dividedBy: 1.2),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      ),
                    )))));
  }
}
