import 'dart:async';

import 'package:app_template/src/screens/otp_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/textfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignInWithPhone extends StatefulWidget {
  @override
  _SignInWithPhoneState createState() => _SignInWithPhoneState();
}

class _SignInWithPhoneState extends State<SignInWithPhone> {
  TextEditingController _phoneNumberTextEditingController =
      TextEditingController();
  TextEditingController _otpTextEditingController = new TextEditingController();
  String verificationId;
  bool isLoading = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool keyBoard = false;
  var keyboardVisibilityController = KeyboardVisibilityController();
  StreamSubscription<bool> keyBoardController;
  @override
  void dispose() {
    keyBoardController.cancel();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    keyBoardController =
        keyboardVisibilityController.onChange.listen((bool visible) {
      keyBoard = visible;
      setState(() {});
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 16),
        ),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
                height: keyBoard == true
                    ? screenHeight(context, dividedBy: 10)
                    : screenHeight(context, dividedBy: 6)),
            Center(
              child: Container(
                width: screenWidth(context, dividedBy: 2),
                height: screenHeight(context, dividedBy: 7),
                child: SvgPicture.asset(
                  "assets/images/mind_logo.svg",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Row(
              children: [
                Text(
                  "Welcome to ",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w600,
                    color: Constants.kitGradients[7],
                    fontFamily: "SofiaProRegular",
                  ),
                ),
                Text(
                  "UpYou !",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Constants.kitGradients[2],
                    fontFamily: "Montserrat-ExtraBold",
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Login With your Mobile Number",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Constants.kitGradients[7],
                    fontFamily: "SofiaProRegular",
                  ),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 90),
            ),
            Text(
              "Please enter your Mobile Number, We will send you an OTP on this mobile Number",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: Constants.kitGradients[7].withOpacity(0.4),
                fontFamily: "SofiaProRegular",
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            CTextField(
                // onSubmitted: (value) {
                //   print("hai");
                //   value = _phoneNumberTextEditingController.text;
                //   setState(() {
                //     isLoading = true;
                //   });
                //   if (_phoneNumberTextEditingController.text.length >= 10)
                //     signInWithPhone(_phoneNumberTextEditingController.text);
                // },

                isNumber: true,
                label: "PhoneNumber",
                textEditingController: _phoneNumberTextEditingController),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 10)),
              child: CButton(
                onPressed: () {
                  setState(() {
                    isLoading = true;
                  });
                  if (_phoneNumberTextEditingController.text.length >= 10)
                    signInWithPhone(_phoneNumberTextEditingController.text);
                },
                title: "VerifyNumber",
                isLoading: isLoading,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void signInWithPhone(String phoneNumber) async {
    // emit(AuthLoading());
    FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: "+91" + phoneNumber,
      timeout: Duration(seconds: 120),
      verificationCompleted: (AuthCredential authCredential) {
        FirebaseAuth.instance
            .signInWithCredential(authCredential)
            .then((value) async {
          // emit(PhoneVerified(phone: "+91" + phoneNumber));
          print("firebaseauth" + authCredential.signInMethod);
        });
      },
      verificationFailed: (FirebaseAuthException authException) {
        setState(() {
          isLoading = false;
        });
        print(authException.toString());
        showToast(authException.toString());
        // showSnackbar(authException.message);
      },
      codeSent: (value, [data]) async {
        verificationId = value;

        setState(() {
          isLoading = false;
        });
        push(
            context,
            OtpPage(
              phoneNumber: _phoneNumberTextEditingController.text,
              verificationId: verificationId,
            ));
      },
      codeAutoRetrievalTimeout: (value) {
        setState(() {
          isLoading = false;
        });

        showToast("code auto retrieval timed out ");

        // emit(PhoneVerified(verificationId: value, phone: "+91" + phoneNumber));
      },
    );
  }

// void showSnackbar(String message) {
//   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//     content: Text(
//       message,
//       style: TextStyle(color: Colors.black),
//     ),
//     duration: const Duration(seconds: 5),
//   ));
// }
}
