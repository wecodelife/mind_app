import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_question_type_response.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/resume_video_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/apptitude_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ApptitudePage extends StatefulWidget {
  @override
  _ApptitudePageState createState() => _ApptitudePageState();
}

class _ApptitudePageState extends State<ApptitudePage> {
  UserBloc userBloc = UserBloc();

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  examId() {
    if (ObjectFactory().appHive.getExamCompletedStatus() == null) {
      ObjectFactory().appHive.putExamCompletedStatus(1);
    }
  }

  String error = "";
  @override
  void initState() {
    // TODO: implement initState
    userBloc.getQuestionType(id: 1);
    examId();

    userBloc.getQuestionTypeResponse.listen((event) {}).onError((event) {
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
    });

    // if (ObjectFactory().appHive.putExamId() != null) {
    //   ObjectFactory().appHive.putExamId(examId: "");
    // } else {}
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Colors.white,
       statusBarIconBrightness: Brightness.dark
    ),
    child:WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            toolbarHeight: screenHeight(context, dividedBy: 15),
            elevation: 0,
            leading: Container(),
            actions: [
              Column(
                children: [
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 100),
                  // ),
                  CAppBar(
                    title: "Aptitude",
                    color: Colors.white,
                    isWhite: false,
                    onPressedLeftIcon: () {
                      pushAndRemoveUntil(context, BottomBar(), false);
                    },
                  ),
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 150),
                  // ),
                ],
              ),
            ],
          ),
          body: Material(
            child: DefaultTabController(
              length: 2,
              child: Scaffold(
                appBar: AppBar(
                    // toolbarHeight: screenHeight(context, dividedBy: 15),
                    actions: [
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 16),
                        color: Colors.white,
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "SELECT A SECTION OF INTEREST",
                          style: TextStyle(
                              color: Color(0xff2C2E39),
                              fontFamily: "SofiaProRegular"),
                        ),
                      ),
                    ],
                    elevation: 0,
                    backgroundColor: Colors.white,
                    bottom: TabBar(
                      onTap: (value) {
                        userBloc.getQuestionType(id: value + 1);
                      },
                      indicatorWeight: 0,
                      indicator: BoxDecoration(
                        color: Constants.kitGradients[2],
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Color(0xffC3C5CB)),
                      ),
                      indicatorSize: TabBarIndicatorSize.label,
                      labelStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                      unselectedLabelStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                      unselectedLabelColor: Color(0xffC3C5CB),
                      tabs: [
                        Tab(
                          child: Container(
                            // width: screenWidth(context, dividedBy: 2),
                            // height: screenHeight(context, dividedBy: 15),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.transparent,
                              border: Border.all(color: Color(0xffC3C5CB)),
                            ),
                            child: Text(
                              "Numericals",
                            ),
                          ),
                        ),
                        Tab(
                          child: Container(
                              // width: screenWidth(context, dividedBy: 2),
                              // height: screenHeight(context, dividedBy: 15),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.transparent,
                                  border: Border.all(color: Color(0xffC3C5CB))),
                              child: Text("Reasoning")),
                        )
                      ],
                    )),
                body: StreamBuilder<GetQuestionTypeResponse>(
                    stream: userBloc.getQuestionTypeResponse,
                    builder: (context, snapshot) {
                      return snapshot.hasError
                          ? Center(
                              child: Text(
                              snapshot.error.toString(),
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.blueGrey,
                              ),
                            ))
                          : snapshot.hasData
                              ? TabBarView(
                                  physics: NeverScrollableScrollPhysics(),
                                  children: [
                                      //aptitude tab
                                      SingleChildScrollView(
                                        child: Container(
                                          color: Colors.white,
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                  height: screenHeight(context,
                                                      dividedBy: 30)),
                                              Container(
                                                  width: screenWidth(context,
                                                      dividedBy: 1),
                                                  height: screenHeight(context,
                                                      dividedBy: 1),
                                                  //color: Colors.blue,
                                                  child: ListView.builder(
                                                    physics:
                                                        NeverScrollableScrollPhysics(),
                                                    itemCount: snapshot
                                                        .data.data.length,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 1),
                                                        child: AptitudeTile(
                                                          title: snapshot.data
                                                              .data[index].name
                                                              .toString(),
                                                          onPressed: () {
                                                            print("Id" +
                                                                ObjectFactory()
                                                                    .appHive
                                                                    .getUserId()
                                                                    .toString());

                                                            ObjectFactory().appHive.putExamId(
                                                                examId: ObjectFactory()
                                                                        .appHive
                                                                        .getUserId()
                                                                        .toString() +
                                                                    snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .id
                                                                        .toString() +
                                                                    ObjectFactory()
                                                                        .appHive
                                                                        .getExamCompletedStatus()
                                                                        .toString());

                                                            push(
                                                                context,
                                                                ResumeVideoPage(
                                                                  id: snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .id,
                                                                  title: snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .title,
                                                                  url: snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .videoUrl
                                                                      ,
                                                                  description: ""
                                                                ));
                                                          },
                                                        ),
                                                      );
                                                    },
                                                  )),
                                              SizedBox(
                                                height: screenHeight(context,
                                                    dividedBy: 30),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // Reasoning Tab
                                      SingleChildScrollView(
                                        child: Container(
                                          color: Colors.white,
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                  height: screenHeight(context,
                                                      dividedBy: 25)),
                                              Container(
                                                width: screenWidth(context,
                                                    dividedBy: 1),
                                                height: screenHeight(context,
                                                    dividedBy: 1),
                                                //color: Colors.blue,
                                                child: ListView.builder(
                                                  physics:
                                                      NeverScrollableScrollPhysics(),
                                                  itemCount:
                                                      snapshot.data.data.length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return Container(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 1),
                                                      child: AptitudeTile(
                                                        title: snapshot.data
                                                            .data[index].name
                                                            .toString(),
                                                        onPressed: () {
                                                          ObjectFactory().appHive.putExamId(
                                                              examId: ObjectFactory()
                                                                      .appHive
                                                                      .getUserId()
                                                                      .toString() +
                                                                  snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .id
                                                                      .toString() +
                                                                  (ObjectFactory()
                                                                              .appHive
                                                                              .getExamCompletedStatus() !=
                                                                          null
                                                                      ? ObjectFactory()
                                                                          .appHive
                                                                          .getExamCompletedStatus()
                                                                          .toString()
                                                                      : 1.toString()));

                                                          push(
                                                              context,
                                                              ResumeVideoPage(
                                                                id: snapshot
                                                                    .data
                                                                    .data[index]
                                                                    .id,
                                                                title: snapshot
                                                                    .data
                                                                    .data[index]
                                                                    .title,
                                                                url: snapshot
                                                                    .data
                                                                    .data[index]
                                                                    .videoUrl,
                                                                  description:""

                                                              ));
                                                        },
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                              SizedBox(
                                                height: screenHeight(context,
                                                    dividedBy: 30),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ])
                              : Container(
                                  height: screenHeight(context, dividedBy: 1.3),
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Constants.kitGradients[2]),
                                    ),
                                  ),
                                );
                    }),
              ),
            ),
          ),
        ),
      ),
    ));
  }
}
