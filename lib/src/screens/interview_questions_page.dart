import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/interview_question_answer_response_model.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/interview_question_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/screens/resources_page.dart';

class InterviewQuestions extends StatefulWidget {
  int id;
  String pageTitle;
  InterviewQuestions({this.id, this.pageTitle});

  @override
  _InterviewQuestionsState createState() => _InterviewQuestionsState();
}

class _InterviewQuestionsState extends State<InterviewQuestions> {
  int countSelected;

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, ResourcesPage(), false);
    // pop(context);
    // push(context, HomePage());
    return true;
  }

  UserBloc userBloc = new UserBloc();
  @override
  void initState() {
    userBloc.getInterviewQuestionsAnswer("1");

    userBloc.getInterviewQuestionAnswerResponse
        .listen((event) {})
        .onError((event) {
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.white,
            statusBarIconBrightness: Brightness.dark),
        child: WillPopScope(
          onWillPop: _willPopCallback,
          child: SafeArea(
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Colors.white,
                elevation: 0,
                leading: Container(),
                actions: [
                  CAppBar(
                    title: widget.pageTitle,
                    color: Colors.white,
                    isWhite: false,
                    onPressedLeftIcon: () {
                      pop(context);
                    },
                  ),
                ],
              ),
              body: SingleChildScrollView(
                child: Container(
                    color: Colors.white,
                    width: screenWidth(context, dividedBy: 1),
                    child: StreamBuilder<InterviewQuestionAnswerResponseModel>(
                        stream: userBloc.getInterviewQuestionAnswerResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasError
                              ? Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 1.2),
                                  child: Center(
                                      child: Text(
                                    snapshot.error.toString(),
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.blueGrey,
                                    ),
                                  )),
                                )
                              : snapshot.hasData
                                  ? ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: snapshot
                                          .data.data[widget.id].qa.questions.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Column(
                                          children: [
                                            InterviewQuestionTile(
                                              onTap: () {
                                                print(index.toString() + "index");
                                                if (countSelected == index) {
                                                  countSelected = null;
                                                  setState(() {});
                                                } else {
                                                  setState(() {
                                                    countSelected = index;
                                                  });
                                                }
                                              },
                                              countSelected: countSelected,
                                              index: index,
                                              count: (index + 1).toString(),
                                              question: snapshot.data.data[0].qa
                                                  .questions[index].question
                                                  .toString(),
                                              answer: snapshot.data.data[0].qa
                                                  .questions[index].answers,
                                            ),
                                          ],
                                        );
                                      },
                                    )
                                  : Container(
                                      height: screenHeight(context, dividedBy: 1.3),
                                      width: screenWidth(context, dividedBy: 1),
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  Constants.kitGradients[2]),
                                        ),
                                      ),
                                    );
                        })),
              ),
            ),
          ),
        ));
  }
}
