import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/screens/onboarding_screen1.dart';
import 'package:app_template/src/screens/otp_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/auth_button.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/textfield.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FirebaseAuth auth = FirebaseAuth.instance;
  UserBloc userBloc = UserBloc();
  bool isLoading = false;
  bool isLoadingPhone = false;
  bool phoneLogin = false;
  String verificationId;
  TextEditingController _phoneNumberTextEditingController =
      TextEditingController();

  @override
  void initState() {
    userBloc.loginResponse.listen((event) async {
      if (event.status == 200) {
        print("Subscription Plann  " +
            event.data.userData.userSubscriptionPlan.toString());
        event.data.userData.userSubscriptionPlan == null
            ? ObjectFactory().appHive.putOrderStatus(status: false)
            : ObjectFactory().appHive.putOrderStatus(status: true);
        ObjectFactory().appHive.putSignedInCheck(true);
        await ObjectFactory()
            .appHive
            .putUserId(userId: event.data.userData.uid);
        await ObjectFactory().appHive.putId(id: event.data.userData.id);
        print(ObjectFactory().appHive.getUserId());
        ObjectFactory().appHive.putToken(token: event.data.token);
        // ObjectFactory().appHive.putName(name:event.data.userData.firstName);
        // ObjectFactory().appHive.putEmail(event.data.userData.email);
        print("token : " + event.data.token);
        // print("firstName   " + ObjectFactory().appHive.getName());
        // print("email  " + ObjectFactory().appHive.getEmail());
        // print("uuid" + ObjectFactory().appHive.putToken(token: event.data.token));
        print("Listen");
        setState(() {
          isLoading = false;
        });
        pushAndRemoveUntil(context, OnBoardingScreen(), false);
      }
      setState(() {
        isLoading = false;
      });
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark),
      child: DoubleBack(
        message: "Press back again quit",
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(
                  flex: 6,
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.4),
                  height: screenHeight(context, dividedBy: 7),
                  child: SvgPicture.asset(
                    "assets/images/mind_logo.svg",
                    fit: BoxFit.cover,
                  ),
                ),
                Spacer(
                  flex: 4,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment:MainAxisAlignment.start,
                        crossAxisAlignment:CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Welcome to",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 12),
                              fontWeight: FontWeight.w600,
                              color: Constants.kitGradients[7],
                              fontFamily: "SofiaProRegular",
                            ),
                          ),
                          SizedBox(
                            width: screenWidth(context,
                                dividedBy: 90),
                          ),
                          Text(
                            "Upyou",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 12),
                              fontWeight: FontWeight.w800,
                              color: Constants.kitGradients[2],
                              fontFamily: "SofiaProRegular",
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 90),
                      ),
                      Text(
                        "Please enter your Mobile Number, We will send you an OTP on this mobile Number",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Constants.kitGradients[7].withOpacity(0.4),
                          fontFamily: "SofiaProRegular",
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 30)),
                  child: CTextField(
                      // onSubmitted: (value) {
                      //   print("hai");
                      //   value = _phoneNumberTextEditingController.text;
                      //   setState(() {
                      //     isLoading = true;
                      //   });
                      //   if (_phoneNumberTextEditingController.text.length >= 10)
                      //     signInWithPhone(_phoneNumberTextEditingController.text);
                      // },
                      borderColor: Constants.kitGradients[2],
                      isNumber: true,
                      label: "PhoneNumber",
                      textEditingController: _phoneNumberTextEditingController),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 9)),
                  child: CButton(
                    onPressed: () {
                      if (isLoading == false) {
                        setState(() {
                          isLoadingPhone = true;
                        });
                        if (_phoneNumberTextEditingController.text.length >=
                            10) {
                          signInWithPhone(
                              _phoneNumberTextEditingController.text);
                        } else {
                          showToast("Please enter the Phone Number.");
                          setState(() {
                            isLoadingPhone = false;
                          });
                        }
                      }
                      setState((){
                        ObjectFactory().appHive.putPhoneNumber(phoneNumber:_phoneNumberTextEditingController.text);
                      });
                    },
                    title: "VerifyNumber",
                    isLoading: isLoadingPhone,
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 7)),
                  child: AuthButton(
                    onPressed: () async {
                      if (isLoadingPhone == false) {
                        setState(() {
                          isLoading = true;
                        });
                        signInWithGoogle();
                      }
                    },
                    boxWidth: 1.2,
                    buttonColor: Colors.white,
                    isLoading: isLoading,
                    mediaSymbol: "assets/icons/google.svg",
                    mediaName: "Login with Google",
                  ),
                ),
                Spacer(
                  flex: 6,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> userCheck(googleUser) {
    if (googleUser == null) {
      setState(() {
        isLoading = false;
      });
    } else {
      return null;
    }
  }

  Future<UserCredential> signInWithGoogle() async {
    phoneLogin = false;
    // Trigger the authentication flow
    try {
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();
      userCheck(googleUser);

      // Obtain the auth details from the request
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      // Create a new credential
      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await auth.signInWithCredential(credential);

      if (googleUser != null) {
        print("uuid   " + auth.currentUser.uid);
        userBloc.login(
            loginRequest: LoginRequestModel(uid: auth.currentUser.uid));
        ObjectFactory().appHive.putPhotoUrl(auth.currentUser.photoURL);
        ObjectFactory().appHive.putName(name: auth.currentUser.displayName);
        ObjectFactory().appHive.putEmail(auth.currentUser.email);
        print("firstName :  " + ObjectFactory().appHive.getName());
        print("email :  " + ObjectFactory().appHive.getEmail());
      } else {
        setState(() {
          isLoading = false;
        });
        showToast("Network Error");
      }

      // Once signed in, return the UserCredential
      return await FirebaseAuth.instance.signInWithCredential(credential);
    } on FirebaseAuthException catch (e) {
      print("error");
      setState(() {
        isLoading = false;
      });
      String status = e.message.trim();
      showToast(status);
      // showTopSnackBar(
      //   context,
      //   CustomSnackBar.error(
      //     message: status,
      //   ),
      // );
      // showToast(status);
    }
  }

  void signInWithPhone(String phoneNumber) async {
    phoneLogin = true;
    // emit(AuthLoading());
    FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: "+91" + phoneNumber,
      timeout: Duration(seconds: 120),
      verificationCompleted: (AuthCredential authCredential) {
        FirebaseAuth.instance
            .signInWithCredential(authCredential)
            .then((value) async {
          userBloc.login(
              loginRequest: LoginRequestModel(
            uid: value.user.uid,
          ));
          // emit(PhoneVerified(phone: "+91" + phoneNumber));
          print("firebaseauth" + authCredential.signInMethod);
        });
      },
      verificationFailed: (FirebaseAuthException authException) {
        setState(() {
          isLoadingPhone = false;
        });
        print(authException.toString());
        showToast(authException.toString());
        // showSnackbar(authException.message);
      },
      codeSent: (value, [data]) async {
        verificationId = value;
        print("verifi" + verificationId.toString());
        setState(() {
          isLoadingPhone = false;
        });
      },
      codeAutoRetrievalTimeout: (value) {
        setState(() {
          isLoadingPhone = false;
        });
        push(
            context,
            OtpPage(
              phoneNumber: _phoneNumberTextEditingController.text,
              verificationId: verificationId,
            ));
        showToast("code auto retrieval timed out ");

        // emit(PhoneVerified(verificationId: value, phone: "+91" + phoneNumber));
      },
    );
  }

  // Future<UserCredential> signInWithFacebook() async {
  //   // Trigger the sign-in flow
  //   final LoginResult result = await FacebookAuth.instance.login();
  //
  //   // Create a credential from the access token
  //   final FacebookAuthCredential facebookAuthCredential =
  //       FacebookAuthProvider.credential(result.accessToken.token);
  //
  //   // Once signed in, return the UserCredential
  //   return await FirebaseAuth.instance
  //       .signInWithCredential(facebookAuthCredential);
  // }
}
