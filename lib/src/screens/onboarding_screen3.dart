import 'package:app_template/src/screens/onboarding_screen4.dart';
import 'package:app_template/src/screens/slide_transition.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swipedetector/swipedetector.dart';

class OnBoardingScreen3 extends StatefulWidget {
  @override
  _OnBoardingScreen3State createState() => _OnBoardingScreen3State();
}

class _OnBoardingScreen3State extends State<OnBoardingScreen3> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
        statusBarColor: Color(0xffD9E7FD),
    ),
    child:SafeArea(
      child: SwipeDetector(
        onSwipeLeft: () {
          Navigator.of(context)
              .push(SlideRightRoute(page: OnBoardingScreen4()));
        },
        child: Scaffold(
            // extendBodyBehindAppBar: true,
            body: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 1),
          child: OnBoardingContent(
            head: "Are you Aware!!",
            index: 2,
            data: "Your resume needs to be rightly tailored to get through ATS "
                "software used by recruiters. No worries, here we help you"
                " have an ATS Friendly Resume.",
            image: "assets/images/on_boarding3.svg",
            buttonData: "Next",
            onPressed: () {
              Navigator.of(context)
                  .push(SlideRightRoute(page: OnBoardingScreen4()));
              //Navigator.push(context, SlideRightRoute(page: OnBoardingScreen4()));
              //push(context, OnBoardingScreen4());
            },
          ),
        )),
      ),
    ));
  }
}
