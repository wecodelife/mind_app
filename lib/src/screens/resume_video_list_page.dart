import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_resume_preparation.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/resume_video_list_tile.dart';
import 'package:app_template/src/screens/resume_video_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class ResumePreparationVideoListPage extends StatefulWidget {
  final int id;
  final String title;

  ResumePreparationVideoListPage({
    this.id,
    this.title,
  });
  @override
  _ResumePreparationVideoListPageState createState() =>
      _ResumePreparationVideoListPageState();
}

class _ResumePreparationVideoListPageState
    extends State<ResumePreparationVideoListPage> {
  UserBloc userBloc = UserBloc();
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  @override
  void initState() {
    userBloc.getResumePreparation(videoId: widget.id.toString());
    userBloc.getGetResumePreparationResponse
        .listen((event) {})
        .onError((event) {
      // if (event[0] == "0") {
      //   showToast(event.toString().substring(0, event.toString().length));
      // } else {
      //   showToast(event.toString());
      // }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          toolbarHeight: screenHeight(context, dividedBy: 12),
          elevation: 0,
          leading: Container(),
          actions: [
            CAppBar(
              title: widget.title,
              color: Colors.white,
              isWhite: false,
              onPressedLeftIcon: () {
                pushAndRemoveUntil(context, BottomBar(), false);
              },
            ),
          ],
        ),
        body: Column(
          children: [
            StreamBuilder<GetResumePreparationResponse>(
                stream: userBloc.getGetResumePreparationResponse,
                builder: (context, snapshot) {
                  return snapshot.hasError
                      ? Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 1.2),
                          child: Center(
                              child: Text(
                            snapshot.error.toString(),
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.blueGrey,
                            ),
                          )),
                        )
                      : snapshot.hasData
                          ? snapshot.data.data.length > 1
                              ? Expanded(
                                  flex: 1,
                                  child: ListView.builder(
                                      padding: EdgeInsets.only(
                                          top: screenHeight(context,
                                              dividedBy: 50)),
                                      shrinkWrap: true,
                                      scrollDirection: Axis.vertical,
                                      physics: AlwaysScrollableScrollPhysics(),
                                      itemCount: snapshot.data.data.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Column(
                                          children: [
                                            ResumeVideoListTile(
                                              controller:
                                                  YoutubePlayerController(
                                                initialVideoId: snapshot
                                                    .data.data[index].videoUrl
                                                    .split("=")
                                                    .last,

                                                // 'cSLAO7zxS2M',
                                                flags: YoutubePlayerFlags(
                                                    autoPlay: false,
                                                    mute: false,
                                                    hideControls: false,
                                                    hideThumbnail: false),
                                              ),
                                              question:
                                                  // "What is a resume? and why is it important to make one",
                                                  snapshot
                                                      .data.data[index].title,
                                              subHeading: snapshot
                                                  .data.data[index].description,

                                              // "Lorem ipsum dolor sit amet, consectetur adipiscing elit, "
                                              // "sed do eiusmod tempor incididunt ut labore et oe.ctetur adipiscing elitsed do eiusmod tempor incididunt ut labore et doe.",
                                              onTap: () {
                                                pushAndReplacement(
                                                    context,
                                                    ResumeVideoPage(
                                                      id: snapshot
                                                          .data.data[index].id,
                                                      title: snapshot.data
                                                          .data[index].title,
                                                      url: snapshot.data
                                                          .data[index].videoUrl
                                                          .split("=")
                                                          .last,
                                                      button: widget.id == 0
                                                          ? true
                                                          : false,
                                                    ));
                                              },
                                            ),
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 40),
                                            )
                                          ],
                                        );
                                      }),
                                )
                              : Container(
                                  height: screenHeight(context, dividedBy: 1.3),
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Center(
                                    child: Text("No Videos Available",
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.blueGrey,
                                            fontWeight: FontWeight.w600)),
                                  ))
                          : Container(
                              height: screenHeight(context, dividedBy: 1.3),
                              width: screenWidth(context, dividedBy: 1),
                              child: Center(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Constants.kitGradients[2]),
                                ),
                              ),
                            );
                }),
          ],
        ),
      ),
    );
  }
}
