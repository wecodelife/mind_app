import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/screens/job_alert_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/job_alert_cards.dart';
import 'package:app_template/src/widgets/job_filter.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewJobAlerts extends StatefulWidget {
  final List<String> jobList;
  ViewJobAlerts({this.jobList});
  @override
  _ViewJobAlertsState createState() => _ViewJobAlertsState();
}

class _ViewJobAlertsState extends State<ViewJobAlerts> {
  UserBloc userBloc = new UserBloc();

  bool jobSelected = false;
  bool internShipSelected = false;
  bool fullTimeSelected = false;
  bool partTimeSelected = false;
  String jobValue = "";
  String type = "";
  String involvement = "";
  ScrollController _scrollController = ScrollController();
  List<String> title = [];
  List<String> description = [];
  List<String> datePosted = [];
  List<String> experience = [];
  List<String> companyName = [];
  List<String> endDate = [];
  List<String> image = [];
  List<bool> isImage = [];
  List<bool> jobApplyTrue = [];
  List<String> jobApplyLink = [];
  List<String> phoneNumber = [];
  bool isLoading = true;
  String next;
  bool loadMore = true;
  bool showStatus = false;

  Future jobSelectedCheck(int index) {
    if (index == 1) {
      if (jobSelected == false) {
        jobSelected = true;
        print("JobSelected" + jobSelected.toString());
        if (internShipSelected == true) {
          type = "";
        } else {
          type = "2";
        }
      } else {
        jobSelected = false;
        if (internShipSelected == true) {
          print("type3" + type.toString());
          type = "1";
        } else {
          type = 1.toString();
          print("type 4" + type.toString());
        }
      }
    }
  }

  Future internShipSelectedCheck(int index) {
    if (index == 0) {
      if (internShipSelected == false) {
        internShipSelected = true;
        if (jobSelected == true) {
          type = "";
        } else {
          type = 1.toString();
        }
        // type = 1.toString();

      } else {
        internShipSelected = false;
        if (jobSelected == true) {
          type = 2.toString();
        } else {
          type = "";
        }
      }
    }
  }

  Future fullTimeSelectedCheck(int index) {
    if (index == 0) {
      if (fullTimeSelected == false) {
        fullTimeSelected = true;
        if (partTimeSelected == true) {
          involvement = "";
        } else {
          involvement = 1.toString();
        }
      } else {
        fullTimeSelected = false;
        if (partTimeSelected == true) {
          involvement = 2.toString();
        } else {
          involvement = "";
        }
      }
    }
  }

  Future partTimeSelectedCheck(int index) {
    if (index == 1) {
      if (partTimeSelected == false) {
        partTimeSelected = true;
        if (fullTimeSelected == true) {
          involvement = "";
        } else {
          involvement = "2";
        }
      } else {
        partTimeSelected = false;
        if (fullTimeSelected == true) {
          involvement = "1";
        } else {
          involvement = "";
        }
      }
    }
  }

  String url;
  void emailCheck(url) {
    final bool isValid = EmailValidator.validate(url);
    if (isValid == true) {
      send(url);
    } else {
      _launchURL(url);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    ObjectFactory().appHive.putResponseStatusJobList(value: false);

    userBloc.getJobRead(
      involvement: "",
      jobType: "",
      category: widget.jobList,
    );
    _scrollController.addListener(() {
      print(_scrollController.offset.round());
      if (_scrollController.position.pixels ==
          (_scrollController.position.maxScrollExtent)) {
        print("Hello World");
        setState(() {
          loadMore = true;
        });
        userBloc.getJobRead(
            category: widget.jobList, jobType: "", involvement: "", next: next);
      }
    });
    userBloc.getJobReadResponse.listen((event) async {
      if (ObjectFactory().appHive.getResponseStatusJobList() != null) {
        if (event.results != null) {
          for (int i = 0; i < event.results.length; i++) {
            title.add(event.results[i].title);
            description.add(event.results[i].description);
            // experience.add(event.results[i].id.toString())
            datePosted.add(
              event.results[i].postedOn == null
                  ? ""
                  : DateFormat('dd-MM-yyyy')
                      .format(event.results[i].postedOn)
                      .toString(),
            );
            endDate.add(
              event.results[i].endDate == null
                  ? ""
                  : DateFormat('dd-MM-yyyy')
                      .format(event.results[i].endDate)
                      .toString(),
            );
            companyName.add(event.results[i].company);
            experience.add(event.results[i].experience);
            image.add(event.results[i].image);
            isImage.add(event.results[i].image.toString() == "" ? false : true);
            jobApplyTrue.add(
              event.results[i].applicationUrl == null ? false : true,
            );
            jobApplyLink.add(event.results[i].applicationUrl.toString());
            phoneNumber.add(event.results[i].phoneNumber);
          }
          next = event.count.toString();
          setState(() {
            print("ggg" + title.length.toString());
            isLoading = false;
            loadMore = false;
          });
          ObjectFactory().appHive.putResponseStatusJobList(value: false);
        }
      } else {
        ObjectFactory().appHive.putResponseStatusJobList(value: false);
        setState(() {
          loadMore = false;
          showStatus = true;
        });
      }
    }).onError((event) {
      setState(() {
        showStatus = true;
      });
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, JobAlertPage(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.white,
            statusBarIconBrightness: Brightness.dark),
        child: WillPopScope(
          onWillPop: _willPopCallback,
          child: SafeArea(
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Colors.white,
                elevation: 0,
                leading: Container(),
                actions: [
                  CAppBar(
                    title: "Job Alert",
                    color: Colors.white,
                    isWhite: false,
                    onPressedLeftIcon: () {
                      pop(context);
                    },
                  ),
                ],
              ),
              body: showStatus == false
                  ? SingleChildScrollView(
                      controller: _scrollController,
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        // height:screenHeight(context, dividedBy:1),
                        //color: Colors.blue,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                //color: Color(0xffC3C5CB),
                                child: Column(children: [
                              ExpansionTile(
                                backgroundColor: Color(0xffD9E7FD),
                                trailing: Icon(Icons.arrow_drop_down_sharp,
                                    color: Colors.black),
                                title: Text("Filter",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: "SofiaProRegular",
                                        color: Colors.black)),
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 90),
                                  ),
                                  JobFilter(
                                      filter: "Type",
                                      value: ["Internship", "Job"],
                                      valueChanged: (index) async {
                                        print(index);
                                        title.clear();
                                        description.clear();
                                        datePosted.clear();
                                        companyName.clear();
                                        endDate.clear();
                                        experience.clear();
                                        await jobSelectedCheck(index);

                                        await internShipSelectedCheck(index);
                                        setState(() {
                                          isLoading = true;
                                        });
                                        userBloc.getJobRead(
                                            jobType: type,
                                            involvement: involvement,
                                            category: widget.jobList);
                                      }),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 90),
                                  ),
                                  JobFilter(
                                    filter: "Involvement",
                                    value: ["Full Time", "Part Time"],
                                    valueChanged: (index) async {
                                      title.clear();
                                      description.clear();
                                      datePosted.clear();
                                      companyName.clear();
                                      endDate.clear();
                                      experience.clear();
                                      await fullTimeSelectedCheck(index);
                                      await partTimeSelectedCheck(index);
                                      setState(() {
                                        isLoading = true;
                                      });
                                      userBloc.getJobRead(
                                          jobType: type,
                                          involvement: involvement,
                                          category: widget.jobList);
                                    },
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 50),
                                  ),
                                ],
                              ),
                            ])),
                            isLoading == false
                                ? ListView.builder(
                                    itemCount: title.length,
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return JobAlertCard(
                                        isImage: true,
                                        title: title[index],
                                        description: description[index],
                                        date: datePosted[index],
                                        experience: "3",
                                        image: image[index],
                                        postedDate: datePosted[index],
                                        company: companyName[index],
                                        endDate: endDate[index],
                                        button: jobApplyTrue[index],
                                        onApply: () {
                                          setState(() {});
                                          url = jobApplyLink[index].toString();
                                          if (url != null) {
                                            emailCheck(url);
                                          } else {
                                            _calling(phoneNumber[index]);
                                          }
                                        },
                                      );
                                      // : Container(
                                      //     height:
                                      //         screenHeight(context, dividedBy: 1.7),
                                      //     width: screenWidth(context, dividedBy: 1),
                                      //     child: Center(
                                      //       child: CircularProgressIndicator(
                                      //         valueColor:
                                      //             new AlwaysStoppedAnimation<Color>(
                                      //                 Constants.kitGradients[2]),
                                      //       ),
                                      //     ),
                                      //   );
                                    })
                                : Container(
                                    height:
                                        screenHeight(context, dividedBy: 1.2),
                                    width: screenWidth(context, dividedBy: 1),
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                Constants.kitGradients[2]),
                                      ),
                                    ),
                                  ),
                            loadMore == true
                                ? Container(
                                    height: screenHeight(context, dividedBy: 4),
                                    width: screenWidth(context, dividedBy: 1),
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                Constants.kitGradients[2]),
                                      ),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                    )
                  : Container(
                      height: screenHeight(context, dividedBy: 1.3),
                      width: screenWidth(context, dividedBy: 1),
                      child: Center(
                        child: Text("SomeThing Went Wrong",
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.blueGrey,
                                fontWeight: FontWeight.w600)),
                      )),
            ),
          ),
        ));
  }

  Future<void> _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _calling(String phoneNumber) async {
    String url = 'tel:+' + phoneNumber;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> send(String recipients) async {
    final Email email = Email(
      body: "",
      subject: "",
      recipients: [recipients],
    );

    String platformResponse;

    try {
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
    } catch (error) {
      platformResponse = error.toString();
    }
  }
}
