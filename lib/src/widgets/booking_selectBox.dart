import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class BookingSelectionBox extends StatefulWidget {
  final String title;
  final Function onSelected;
  final int index;
  BookingSelectionBox({this.title, this.onSelected, this.index});
  @override
  _BookingSelectionBoxState createState() => _BookingSelectionBoxState();
}

class _BookingSelectionBoxState extends State<BookingSelectionBox> {
  //Color changeColor = Color(0xff828282);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          widget.onSelected();
          setState(() {
            //changeColor = Color(0xffD5A739);
          });
        },
        child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 6),
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: widget.title.contains("Elite")
                  ? Color(0xffD5A739)
                  : Color(0xff828282),
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Color(0xffA8A8A8)),
            ),
            child: Shimmer.fromColors(
              baseColor: Colors.white,
              highlightColor: Colors.grey,
              child: Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 30),
                    child: Container(),
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: screenWidth(context, dividedBy: 20),
                        fontFamily: "SofiaProRegular"),
                  ),
                ],
              ),
            )));
  }
}
