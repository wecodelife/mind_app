import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AuthButton extends StatefulWidget {
  Color buttonColor;
  String mediaSymbol;
  String mediaName;
  double boxWidth;
  Function onPressed;
  bool isLoading;
  bool iconColor;
  AuthButton(
      {this.boxWidth,
      this.iconColor,
      this.buttonColor,
      this.mediaName,
      this.mediaSymbol,
      this.onPressed,
      this.isLoading});
  @override
  _AuthButtonState createState() => _AuthButtonState();
}

class _AuthButtonState extends State<AuthButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: widget.boxWidth),
        height: screenHeight(context, dividedBy: 14),
        child: RaisedButton(
          padding: EdgeInsets.only(),
          color: widget.buttonColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          onPressed: () {
            widget.onPressed();
          },
          child: widget.isLoading == true
              ? Container(
                  height: screenHeight(context, dividedBy: widget.boxWidth),
                  width: screenWidth(context, dividedBy: 1),
                  color: Colors.white,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          Constants.kitGradients[2]),
                    ),
                  ),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Spacer(
                      flex: 7,
                    ),
                    Container(
                        height: screenHeight(context, dividedBy: 40),
                        child: widget.iconColor != true
                            ? SvgPicture.asset(
                                widget.mediaSymbol,
                                width: screenWidth(context, dividedBy: 17),
                              )
                            : SvgPicture.asset(
                                widget.mediaSymbol,
                                color: Constants.kitGradients[0],
                                width: screenWidth(context, dividedBy: 17),
                              )),
                    Spacer(
                      flex: 1,
                    ),
                    Text(
                      widget.mediaName,
                      style: TextStyle(
                        color: widget.buttonColor == Colors.white
                            ? Colors.black
                            : Colors.white,
                        fontFamily: 'MuliBold',
                        fontSize: screenWidth(context, dividedBy: 29),
                      ),
                      overflow: TextOverflow.clip,
                    ),
                    Spacer(
                      flex: 7,
                    )
                  ],
                ),
        ));
  }
}
