import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';

class FilterChips extends StatefulWidget {
  final String values;
  final Function onPressed;
  final valueChanged;
  final int index;
  FilterChips({this.values, this.index, this.onPressed, this.valueChanged});
  @override
  _FilterChipsState createState() => _FilterChipsState();
}

class _FilterChipsState extends State<FilterChips> {
  bool valueSet = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // widget.valueChanged(widget.index);
        widget.onPressed();
        setState(() {
          valueSet = !valueSet;
        });
      },
      child: Container(
        // width: screenWidth(context, dividedBy:20),
        //height: screenHeight(context, dividedBy: 20),
        margin: EdgeInsets.symmetric(horizontal: 7),
        padding: EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
            color: valueSet ? Constants.kitGradients[2] : Colors.white,
            borderRadius: BorderRadius.circular(25),
            border: Border.all(color: Color(0xffC3C5CB))),
        child: Center(
          child: Text(
            widget.values,
            style: TextStyle(
                fontSize: 12,
                fontFamily: "SofiaProRegular",
                color: valueSet ? Colors.white : Color(0xffC3C5CB)),
          ),
        ),
      ),
    );
  }
}
