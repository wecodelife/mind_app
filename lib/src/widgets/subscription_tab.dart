import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/screens/payment_page.dart';
import 'package:app_template/src/screens/subscription_details_page.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/alert_box_list_tile.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/subscription_tab_header.dart';
import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shimmer/shimmer.dart';
import 'package:app_template/src/models/get_subscription_plans_response.dart';
import 'package:app_template/src/models/order_plan_request_model.dart';
import 'package:url_launcher/url_launcher.dart';

class SubscriptionTab extends StatefulWidget {
  // const SubscriptionTab({Key? key}) : super(key: key);
  // final int id;
  // SubscriptionTab({this.id});

  @override
  _SubscriptionTabState createState() => _SubscriptionTabState();
}

class _SubscriptionTabState extends State<SubscriptionTab> {
  int currentIndex = 1;
  List<String> package1 = [
    // "Complete Videos Resources",
    // "Complete Text Resources",
  ];
  List<String> package2 = [
    // "Complete Videos Resources",
    // "Complete Text Resources",
    // "One - Mock Interview",
    // "Single - Resume Building",
  ];
  List<String> package3 = [
    // "Complete Videos Resources",
    // "Complete Text Resources",
    // "Two - Mock Interview",
    // "Single - Resume Building",
  ];

  int selectedItem = 1;
  UserBloc userBloc = UserBloc();
  bool isLoading = true;
  List<String> sliderImages = [];
  // List<Datum> data;
  String orderId;
  bool email;
  bool phoneNumber;
  List<String> compliment = [];
  List<String> packageInclude = [];
  int subscriptionId;
  TextEditingController dataTextEditingController = TextEditingController();

  @override
  void initState() {
    setState(() {
      email = ObjectFactory().appHive.getEmail() == null ? true : false;
      phoneNumber =
          ObjectFactory().appHive.getPhoneNumber() == null ? true : false;
    });
    ObjectFactory().appHive.getEmail() == null
        ? print("Email Id No value ")
        : print("Email id " + ObjectFactory().appHive.getEmail());
    print(
        "Phone Number  " + ObjectFactory().appHive.getPhoneNumber().toString());

    userBloc.getSubscriptionPlan();
    userBloc.getSubscriptionPlans.listen((event) {
      if (event.status == 200 || event.status == 201) {
        print("Subscription Plans loaded Successfully");
        for (int i = 0;
            i < event.data[selectedItem].compliments.data.length;
            i++) {
          setState(() {
            compliment.add(event.data[selectedItem].compliments.data[i].title);
            packageInclude
                .add(event.data[selectedItem].packageInclude.data[i].title);
            subscriptionId = event.data[selectedItem].id;
          });
        }
        print("Compliments " + compliment.toString());
        print("Subscription plan ID  " + subscriptionId.toString());
        print(event.data);
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });

    userBloc.orderPlanResponse.listen((event) async {
      if (event.status == 200 || event.status == 201) {
        print("Subscription order response loaded Successfully");
        print("Payment Link  " + event.data.paymentLink);
        print("OrderId  " + event.data.orderId);
        print("Compliments data  ");
        print(compliment);
        setState(() {
          orderId = event.data.orderId;
        });
        if (event.data.paymentLink != null) {
          push(
              context,
              PaymentPage(
                url: event.data.paymentLink,
                orderId: event.data.orderId,
                compliments: compliment,
                subscriptionId: subscriptionId,
                // id:widget.id
              ));
        }
        //
        // if (await canLaunch(event.data.paymentLink)) {
        //   await launch(event.data.paymentLink);
        //   print("Payment Link loaded Successfully");
        // } else {
        //   throw 'Could not launch ' + event.data.paymentLink;
        // }
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
      });
      showToast(event.toString());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      child: Column(
        // mainAxisSize:MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              selectedItem == 1
                  ? Shimmer.fromColors(
                      baseColor: Constants.kitGradients[2].withOpacity(0.8),
                      highlightColor: Colors.grey[100],
                      child: Container(
                        // width: screenWidth(context, dividedBy: 4),
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 20),
                            vertical: screenHeight(context, dividedBy: 70)),
                        decoration: BoxDecoration(
                          color: Constants.kitGradients[2].withOpacity(0.3),
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(8),
                            bottomLeft: Radius.circular(8),
                          ),
                          // shadow:
                        ),
                        child: Center(
                          child: Text(
                            "Best Value",
                            style: TextStyle(
                                fontFamily: "SofiaProRegular",
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
          Padding(
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 40),
              ),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    selectedItem == 1
                        ? Container()
                        : SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                    Text(
                      "Annually",
                      style: TextStyle(
                          fontFamily: "SofiaProRegular",
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                    ),
                    // Spacer(),
                    // SizedBox(
                    //   width: screenWidth(context, dividedBy: 30),
                    // ),
                    // Padding(
                    //   padding: EdgeInsets.symmetric(
                    //     // horizontal: screenWidth(context, dividedBy: 30),
                    //   ),
                    //   child: Text(
                    //     "Annually",
                    //     style: TextStyle(
                    //         fontFamily: "SofiaProRegular",
                    //         fontSize: 20,
                    //         fontWeight: FontWeight.w500),
                    //   ),
                    // ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 130),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      child: StreamBuilder<GetSubscriptionPlansResponse>(
                        stream: userBloc.getSubscriptionPlans,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          // width: screenWidth(context, dividedBy: 2.9),
                                          padding: EdgeInsets.symmetric(
                                              // horizontal: screenWidth(context, dividedBy: 40),
                                              ),
                                          child: Label(
                                            triangleHeight: 10.0,
                                            edge: Edge.RIGHT,
                                            child: Container(
                                              // width: screenWidth(context, dividedBy: 2.9),
                                              padding: EdgeInsets.symmetric(
                                                horizontal: screenWidth(context,
                                                    dividedBy: 40),
                                                vertical: screenHeight(context,
                                                    dividedBy: 120),
                                              ),
                                              color: Colors.green,
                                              child: Row(children: [
                                                Text(
                                                  snapshot.data
                                                      .data[selectedItem].price
                                                      .toString(),
                                                  style: TextStyle(
                                                      fontFamily:
                                                          "SofiaProRegular",
                                                      fontSize: screenHeight(
                                                          context,
                                                          dividedBy: 50),
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      decoration: TextDecoration
                                                          .lineThrough,
                                                      decorationStyle:
                                                          TextDecorationStyle
                                                              .solid),
                                                ),
                                                Text(
                                                  " Save " +
                                                      (((snapshot
                                                                          .data
                                                                          .data[
                                                                              selectedItem]
                                                                          .price -
                                                                      snapshot
                                                                          .data
                                                                          .data[
                                                                              selectedItem]
                                                                          .offerPrice) /
                                                                  snapshot
                                                                      .data
                                                                      .data[
                                                                          selectedItem]
                                                                      .price) *
                                                              100)
                                                          .round()
                                                          .toString() +
                                                      "%",
                                                  style: TextStyle(
                                                      fontFamily:
                                                          "SofiaProRegular",
                                                      fontSize: screenHeight(
                                                          context,
                                                          dividedBy: 50),
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                SizedBox(
                                                  width: screenWidth(context,
                                                      dividedBy: 40),
                                                ),
                                              ]),
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                      ],
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 70),
                                    ),
                                    Container(
                                      width: screenWidth(context, dividedBy: 1),
                                      height:
                                          screenHeight(context, dividedBy: 10),
                                      alignment: Alignment.center,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: snapshot.data.data.length,
                                        shrinkWrap: true,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: [
                                              Container(
                                                child: Expanded(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      // Spacer(),
                                                      SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 90)),
                                                      SubscriptionTabHeader(
                                                        label: snapshot
                                                            .data
                                                            .data[index]
                                                            .offerPrice
                                                            .toString(),
                                                        indexValue: index,
                                                        selectedItem:
                                                            selectedItem,
                                                        onSelected: () {
                                                          setState(() {
                                                            selectedItem =
                                                                index;
                                                          });
                                                        },
                                                      ),
                                                      SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 90)),
                                                      // Spacer(),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                )
                              : Container();
                        },
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 120),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 50),
                        // vertical: screenHeight(context, dividedBy: 50)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: StreamBuilder<GetSubscriptionPlansResponse>(
                              stream: userBloc.getSubscriptionPlans,
                              builder: (context, snapshot) {
                                return snapshot.hasData
                                    ? Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                "Package Include",
                                                style: TextStyle(
                                                    fontFamily:
                                                        "SofiaProRegular",
                                                    fontSize: 17,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              Spacer(),
                                              GestureDetector(
                                                onTap: () {
                                                  push(
                                                      context,
                                                      SubscriptionDetailsPage(
                                                        currentIndex:
                                                            selectedItem,
                                                        // id:widget.id
                                                      ));
                                                },
                                                child: Text(
                                                  "View Details",
                                                  style: TextStyle(
                                                      decoration: TextDecoration
                                                          .underline,
                                                      fontFamily:
                                                          "SofiaProRegular",
                                                      fontSize: 14,
                                                      color: Constants
                                                          .kitGradients[2],
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                              SizedBox(
                                                width: screenWidth(context,
                                                    dividedBy: 40),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 70),
                                          ),
                                          Container(
                                            child: ListView.builder(
                                              itemCount: snapshot
                                                  .data
                                                  .data[selectedItem]
                                                  .packageInclude
                                                  .data
                                                  .length,
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return snapshot
                                                            .data
                                                            .data[selectedItem]
                                                            .packageInclude
                                                            .data
                                                            .length >
                                                        0
                                                    ? Column(
                                                        children: [
                                                          Row(children: [
                                                            Container(
                                                                child: SvgPicture.asset(
                                                                    "assets/icons/button_tick.svg",
                                                                    // fit: BoxFit.contain,
                                                                    width: screenWidth(
                                                                        context,
                                                                        dividedBy:
                                                                            30),
                                                                    height: screenHeight(
                                                                        context,
                                                                        dividedBy:
                                                                            30))),
                                                            SizedBox(
                                                              width:
                                                                  screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          30),
                                                            ),
                                                            Expanded(
                                                                child: Text(
                                                              snapshot
                                                                  .data
                                                                  .data[
                                                                      selectedItem]
                                                                  .packageInclude
                                                                  .data[index]
                                                                  .title,
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      "SofiaProRegular",
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ))
                                                          ]),
                                                          // AlertBoxListTile(
                                                          //   icon: Container(
                                                          //       child: SvgPicture.asset(
                                                          //           "assets/icons/button_tick.svg",
                                                          //           // fit: BoxFit.contain,
                                                          //           width: screenWidth(
                                                          //               context,
                                                          //               dividedBy:
                                                          //                   30),
                                                          //           height: screenHeight(
                                                          //               context,
                                                          //               dividedBy:
                                                          //                   30))),
                                                          //   data: snapshot
                                                          //       .data
                                                          //       .data[
                                                          //           selectedItem]
                                                          //       .packageInclude.data[index].title,
                                                          // ),
                                                          SizedBox(
                                                            height:
                                                                screenHeight(
                                                                    context,
                                                                    dividedBy:
                                                                        80),
                                                          ),
                                                        ],
                                                      )
                                                    : Container(
                                                        child: Center(
                                                          heightFactor: 1,
                                                          widthFactor: 1,
                                                          child: SizedBox(
                                                            height: 16,
                                                            width: 16,
                                                            child:
                                                                CircularProgressIndicator(
                                                              valueColor: AlwaysStoppedAnimation(
                                                                  Constants
                                                                      .kitGradients[
                                                                          2]
                                                                      .withOpacity(
                                                                          0.4)),
                                                              strokeWidth: 2,
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                              },
                                            ),
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 40),
                                          ),
                                          Text(
                                            "Compliment:",
                                            style: TextStyle(
                                                fontFamily: "SofiaProRegular",
                                                fontSize: 17,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 90),
                                          ),
                                          Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: screenWidth(
                                                      context,
                                                      dividedBy: 30),
                                                  vertical: screenHeight(
                                                      context,
                                                      dividedBy: 50)),
                                              decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                color: Constants.kitGradients[2]
                                                    .withOpacity(0.13),
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              child: ListView.builder(
                                                itemCount: snapshot
                                                    .data
                                                    .data[selectedItem]
                                                    .compliments
                                                    .data
                                                    .length,
                                                shrinkWrap: true,
                                                physics:
                                                    NeverScrollableScrollPhysics(),
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return snapshot
                                                              .data
                                                              .data[
                                                                  selectedItem]
                                                              .compliments
                                                              .data
                                                              .length >
                                                          0
                                                      ? Column(
                                                          children: [
                                                            Row(
                                                              children: [
                                                                Container(
                                                                  width: 4,
                                                                  height: 4,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    shape: BoxShape
                                                                        .circle,
                                                                    color: Constants
                                                                        .kitGradients[2],
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  width: screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          40),
                                                                ),
                                                                Expanded(
                                                                    child: Text(
                                                                  snapshot
                                                                      .data
                                                                      .data[
                                                                          selectedItem]
                                                                      .compliments
                                                                      .data[
                                                                          index]
                                                                      .title,
                                                                  style: TextStyle(
                                                                      fontFamily:
                                                                          "SofiaProRegular",
                                                                      fontSize:
                                                                          14,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ))
                                                              ],
                                                            ),
                                                            SizedBox(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          120),
                                                            ),
                                                          ],
                                                        )
                                                      : Container(
                                                          child: Center(
                                                            heightFactor: 1,
                                                            widthFactor: 1,
                                                            child: SizedBox(
                                                              height: 16,
                                                              width: 16,
                                                              child:
                                                                  CircularProgressIndicator(
                                                                valueColor: AlwaysStoppedAnimation(Constants
                                                                    .kitGradients[
                                                                        2]
                                                                    .withOpacity(
                                                                        0.4)),
                                                                strokeWidth: 2,
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                },
                                              )),
                                          SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 130),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 15),
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            height: screenHeight(context,
                                                dividedBy: 10),
                                            child: CButton(
                                              title: "Subscribe Now",
                                              onPressed: () async {
                                                print("Missing data " +
                                                    dataTextEditingController
                                                        .text);
                                                if (email == true ||
                                                    phoneNumber == true) {
                                                  showAddEmailAlert(
                                                    context,
                                                    isEmail: email == true
                                                        ? true
                                                        : false,
                                                    textEditingController:
                                                        dataTextEditingController,
                                                    onPressed: () {
                                                      phoneNumber == true
                                                          ? ObjectFactory()
                                                              .appHive
                                                              .putPhoneNumber(
                                                                  phoneNumber:
                                                                      dataTextEditingController
                                                                          .text)
                                                          : email == true
                                                              ? ObjectFactory()
                                                                  .appHive
                                                                  .putEmail(
                                                                      dataTextEditingController
                                                                          .text)
                                                              : setState(() {});
                                                      email == true
                                                          ? print(
                                                              "Email missing, added to hive " +
                                                                  ObjectFactory()
                                                                      .appHive
                                                                      .getEmail())
                                                          : print(
                                                              "Email is not missing");
                                                      phoneNumber == true
                                                          ? print("PhoneNumber missing, added to hive " +
                                                              ObjectFactory()
                                                                  .appHive
                                                                  .getPhoneNumber())
                                                          : print(
                                                              "Phone Number is not missing");

                                                      userBloc.orderPlans(
                                                          orderPlanRequest: OrderPlanRequest(
                                                              customerEmail:
                                                                  ObjectFactory()
                                                                      .appHive
                                                                      .getEmail(),
                                                              customerPhone:
                                                                  ObjectFactory()
                                                                      .appHive
                                                                      .getPhoneNumber()
                                                                      .toString(),
                                                              customerName:
                                                                  ObjectFactory()
                                                                      .appHive
                                                                      .getName(),
                                                              orderAmount: snapshot
                                                                  .data
                                                                  .data[
                                                                      selectedItem]
                                                                  .offerPrice
                                                                  .toString(),
                                                              orderCurrency:
                                                                  "INR",
                                                              orderNote:
                                                                  "test"));
                                                    },
                                                  );
                                                } else if (email != true &&
                                                    phoneNumber != true) {
                                                  print("Subscription ID of " +
                                                      selectedItem.toString() +
                                                      " is " +
                                                      snapshot.data
                                                          .data[selectedItem].id
                                                          .toString());
                                                  print(
                                                      "___________________________________________");
                                                  print("Missing data " +
                                                      dataTextEditingController
                                                          .text);
                                                  userBloc.orderPlans(
                                                      orderPlanRequest: OrderPlanRequest(
                                                          customerEmail: ObjectFactory()
                                                                      .appHive
                                                                      .getEmail() ==
                                                                  null
                                                              ? dataTextEditingController
                                                                  .text
                                                              : ObjectFactory()
                                                                  .appHive
                                                                  .getEmail(),
                                                          customerPhone: ObjectFactory()
                                                                      .appHive
                                                                      .getPhoneNumber() ==
                                                                  null
                                                              ? dataTextEditingController
                                                                  .text
                                                              : ObjectFactory()
                                                                  .appHive
                                                                  .getPhoneNumber()
                                                                  .toString(),
                                                          customerName:
                                                              ObjectFactory()
                                                                  .appHive
                                                                  .getName(),
                                                          orderAmount: snapshot
                                                              .data
                                                              .data[
                                                                  selectedItem]
                                                              .offerPrice
                                                              .toString(),
                                                          orderCurrency: "INR",
                                                          orderNote: "test"));
                                                }

                                                // if (email == true ||
                                                //     phoneNumber == true) {
                                                //   showAddEmailAlert(context,
                                                //       isEmail:
                                                //           email == true ? true : false,
                                                //       textEditingController:
                                                //           dataTextEditingController,
                                                //       onPressed: () {
                                                //     phoneNumber == true
                                                //         ? ObjectFactory()
                                                //             .appHive
                                                //             .putPhoneNumber(
                                                //                 phoneNumber: int.parse(
                                                //                     dataTextEditingController
                                                //                         .text))
                                                //         : setState(() {});
                                                //     print("Subscription ID of " +
                                                //         selectedItem.toString() +
                                                //         " is " +
                                                //         snapshot
                                                //             .data.data[selectedItem].id
                                                //             .toString());
                                                //     userBloc.orderPlans(
                                                //         orderPlanRequest: OrderPlanRequest(
                                                //             customerEmail: ObjectFactory()
                                                //                         .appHive
                                                //                         .getEmail() ==
                                                //                     null
                                                //                 ? dataTextEditingController
                                                //                     .text
                                                //                 : ObjectFactory()
                                                //                     .appHive
                                                //                     .getEmail(),
                                                //             customerPhone: ObjectFactory()
                                                //                         .appHive
                                                //                         .getPhoneNumber() ==
                                                //                     null
                                                //                 ? dataTextEditingController
                                                //                     .text
                                                //                 : ObjectFactory()
                                                //                     .appHive
                                                //                     .getPhoneNumber()
                                                //                     .toString(),
                                                //             customerName: ObjectFactory()
                                                //                 .appHive
                                                //                 .getName(),
                                                //             orderAmount: snapshot
                                                //                 .data
                                                //                 .data[selectedItem]
                                                //                 .offerPrice
                                                //                 .toString(),
                                                //             orderCurrency: "INR",
                                                //             orderNote: "test"));
                                                //   });
                                                //   print("Add data");
                                                // }
                                                // pop(context);
                                              },
                                            ),
                                          ),
                                        ],
                                      )
                                    : Container(
                                        width:
                                            screenWidth(context, dividedBy: 1),
                                        height:
                                            screenHeight(context, dividedBy: 2),
                                        child: Center(
                                          heightFactor: 1,
                                          widthFactor: 1,
                                          child: SizedBox(
                                            height: 16,
                                            width: 16,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Constants.kitGradients[2]
                                                          .withOpacity(0.4)),
                                              strokeWidth: 2,
                                            ),
                                          ),
                                        ),
                                      );
                              },
                            ),
                          ),

                          // SizedBox(
                          //   height: screenHeight(context, dividedBy: 70),
                          // ),
                        ],
                      ),
                    ),
                  ])),
        ],
      ),
    );
  }
}
