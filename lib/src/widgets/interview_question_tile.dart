import 'package:app_template/src/models/interview_question_answer_response_model.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class InterviewQuestionTile extends StatefulWidget {
  final String count;
  final String question;
  final int index;
  final List<AnswerElement> answer;
  final Function onTap;
  final int countSelected;
  InterviewQuestionTile(
      {this.count,
      this.question,
      this.answer,
      this.onTap,
      this.index,
      this.countSelected});
  @override
  _InterviewQuestionTileState createState() => _InterviewQuestionTileState();
}

class _InterviewQuestionTileState extends State<InterviewQuestionTile> {
  @override
  void initState() {
    print(widget.index.toString());
    print("countSelected" + widget.countSelected.toString());
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap();
      },
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        //height: screenHeight(context, dividedBy: 9),
        //padding:EdgeInsets.symmetric(vertical:13),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(bottom: BorderSide(color: Color(0xffC3C5CB)))),
        child: Column(
          children: [
            SizedBox(
              height: screenWidth(context, dividedBy: 70),
            ),
            Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 70),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 40),
                        ),
                        Container(
                          width: 35,
                          padding: EdgeInsets.all(4),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Constants.kitGradients[2]),
                          child: Text(
                            widget.count,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                fontFamily: "SofiaProRegular"),
                          ),
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 70),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1.5),
                          //height: screenHeight(context, dividedBy: 9),
                          child: Text(
                            widget.question,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontFamily: "SofiaProRegular",
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 70),
                    ),
                  ],
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 13),
                ),
                Icon(Icons.arrow_drop_down_sharp, color: Colors.black),
              ],
            ),
            widget.countSelected == widget.index
                ? SizedBox(
                    height: 2,
                  )
                : Container(),
            widget.index == widget.countSelected
                ? Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 20),
                      ),
                      Expanded(
                        child: Container(
                          color: Colors.white,
                          child: ListView.builder(
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            itemCount: widget.answer.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 60)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      width: screenWidth(context, dividedBy: 1),
                                      // alignment: Alignment.topLeft,
                                      color: Colors.white,
                                      child: Row(
                                        // mainAxisAlignment: MainAxisAlignment.start,
                                        // crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 6,
                                            height: 6,
                                            decoration: BoxDecoration(
                                                color: Color(0xffC3C5CB),
                                                //color: Colors.red,
                                                shape: BoxShape.circle),
                                          ),
                                          SizedBox(
                                            width: screenWidth(context,
                                                dividedBy: 50),
                                          ),
                                          Flexible(
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  right: screenWidth(context,
                                                      dividedBy: 20)),
                                              child: Text(
                                                widget.answer[index].answer,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "SofiaProRegular",
                                                  fontWeight: FontWeight.w400,
                                                ),
                                                textAlign: TextAlign.justify,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 70),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  )
                : Container(),
            SizedBox(
              height: screenWidth(context, dividedBy: 70),
            )
          ],
        ),
      ),
    );
  }
}
