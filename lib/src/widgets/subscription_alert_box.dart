import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/subscription_tab.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class SubscriptionAlertBox extends StatefulWidget {
  // const SubscriptionAlertBox({Key? key}) : super(key: key);

  @override
  _SubscriptionAlertBoxState createState() => _SubscriptionAlertBoxState();
}

class _SubscriptionAlertBoxState extends State<SubscriptionAlertBox> {


  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, BottomBar(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 16),
          vertical: screenHeight(context, dividedBy:13)),
          child: Center(child: contentBox(context))),
    );
  }

  contentBox(context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      // padding: EdgeInsets.symmetric(
      //   horizontal: screenWidth(context, dividedBy: 40),
      // ),
      // margin: EdgeInsets.only(top: 45, bottom: 45),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Constants.kitGradients[0],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SubscriptionTab(),
        ],
      ),
    );
  }
}
