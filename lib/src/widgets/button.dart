import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  CButton({this.onPressed, this.title, this.isLoading});
  @override
  _CButtonState createState() => _CButtonState();
}

class _CButtonState extends State<CButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 14),
        margin: EdgeInsets.all(10),
        child: RaisedButton(
          onPressed: () {
            widget.onPressed();
          },
          color: Constants.kitGradients[2],
          shape: RoundedRectangleBorder(
              //  side: BorderSide(color: Constants.kitGradients[1])
              borderRadius: BorderRadius.circular(10)),
          child: widget.isLoading == true
              ? Center(
                  child: SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                    width: screenHeight(context, dividedBy: 30),
                    child: CircularProgressIndicator(
                      strokeWidth: 2.0,
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          Constants.kitGradients[0]),
                    ),
                  ),
                )
              : Center(
                  child: Text(widget.title,
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontFamily: "SofiaProRegular"))),
        ));
  }
}
