import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class JobAlertCard extends StatefulWidget {
  final String image;
  final String title;
  final String description;
  final String date;
  final String experience;
  final String postedDate;
  final String company;
  final String endDate;
  final Function onApply;
  final bool isImage;
  final bool button;
  JobAlertCard(
      {this.company,
      this.date,
      this.description,
      this.endDate,
      this.experience,
      this.image,
      this.postedDate,
      this.title,
      this.button,
      this.onApply,
      this.isImage});
  @override
  _JobAlertCardState createState() => _JobAlertCardState();
}

class _JobAlertCardState extends State<JobAlertCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      //height: screenHeight(context, dividedBy: 2),
      margin: EdgeInsets.all(10),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Color(0xffC3C5CB),
        ),
      ),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          widget.isImage
              ? Container(
                  width: screenWidth(context, dividedBy: 1),
                  // decoration: BoxDecoration(
                  //   borderRadius: BorderRadius.only(topRight: Radius.circular(8), topLeft: Radius.circular(8)),
                  // ),
                  //height: screenHeight(context, dividedBy: 2),
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          topLeft: Radius.circular(8)),
                      child: widget.image != null
                          ? Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 4),
                              child:
                                  Image.network(widget.image, fit: BoxFit.cover,
                                      loadingBuilder: (BuildContext context,
                                          Widget child,
                                          ImageChunkEvent loadingProgress) {
                                if (loadingProgress == null) return child;
                                return Center(
                                    child: CircularProgressIndicator(
                                        valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                  Constants.kitGradients[28],
                                )));
                              }),
                            )
                          : Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 4),
                              child: Center(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Constants.kitGradients[2]),
                                ),
                              ),
                            )))
              : Container(),
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(horizontal: 15),
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(widget.title,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        fontFamily: "SofiaProRegular",
                        color: Color(0xff000000))),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Text(widget.description,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: "SofiaProRegular",
                        color: Colors.black)),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Text(widget.date,
                    style: TextStyle(
                        fontSize: 11,
                        fontFamily: "SofiaProRegular",
                        color: Color(0xfff888D99))),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Row(
                  children: [
                    SvgPicture.asset("assets/icons/experience.svg"),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    Text("Experience :",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "SofiaProRegular",
                            color: Color(0xfff000000))),
                    Text(widget.experience,
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "SofiaProRegular",
                            color: Color(0xfff000000)))
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 150),
                    ),
                    SvgPicture.asset("assets/icons/postedDate.svg"),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 35),
                    ),
                    Text("Date Posted :",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "SofiaProRegular",
                            color: Color(0xfff000000))),
                    Text(widget.postedDate,
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "SofiaProRegular",
                            color: Color(0xfff000000))),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 150),
                    ),
                    SvgPicture.asset("assets/icons/company.svg"),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    Text("Company :",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "SofiaProRegular",
                            color: Color(0xfff000000))),
                    Container(
                      width: screenWidth(context, dividedBy: 2),
                      child: Text(widget.company,
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "SofiaProRegular",
                              color: Color(0xfff000000))),
                    )
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  children: [
                    SvgPicture.asset("assets/icons/endDate.svg"),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 55),
                    ),
                    Text("End Date :",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "SofiaProRegular",
                            color: Color(0xfff000000))),
                    Text(widget.endDate,
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "SofiaProRegular",
                            color: Color(0xfff000000)))
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Center(
                child: widget.button != false
                    ? RaisedButton(
                        onPressed: () {
                          widget.onApply();
                        },
                        elevation: 0,
                        padding: EdgeInsets.zero,
                        color: Colors.transparent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          width: screenWidth(context, dividedBy: 2),
                          height: screenHeight(context, dividedBy: 18),
                          decoration: BoxDecoration(
                            color: Constants.kitGradients[2],
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                              child: Text("Apply",
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "SofiaProRegular"))),
                        ),
                      )
                    : Container()),
          )
        ],
      ),
    );
  }
}
