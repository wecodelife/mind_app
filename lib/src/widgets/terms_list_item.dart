import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class TermsListItems extends StatefulWidget {
  //const TermsListItems({Key? key}) : super(key: key);
  final String title;
  final String data;
  final int index;
  TermsListItems({this.title, this.data, this.index});

  @override
  _TermsListItemsState createState() => _TermsListItemsState();
}

class _TermsListItemsState extends State<TermsListItems> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 6,
                height: 6,
                decoration: BoxDecoration(
                  color: Constants.kitGradients[7].withOpacity(0.7),
                  shape: BoxShape.circle,
                ),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 40),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.3),
                child: Text(
                  widget.title,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    fontFamily: "Prompt-Light",
                    color: Constants.kitGradients[7].withOpacity(0.5),
                  ),
                ),
              ),
              widget.index == 0
                  ? GestureDetector(
                      onTap: () {
                        pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios_outlined,
                        size: 20,
                        color: Constants.kitGradients[2].withOpacity(0.6),
                      ),
                    )
                  : Container(),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
          Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.14),
                child: Text(
                  widget.data,
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Prompt-Light",
                    color: Constants.kitGradients[7],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
