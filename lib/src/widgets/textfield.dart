import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CTextField extends StatefulWidget {
  final String label;
  final bool isNumber;
  final TextEditingController textEditingController;
  final Color borderColor;
  CTextField({
    this.label,
    this.isNumber,
    this.textEditingController,
    this.borderColor
  });
  @override
  _CTextFieldState createState() => _CTextFieldState();
}

class _CTextFieldState extends State<CTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 26),),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
          Text(
              widget.label,
              style: TextStyle(
                  fontSize: 12,
                  color: widget.borderColor == null ? Color(0xff888D99) : widget.borderColor,
                  fontFamily: "SofiaProRegular")),
          SizedBox(
            height: screenHeight(context, dividedBy: 60),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            child: TextField(
              controller: widget.textEditingController,
              keyboardType:
                  widget.isNumber ? TextInputType.number : TextInputType.text,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Constants.kitGradients[4],
                fontFamily: "Prompt-Light",
              ),
              decoration: InputDecoration(
                // border: new UnderlineInputBorder(
                //     borderSide: new BorderSide(color: Colors.red)),

                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: widget.borderColor ==  null ? Constants.kitGradients[19].withOpacity(0.3): widget.borderColor, width: 1.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: widget.borderColor ==  null ? Constants.kitGradients[7].withOpacity(0.3) : widget.borderColor, width: 1.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
