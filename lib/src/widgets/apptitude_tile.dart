import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class AptitudeTile extends StatefulWidget {
  final String title;
  final Function onPressed;
  AptitudeTile({this.title, this.onPressed});
  @override
  _AptitudeTileState createState() => _AptitudeTileState();
}

class _AptitudeTileState extends State<AptitudeTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child:Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 9),
        margin: EdgeInsets.symmetric(horizontal:15, vertical:5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xffC3C5CB)),
        ),
        child: Row(
          children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 20),
          ),
          Container(
            height: 13,
            width: 13,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Constants.kitGradients[2],
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 25),
          ),
          Text(widget.title,
              style: TextStyle(fontSize: 16, fontFamily: "SofiaProRegular"))
        ]))
    );
  }
}
