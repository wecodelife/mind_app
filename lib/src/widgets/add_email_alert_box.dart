import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/textfield.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class AddEmailAlertBox extends StatefulWidget {
  // const AddEmailAlertBox({Key? key}) : super(key: key);
  final bool isEmail;
  final Function onPressed;
  final TextEditingController textEditingController;
  AddEmailAlertBox({this.onPressed, this.isEmail, this.textEditingController});

  @override
  _AddEmailAlertBoxState createState() => _AddEmailAlertBoxState();
}

class _AddEmailAlertBoxState extends State<AddEmailAlertBox> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Constants.kitGradients[0],
      // title: Text(widget.isEmail == true ? "Your Email ID is missing, Please add it for further payment processes" : "Your Phone Number is missing, Please add it for further payment processes", style: TextStyle(
      //     fontSize: 16,
      //     color:Constants.kitGradients[7],
      //     fontFamily: "SofiaProRegular")),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Text(
              widget.isEmail == true
                  ? "Your Email ID is missing, Please add it for further payment processes"
                  : "Your Phone Number is missing, Please add it for further payment processes",
              style: TextStyle(
                  fontSize: 16,
                  color: Constants.kitGradients[7],
                  fontFamily: "SofiaProRegular")),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: CTextField(
              label: widget.isEmail == true ? "Enter Email" : "Phone Number",
              isNumber: widget.isEmail == true ? false : true,
              textEditingController: widget.textEditingController,
            ),
          ),
        ],
      ),
      // Container(
      //   width: screenWidth(context, dividedBy: 1),
      //   padding: EdgeInsets.symmetric(
      //     horizontal: screenWidth(context, dividedBy: 20),
      //   ),
      //   // margin: EdgeInsets.only(top: 45, bottom: 45),
      //   decoration: BoxDecoration(
      //     shape: BoxShape.rectangle,
      //     color: Constants.kitGradients[0],
      //     borderRadius: BorderRadius.circular(10),
      //   ),
      //   child: Column(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     crossAxisAlignment:CrossAxisAlignment.start,
      //     children: [
      //       SizedBox(
      //         height: screenHeight(context, dividedBy: 35),
      //       ),
      //       Text(widget.isEmail == true ? "Your Email ID is missing, Please add it for further payment processes" : "Your Phone Number is missing, Please add it for further payment processes", style: TextStyle(
      //           fontSize: 16,
      //           color:Constants.kitGradients[7],
      //           fontFamily: "SofiaProRegular")),
      //       SizedBox(
      //         height: screenHeight(context, dividedBy: 40),
      //       ),
      //       CTextField(
      //           label: widget.isEmail == true ? "Enter Email" : "Phone Number",
      //           isNumber: widget.isEmail == true ? false : true,
      //         textEditingController: widget.textEditingController,
      //       ),
      //       SizedBox(
      //         height: screenHeight(context, dividedBy: 30),
      //       ),
      //       Container(
      //         alignment: Alignment.center,
      //         padding: EdgeInsets.symmetric(horizontal: 15),
      //         width: screenWidth(context, dividedBy: 1),
      //         height: screenHeight(context, dividedBy: 10),
      //         child: CButton(
      //           title: "Add",
      //           onPressed: widget.onPressed,
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
      actions: [
        // SizedBox(
        //   height: screenHeight(context, dividedBy: 40),
        // ),
        CButton(
          title: "Add",
          onPressed: widget.onPressed,
        ),
        // Container(
        //   alignment: Alignment.center,
        //   padding: EdgeInsets.symmetric(horizontal: 15),
        //   width: screenWidth(context, dividedBy: 1),
        //   height: screenHeight(context, dividedBy: 10),
        //   child: CButton(
        //     title: "Add",
        //     onPressed: widget.onPressed,
        //   ),
        // ),
      ],
    );
  }
}
// onPressed: () {
// if (widget.textEditingController.text == " ") {
// showToast("Please fill the field");
// } else {
// widget.onPressed();
// }
// },