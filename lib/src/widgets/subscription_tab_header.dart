import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class SubscriptionTabHeader extends StatefulWidget {
  // const SubscriptionTabHeader({Key? key}) : super(key: key);
  final String label;
  final int indexValue;
  final int selectedItem;
  final Function onSelected;
  SubscriptionTabHeader({this.label, this.indexValue, this.onSelected, this.selectedItem});

  @override
  _SubscriptionTabHeaderState createState() => _SubscriptionTabHeaderState();
}

class _SubscriptionTabHeaderState extends State<SubscriptionTabHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            GestureDetector(
              onTap:widget.onSelected,
              child:Container(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 50),
                    vertical: screenHeight(context,
                        dividedBy: widget.selectedItem == widget.indexValue ? 35 : 40)),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: widget.selectedItem == widget.indexValue
                      ? Constants.kitGradients[2]
                      : Constants.kitGradients[0],
                  borderRadius: widget.selectedItem == widget.indexValue
                      ? BorderRadius.circular(10)
                      : BorderRadius.circular(10),
                  border: widget.selectedItem == widget.indexValue
                      ? Border.all(color: Colors.transparent)
                      : Border.all(
                      color: Constants.kitGradients[2].withOpacity(0.13)),
                ),
                child: Text(
                  "RS "+ widget.label+"/year",
                  style: TextStyle(
                      fontFamily: "SofiaProRegular",
                      fontSize: 12,
                      color: widget.selectedItem == widget.indexValue
                          ? Colors.white
                          : Constants.kitGradients[2],
                      fontWeight: FontWeight.w600),
                ),
              )
            ),
            // GestureDetector(
            //   onTap: widget.onSelected,
            //   child: Container(
            //     child: Row(
            //       children: [
            //         Container(
            //           width: screenWidth(context, dividedBy: 15),
            //           height: screenWidth(context, dividedBy: 15),
            //           alignment: Alignment.center,
            //           decoration: BoxDecoration(
            //             shape: BoxShape.circle,
            //             gradient: LinearGradient(
            //                 begin: Alignment.topLeft,
            //                 end: Alignment.bottomRight,
            //                 colors: [
            //                   Colors.grey.shade300,
            //                   Constants.kitGradients[31],
            //                   Constants.kitGradients[31],
            //                   Colors.grey.shade300,
            //                 ],
            //                 stops: [
            //                   0.2,
            //                   .4,
            //                   .6,
            //                   1.0,
            //                 ]),
            //             boxShadow: [
            //               BoxShadow(
            //                 blurRadius: 6,
            //                 spreadRadius: 3,
            //                 offset: Offset(-4, -2),
            //                 color: Color.fromRGBO(255, 255, 255, 0.9),
            //               ),
            //               BoxShadow(
            //                 blurRadius: 7,
            //                 spreadRadius: 3,
            //                 offset: Offset(6, 2),
            //                 color: Color.fromRGBO(0, 0, 0, 0.1),
            //               ),
            //             ],
            //           ),
            //           child: widget.selectedItem == widget.indexValue
            //               ? Container(
            //             width: 15,
            //             height: 15,
            //             decoration: BoxDecoration(
            //               // borderRadius: BorderRadius.circular(6),
            //               // color: Constants.kitGradients[31],
            //               // color: Colors.grey.shade300,
            //               // border: Border.all(color: Colors.grey.shade200),
            //               shape: BoxShape.circle,
            //               gradient: LinearGradient(
            //                   begin: Alignment.topLeft,
            //                   end: Alignment.bottomRight,
            //                   colors: [
            //                     Colors.grey.shade300,
            //                     Constants.kitGradients[31],
            //                     Constants.kitGradients[31],
            //                     Colors.grey.shade400,
            //                   ],
            //                   stops: [
            //                     0.2,
            //                     .4,
            //                     .5,
            //                     .8,
            //                   ]),
            //               boxShadow: [
            //                 BoxShadow(
            //                   blurRadius: 6,
            //                   spreadRadius: 3,
            //                   offset: Offset(-4, -2),
            //                   color: Color.fromRGBO(255, 255, 255, 0.9),
            //                 ),
            //                 BoxShadow(
            //                   blurRadius: 7,
            //                   spreadRadius: 3,
            //                   offset: Offset(6, 2),
            //                   color: Color.fromRGBO(0, 0, 0, 0.1),
            //                 ),
            //               ],
            //             ),
            //           )
            //               : Container(),
            //         ),
            //         SizedBox(
            //           width: screenWidth(context, dividedBy: 30),
            //         ),
            //         Text(
            //           widget.label,
            //           style: TextStyle(
            //             fontSize: 17,
            //             fontWeight: FontWeight.w700,
            //             color: Constants.kitGradients[19],
            //             fontFamily: "KleeOne-SemiBold",
            //           ),
            //         ),
            //         SizedBox(
            //           width: screenWidth(context, dividedBy: 30),
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
          ],
        )
    );
  }
}
