import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class JobTile extends StatefulWidget {
  final String data;
  final Function onPressed;
  JobTile({this.data, this.onPressed});
  @override
  _JobTileState createState() => _JobTileState();
}

class _JobTileState extends State<JobTile> {
  bool onSelect = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          setState(() {
            onSelect = !onSelect;
          });
          widget.onPressed();
        },
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 12),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: onSelect ? Constants.kitGradients[2] : Colors.white,
              border: Border(bottom: BorderSide(color: Color(0xffC3C5CB)))),
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            widget.data,
            style: TextStyle(
                color: onSelect ? Colors.white : Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w600,
                fontFamily: "SofiaProRegular"),
          ),
        ));
  }
}
