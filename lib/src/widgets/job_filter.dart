import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/filter_chips.dart';
import 'package:flutter/material.dart';

class JobFilter extends StatefulWidget {
  final String filter;
  final List value;
  final ValueChanged valueChanged;
  JobFilter({
    this.valueChanged,
    this.filter,
    this.value,
  });
  @override
  _JobFilterState createState() => _JobFilterState();
}

class _JobFilterState extends State<JobFilter> {
  Color changeColor = Colors.white;
  int currentIndex = 0;
  bool selected = false;
  //List<String> types = ["Internship", 'Jobs'];
  @override
  Widget build(BuildContext context) {
    return Container(
      //width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 16),
      // padding: EdgeInsets.symmetric(horizontal: 15),
      //color: Colors.amber,
      child: Row(
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 15),
          ),
          Container(
            width: screenWidth(context, dividedBy: 4.2),
            child: Text(widget.filter + ":",
                style: TextStyle(
                    fontSize: screenWidth(context, dividedBy: 25),
                    fontFamily: "SofiaProRegular")),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 100),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1.5),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: (widget.value).length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                    child: FilterChips(
                  values: widget.value[index],
                  onPressed: () {
                    widget.valueChanged(index);
                  },
                ));
              },
            ),
          ),
        ],
      ),
    );
  }
}

// FilterChip(
// //  avatar:Container(),
// label: Text(widget.value1),
// labelStyle: TextStyle(fontSize:12, fontFamily:"SofiaProRegular"),
// selectedColor: Constants.kitGradients[2],
// selected: widget.isSelected,
// onSelected: (bool selected) {
// setState(() {
// widget.isSelected = ! widget.isSelected;
// });
// },
// ),

// child: FilterChip(
//   label: Text(widget.value[index]),
//   backgroundColor: Constants.kitGradients[2],
//   selected: selected,
//   onSelected: (bool selected) {
//     setState(() {});
//   },
// ),
