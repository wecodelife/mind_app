import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class AlertBoxListTile extends StatefulWidget {
  // const AlertBoxListTile({Key? key}) : super(key: key);
  final Widget icon;
  final List<String> data;
  AlertBoxListTile({this.icon, this.data});

  @override
  _AlertBoxListTileState createState() => _AlertBoxListTileState();
}

class _AlertBoxListTileState extends State<AlertBoxListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context,
          dividedBy: 1),
      child: ListView.builder(
          itemCount: widget
              .data.length,
          shrinkWrap: true,
          physics:
          NeverScrollableScrollPhysics(),
        itemBuilder:(BuildContext context, int index) {
          return Container(
              width: screenWidth(context,
                  dividedBy: 1),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                widget.icon == null ? Container() : widget.icon,
                SizedBox(
                  width: screenWidth(context, dividedBy: 30),
                ),
                Expanded(
                  child: Text(widget.data[index],
                    style: TextStyle(
                        fontFamily: "SofiaProRegular",
                        fontSize: 15,
                        fontWeight: FontWeight.w500),),
                ),
              ],
            )
          );
        }
      ),
    );
  }
}
