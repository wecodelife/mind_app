import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';

class CBottomNavigationBar extends StatefulWidget {
  final String label;
  //final Color backgroundColor;
  final IconData icon;
  // final Function(int) onChange;
   final int index;
  CBottomNavigationBar({this.label, this.icon,this.index });
  @override
  _CBottomNavigationBarState createState() => _CBottomNavigationBarState();
}

class _CBottomNavigationBarState extends State<CBottomNavigationBar> {
int activeIndex = 2 ;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: (){
      //   setState(() {
      //     activeIndex = widget.index ;
      //   });
      // },
      child: Container(
        margin: EdgeInsets.symmetric(vertical:3),
          height: screenHeight(context, dividedBy :10),
          width:screenWidth(context, dividedBy:5.5),
          decoration: BoxDecoration(
            color: widget.index == activeIndex ? Colors.blue : Colors.white,
            borderRadius: widget.index == activeIndex ? BorderRadius.circular(25) : BorderRadius.circular(0),
          ),

          // color:Colors.red,
          child:Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children:<Widget>[
                Icon(widget.icon, color: widget.index == activeIndex ?  Colors.white : Colors.grey),
                Text(widget.label, style:TextStyle(fontSize: 14,fontFamily: "SofiaProRegular",color: widget.index == activeIndex ?  Colors.white : Color(0xff888D99)),)

              ]
          )
      ),
    );
  }
}

