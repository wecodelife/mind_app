import 'package:app_template/src/models/get_questions.dart';
import 'package:app_template/src/screens/test_complete.dart';
import 'package:app_template/src/screens/test_page.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/button.dart';
import 'package:app_template/src/widgets/right_answer_bottomsheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RadioButtonTile extends StatefulWidget {
  final Option option1;
  final Option option2;
  final Option option3;
  final bool isRightAnswer1;
  final bool isRightAnswer2;
  final bool isRightAnswer3;
  final ValueChanged onChanged;
  final String answer;
  final int count;
  final int id;
  final String answerExplanation1;
  final String answerExplanation2;
  final String answerExplanation3;
  RadioButtonTile(
      {this.option1,
      this.option2,
      this.option3,
      this.isRightAnswer1,
      this.isRightAnswer2,
      this.onChanged,
      this.answer,
      this.isRightAnswer3,
      this.count,
      this.answerExplanation1,
      this.answerExplanation2,
      this.answerExplanation3,
      this.id});
  @override
  _RadioButtonTileState createState() => _RadioButtonTileState();
}

class _RadioButtonTileState extends State<RadioButtonTile> {
  // Declare this variable
  int selectedRadio;
  bool correctionCheck;
  // bool rightAnswer = false;
  bool clicked = false;
  String answerExplanation = "";

  @override
  void initState() {
    super.initState();
    selectedRadio = 0;
  }

// Changes the selected value on 'onChanged' click on each radio button
  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              //crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Radio(
                  value: 1,
                  groupValue: selectedRadio,
                  activeColor: Colors.blue,
                  onChanged: (val) {
                    if (clicked == false) {
                      clicked = true;
                      print("Radio $val");
                      setSelectedRadio(val);
                      correctionCheck = widget.isRightAnswer1;
                      answerExplanation = widget.answerExplanation1;
                      widget.onChanged(widget.option1.id);
                      // rightAnswer = false;
                    }
                  },
                ),
                GestureDetector(
                  onTap:(){
                    setState((){
                      selectedRadio = 1;
                      clicked = true;
                      correctionCheck = widget.isRightAnswer1;
                      answerExplanation = widget.answerExplanation1;
                      widget.onChanged(widget.option1.id);
                    });
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.2),
                    child: Text(
                      widget.option1.option,
                      style: TextStyle(
                          color: Color(0xff2C2E39),
                          fontSize: 15,
                          fontFamily: "SofiaProRegular",
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Radio(
                  value: 2,
                  groupValue: selectedRadio,
                  activeColor: Colors.blue,
                  onChanged: (val) {
                    print("Radio $val");
                    if (clicked == false) {
                      clicked = true;
                      setSelectedRadio(val);
                      correctionCheck = widget.isRightAnswer2;
                      answerExplanation = widget.answerExplanation2;
                      widget.onChanged(widget.option2.id);
                      // wrongAnswer = false;
                    }
                  },
                ),
                GestureDetector(
                  onTap:(){
                    setState((){
                      selectedRadio = 2;
                      clicked = true;
                      correctionCheck = widget.isRightAnswer2;
                      answerExplanation = widget.answerExplanation2;
                      widget.onChanged(widget.option2.id);
                    });
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.2),
                    child: Text(
                      widget.option2.option,
                      style: TextStyle(
                          color: Color(0xff2C2E39),
                          fontSize: 15,
                          fontFamily: "SofiaProRegular",
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Radio(
                  value: 3,
                  groupValue: selectedRadio,
                  activeColor: Colors.blue,
                  onChanged: (val) {
                    print("Radio $val");
                    if (clicked == false) {
                      clicked = true;
                      setSelectedRadio(val);
                      correctionCheck = widget.isRightAnswer3;
                      answerExplanation = widget.answerExplanation3;
                      widget.onChanged(widget.option3.id);
                      // rightAnswer = false;}
                    }
                  },
                ),
                GestureDetector(
                  onTap:(){
                    setState((){
                      selectedRadio = 3;
                      clicked = true;
                      correctionCheck = widget.isRightAnswer3;
                      answerExplanation = widget.answerExplanation3;
                      widget.onChanged(widget.option3.id);
                    });
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.2),
                    child: Text(
                      widget.option3.option,
                      style: TextStyle(
                          color: Color(0xff2C2E39),
                          fontSize: 15,
                          fontFamily: "SofiaProRegular",
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: correctionCheck == false
                  ? screenHeight(context, dividedBy: 10)
                  : screenHeight(context, dividedBy: 5),
            ),
            correctionCheck == false
                ? Container(
                    width: screenWidth(context, dividedBy: 1),
                    //color:Colors.blue,
                    child: Column(
                      children: [
                        AlertButton(
                          message: "View right Answer",
                          onPressed: () {
                            showModalBottomSheet(
                                backgroundColor: Colors.transparent,
                                context: context,
                                builder: (context) => SingleChildScrollView(
                                      child: RightAnswerBottomSheet(
                                          number: widget.answer,
                                          explanationOne: answerExplanation),
                                    ));
                          },
                          boxColor: Colors.white,
                          icon: "",
                        ),
                        AlertButton(
                          message: "Sorry, Better luck next time",
                          onPressed: () {},
                          icon: "assets/icons/button_wrong.svg",
                          boxColor: Color(0xffF19797),
                        ),
                      ],
                    ),
                  )
                : correctionCheck == true
                    ? AlertButton(
                        message: "Right Answer, well done !",
                        onPressed: () {},
                        boxColor: Color(0xff4CC590),
                        icon: "assets/icons/button_tick.svg",
                      )
                    : Container(
                        height: screenHeight(context, dividedBy: 10),
                        //color:Colors.amber,
                      ),
            Center(
              child: CButton(
                  title: "Next Question",
                  onPressed: () {
                    if (clicked == true) {
                      if (widget.count < 10) {
                        print("Count Page" + widget.count.toString());
                        push(
                            context,
                            TestPage(
                              id: widget.id,
                              firstTime: false,
                            ));
                      } else {
                        pushAndReplacement(context, TestCompletePage());
                      }
                    } else {
                      showToast("Please update answer");
                    }
                  }),
            )
          ],
        ));
  }
}

class AlertButton extends StatefulWidget {
  final String icon;
  final String message;
  final Function onPressed;
  final Color boxColor;
  AlertButton({this.icon, this.message, this.onPressed, this.boxColor});
  @override
  _AlertButtonState createState() => _AlertButtonState();
}

class _AlertButtonState extends State<AlertButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 13),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(),
      child: Center(
        child: RaisedButton(
          onPressed: widget.onPressed,
          color: widget.boxColor,
          elevation: 0,
          shape: RoundedRectangleBorder(
              // border: widget.boxColor == Colors.white ? Border.all(color: Color(0xffC3C5CB)) : Border.all(),
              side: widget.boxColor == Colors.white
                  ? BorderSide(color: Color(0xffC3C5CB))
                  : BorderSide(color: Colors.transparent),
              borderRadius: BorderRadius.circular(10)),
          child: Center(
            child: Row(
              mainAxisAlignment: widget.boxColor == Colors.white
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.start,
              children: [
                Container(
                  child: SvgPicture.asset(widget.icon),
                ),
                widget.boxColor != Colors.white
                    ? SizedBox(
                        width: screenWidth(context, dividedBy: 50),
                      )
                    : SizedBox(),
                Text(
                  widget.message,
                  style: widget.boxColor == Color(0xff4CC590)
                      ? TextStyle(
                          fontSize: 15,
                          color: Color(0xff003904),
                          fontWeight: FontWeight.w400,
                          fontFamily: "SofiaProRegular")
                      : widget.boxColor == Color(0xffF19797)
                          ? TextStyle(
                              fontSize: 15,
                              color: Color(0xff980101),
                              fontWeight: FontWeight.w400,
                              fontFamily: "SofiaProRegular")
                          : TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SofiaProRegular"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
