import 'package:awesome_loader/awesome_loader.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:shimmer/shimmer.dart';

class CustomImageSlider extends StatefulWidget {
  final List<String> sliderImages;
  CustomImageSlider({this.sliderImages});
  @override
  _CustomImageSliderState createState() => _CustomImageSliderState();
}

class _CustomImageSliderState extends State<CustomImageSlider> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(),
      child:
      // widget.sliderImages == null
      //     ? Shimmer.fromColors(
      //         baseColor: Constants.kitGradients[0],
      //         highlightColor: Colors.grey[400],
      //         child: Container(
      //           width: screenWidth(context, dividedBy: 1),
      //           height: screenHeight(context, dividedBy: 3.2),
      //           decoration: BoxDecoration(
      //             // color: Colors.red,
      //             borderRadius: BorderRadius.only(
      //               topRight: Radius.circular(30),
      //               topLeft: Radius.circular(30),
      //             ),
      //           ),
      //           // child: Center(
      //             heightFactor: 1,
      //             widthFactor: 1,
      //           //   child: SizedBox(
      //           //     height: 16,
      //           //     width: 16,
      //           //     child: CircularProgressIndicator(
      //           //       valueColor:
      //           //           AlwaysStoppedAnimation(Colors.grey.withOpacity(0.4)),
      //           //       strokeWidth: 2,
      //           //     ),
      //           //   ),
      //           // ),
      //         ),
      //       )
      //     :
      Carousel(
              boxFit: BoxFit.fill,
              animationDuration: Duration(milliseconds: 800),
              animationCurve: Curves.easeInOut,
              images: widget.sliderImages
                  .map(
                    (item) => CachedNetworkImage(
                      width: screenWidth(context, dividedBy: 1),
                      // height: screenHeight(context, dividedBy: 3.2),
                      fit: BoxFit.fill,
                      imageUrl: item,
                      imageBuilder: (context, imageProvider) => Container(
                        width: screenWidth(context, dividedBy: 1),
                        // height: screenHeight(context, dividedBy: 3.2),
                        decoration: BoxDecoration(
                          // borderRadius: BorderRadius.circular(30),
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30),
                            topLeft: Radius.circular(30),
                          ),
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 3.2),
                        decoration: BoxDecoration(
                          // color: Colors.red,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30),
                            topLeft: Radius.circular(30),
                          ),
                        ),
                        child: Center(
                          heightFactor: 1,
                          widthFactor: 1,
                          child: SizedBox(
                            height: 16,
                            width: 16,
                            child:  AwesomeLoader(
                              loaderType: AwesomeLoader.AwesomeLoader3,
                              color: Constants.kitGradients[2].withOpacity(0.3),
                            ),
                            // CircularProgressIndicator(
                            //   valueColor: AlwaysStoppedAnimation(
                            //       Constants.kitGradients[2].withOpacity(0.4)),
                            //   strokeWidth: 2,
                            // ),
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
              showIndicator: true,
              dotSize: 7.0,
              autoplay: true,
              dotIncreaseSize: 1.2,
              dotSpacing: 20.0,
              dotColor: Colors.white.withOpacity(0.3),
              dotBgColor: Colors.transparent,
              dotIncreasedColor: Colors.white,
              //moveIndicatorFromBottom: 8.0,
              dotVerticalPadding: screenHeight(context, dividedBy: 120),
              indicatorBgPadding: 3.0,
              autoplayDuration: Duration(seconds: 3),
            ),

      // CarouselSlider(
      //     items: widget.sliderImages
      //         .map((item) => CachedNetworkImage(
      //               width: screenWidth(context, dividedBy: 1),
      //               // height: screenHeight(context, dividedBy: 3),
      //               fit: BoxFit.fitWidth,
      //               imageUrl: item,
      //               imageBuilder: (context, imageProvider) => Container(
      //                 width: screenWidth(context, dividedBy: 1),
      //                 // height: screenHeight(context, dividedBy: 3),
      //                 margin: EdgeInsets.symmetric(
      //                   // vertical: screenWidth(context, dividedBy: 80),
      //                   horizontal: screenWidth(context, dividedBy: 60),
      //                 ),
      //                 decoration: BoxDecoration(
      //                   borderRadius: BorderRadius.circular(15),
      //                   // boxShadow: [
      //                   //   BoxShadow(
      //                   //     blurRadius: 6,
      //                   //     spreadRadius: 3,
      //                   //     offset: Offset(4, 2),
      //                   //     color: Colors.grey.withOpacity(0.4),
      //                   //   ),
      //                   //   BoxShadow(
      //                   //     blurRadius: 6,
      //                   //     spreadRadius: 3,
      //                   //     offset: Offset(-4, -2),
      //                   //     color: Colors.white.withOpacity(0.4),
      //                   //   ),
      //                   // ],
      //                   image: DecorationImage(
      //                     image: imageProvider,
      //                     fit: BoxFit.fitWidth,
      //                   ),
      //                 ),
      //               ),
      //               placeholder: (context, url) => Center(
      //                 heightFactor: 1,
      //                 widthFactor: 1,
      //                 child: SizedBox(
      //                   height: 16,
      //                   width: 16,
      //                   child: CircularProgressIndicator(
      //                     valueColor: AlwaysStoppedAnimation(
      //                         Colors.blue.withOpacity(0.4)),
      //                     strokeWidth: 2,
      //                   ),
      //                 ),
      //               ),
      //             ))
      //         .toList(),
      //     options: CarouselOptions(
      //       height: 400,
      //       aspectRatio: 16 / 9,
      //       viewportFraction: 0.9,
      //       initialPage: 0,
      //       enableInfiniteScroll: true,
      //       reverse: false,
      //       autoPlay: true,
      //       autoPlayInterval: Duration(seconds: 3),
      //       autoPlayAnimationDuration: Duration(milliseconds: 800),
      //       autoPlayCurve: Curves.fastOutSlowIn,
      //       enlargeCenterPage: false,
      //       // onPageChanged: (){},
      //       scrollDirection: Axis.horizontal,
      //     ))
    );
  }
}
