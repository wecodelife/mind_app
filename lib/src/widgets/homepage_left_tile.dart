import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';

class LeftTile extends StatefulWidget {
  final String title;
  final Color boxColor;
  final String image;
  final Function onTap;

  LeftTile({this.title, this.boxColor, this.image, this.onTap});
  @override
  _LeftTileState createState() => _LeftTileState();
}

class _LeftTileState extends State<LeftTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){
        widget.onTap();
      },
      child: Container(
        padding: EdgeInsets.all(10),
        // margin: EdgeInsets.symmetric(vertical: 5),
        width: screenWidth(context, dividedBy: 2.2),
        height: screenHeight(context, dividedBy: 3),
        decoration: BoxDecoration(
            color: widget.boxColor, borderRadius: BorderRadius.circular(8)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 90),
            ),
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                child: Image(
                  width: screenWidth(context, dividedBy: 2.5),
                  height: screenHeight(context, dividedBy: 5),
                  image: AssetImage(widget.image),
                  fit: BoxFit.contain,
                ),
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Text(widget.title,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400,fontFamily: "SofiaProRegular", color:Color(0xff000000)))
          ],
        ),
      ),
    );
  }
}
