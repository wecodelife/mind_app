import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeTile extends StatefulWidget {
  final String title;
  final Color boxColor;
  final String image;
  final Function onTap;
  final double boxHeight;
  HomeTile({this.title, this.boxColor, this.image, this.onTap, this.boxHeight});
  @override
  _HomeTileState createState() => _HomeTileState();
}

class _HomeTileState extends State<HomeTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical:screenHeight(context, dividedBy: 60), horizontal: screenWidth(context, dividedBy: 40)),
        margin: EdgeInsets.symmetric(vertical: 5),
        width: screenWidth(context, dividedBy: 3.5),
        height: screenHeight(context, dividedBy: widget.boxHeight),
        decoration: BoxDecoration(
            color: widget.boxColor, borderRadius: BorderRadius.circular(10)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Center(
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: SvgPicture.asset(widget.image,
                      fit: BoxFit.contain,
                      width: screenWidth(context, dividedBy: 6),
                      height: screenHeight(context, dividedBy: 12))),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Text(widget.title,
                style: TextStyle(
                    fontSize: screenWidth(context, dividedBy: 30),
                    fontWeight: FontWeight.w400,
                    fontFamily: "SofiaProRegular",
                    color: Color(0xff000000))),
          ],
        ),
      ),
    );
  }
}
