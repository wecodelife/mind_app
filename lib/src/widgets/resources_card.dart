import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ResourceCard extends StatefulWidget {
  final String title;
  final Function onSelected;
  final Color boxColor;
  final String bgImage;
  final int index;
  final int countSelected;
  ResourceCard(
      {this.title,
      this.onSelected,
      this.countSelected,
      this.boxColor,
      this.index,
      this.bgImage});
  @override
  _ResourceCardState createState() => _ResourceCardState();
}

class _ResourceCardState extends State<ResourceCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          widget.onSelected();
        },
        child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 9),
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(widget.bgImage),
                fit: BoxFit.cover,
              ),
              color: widget.countSelected != widget.index
                  ? widget.boxColor
                  : Color(0xff172A88),
              borderRadius: BorderRadius.circular(10),
              //border: Border.all(color: Color(0xffA8A8A8)),
            ),
            child: Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 15),
                ),
                Text(
                  widget.title,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontSize: 24,
                      fontFamily: "SofiaProRegular"),
                ),
              ],
            )));
  }
}
