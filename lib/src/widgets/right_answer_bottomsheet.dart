import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class RightAnswerBottomSheet extends StatefulWidget {
  final String number;
  final String explanationOne;
  final String explanationTwo;
  RightAnswerBottomSheet(
      {this.number, this.explanationOne, this.explanationTwo});
  @override
  _RightAnswerBottomSheetState createState() => _RightAnswerBottomSheetState();
}

class _RightAnswerBottomSheetState extends State<RightAnswerBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      //height: screenHeight(context, dividedBy:1.6),
      padding: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10))),
      child: Column(
        //shrinkWrap: true,
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 1),
            //height: screenHeight(context, dividedBy:15),
            child: Row(
              children: [
                Text(
                  "Right Answer : " + widget.number,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      fontFamily: "SofiaProRegular"),
                ),
                Spacer(),
                IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    pop(context);
                  },
                  color: Colors.black,
                )
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 45),
          ),
          SizedBox(
              width: screenWidth(context, dividedBy: 1),
              child: Text(
                widget.explanationOne,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontFamily: "SofiaProRegular"),
              )),
          // SizedBox(height: screenHeight(context, dividedBy:45),),
          // Container(
          //   alignment: Alignment.centerLeft,
          //   child:Image(
          //     image:AssetImage("assets/images/eqa.png"),
          //   )
          // ),
          // SizedBox(height: screenHeight(context, dividedBy:45),),
          // SizedBox(
          //     width:screenWidth(context, dividedBy:1),
          //     child:Text(widget.explanationTwo,style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, fontFamily: "SofiaProRegular"),)
          // ),
          SizedBox(
            height: screenHeight(context, dividedBy: 25),
          ),
        ],
      ),
    );
  }
}
