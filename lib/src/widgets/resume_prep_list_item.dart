import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';
import 'package:flick_video_player/flick_video_player.dart';

class ResumePreperationListItem extends StatefulWidget {
  const ResumePreperationListItem(
      {Key key,
      this.looping,
      this.description,
      this.vdoUrl,
      this.onTap,
      this.videoIndex = 2})
      : super(key: key);
  final bool looping;
  final String vdoUrl;
  final String description;
  final Function onTap;
  final int videoIndex;

  @override
  _ResumePreperationListItemState createState() =>
      _ResumePreperationListItemState();
}

class _ResumePreperationListItemState extends State<ResumePreperationListItem> {
  ChewieController _chewieController;
  Future<void> _initializeVideoPlayerFuture;
  bool isPlaying = false;
  bool isComplete = false;
  bool showControl = false;
  VideoPlayerController videoPlayerController;
  Widget networkPlayerWidget;
  FlickManager flickManager;
  bool alert = false;

  showSubscription(){
    showSubscriptionAlertDialog(context);
    setState((){
      alert = true;
    });
  }

  @override
  void initState() {
    alert == true ? _chewieController.pause() : print("Playing Videos");
    flickManager = FlickManager(
      autoPlay: false,
      videoPlayerController: VideoPlayerController.network(widget.vdoUrl),
    );

    // TODO: implement initState
    // _initializeVideoPlayerFuture = widget.videoPlayerController.initialize();
    videoPlayerController = VideoPlayerController.network(widget.vdoUrl);
    // ..initialize().then((_) {
    //   // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
    //   setState(() {});
    // });

    _chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 16 / 9,
        autoInitialize: true,
        showControlsOnInitialize: false,
        autoPlay: false,
        showControls: true,
        // allowPlaybackSpeedChanging: false,
        errorBuilder: (context, errorMessage) {
          return Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 3.4),
            child: Center(
                child: GestureDetector(
              onTap: () {
                setState(() {
                  print("Try again");
                  _chewieController.seekTo(Duration(seconds: 0));
                  _chewieController.play();
                });
              },
              child: Text("Try again",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontFamily: 'SofiaProSemiBold')),
            )),
          );
        });

    // BetterPlayerDataSource betterPlayerDataSource = BetterPlayerDataSource(
    //     BetterPlayerDataSourceType.network,widget.vdoUrl,);
    //     // "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
    // _betterPlayerController = BetterPlayerController(
    //     BetterPlayerConfiguration(
    //       aspectRatio: 16 / 9, // default aspect ratio
    //       fullScreenAspectRatio: 16 / 9, // fullscreen mode aspect ratio
    //       autoPlay: false,
    //       looping: widget.looping,
    //       autoDispose: false,
    //       placeholder: Center(child: CircularProgressIndicator()),
    //       allowedScreenSleep: false,
    //       controlsConfiguration: BetterPlayerControlsConfiguration(
    //         textColor: Colors.white,
    //         iconsColor: Colors.white,
    //         // skipForwardIcon: Icons.forward_10,
    //         // skipBackIcon: Icons.replay_10,
    //         forwardSkipTimeInMilliseconds: 10000,
    //         controlBarColor: Colors.transparent,
    //         showControlsOnInitialize: true,
    //
    //       ),
    //     ),
    //     betterPlayerDataSource: betterPlayerDataSource);

    // networkPlayerWidget = AspectRatio(
    //   aspectRatio: 16 / 9, // default aspect ratio
    //   child: BetterPlayer.network(
    //     widget.vdoUrl,
    //     // 'https://assets.mixkit.co/videos/preview/mixkit-forest-stream-in-the-sunlight-529-large.mp4',  // video url
    //     betterPlayerConfiguration: BetterPlayerConfiguration(
    //       aspectRatio: 16 / 9, // default aspect ratio
    //       fullScreenAspectRatio: 16 / 9, // fullscreen mode aspect ratio
    //       autoPlay: false,
    //       looping: widget.looping,
    //       placeholder: Center(child: CircularProgressIndicator()),
    //       allowedScreenSleep: false,
    //       controlsConfiguration: BetterPlayerControlsConfiguration(
    //         textColor: Colors.white,
    //         iconsColor: Colors.white,
    //         skipForwardIcon: Icons.forward_10,
    //         skipBackIcon: Icons.replay_10,
    //         forwardSkipTimeInMilliseconds: 10000,
    //         // controlBarColor: Colors.transparent,
    //         showControlsOnInitialize: false,
    //       ),
    //     ),
    //   ),
    // );
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoPlayerController.dispose();
    _chewieController.dispose();
    // _chewieController.pause();
    // _betterPlayerController.dispose();
    flickManager.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      padding: EdgeInsets.symmetric(
          // horizontal: screenWidth(context, dividedBy: 70),
          ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        // border: Border.all(color: Constants.kitGradients[19])
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Stack(
              children: <Widget>[
                // AspectRatio(
                //   aspectRatio: 16 / 9,
                //   child: Container(
                //     width: screenWidth(context, dividedBy: 1),
                //     height: screenHeight(context, dividedBy: 3.4),
                //     decoration: BoxDecoration(
                //       color: Colors.white,
                //     ),
                //     child: FlickVideoPlayer(flickManager: flickManager),
                //   ),
                // ),
                //

                // _betterPlayerController != null ? AspectRatio(
                //   aspectRatio: 16 / 9,
                //   child: Container(
                //     width: screenWidth(context, dividedBy: 1),
                //       height: screenHeight(context, dividedBy: 3.4),
                //       decoration: BoxDecoration(
                //           color: Colors.white,
                //       ),
                //     child: BetterPlayer(
                //       controller: _betterPlayerController,
                //     ),
                //   ),
                // ): Container(),

                _chewieController != null
                    ? AspectRatio(
                        aspectRatio: 16 / 9,
                        child: Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 3.4),
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          padding: EdgeInsets.symmetric(
                              // horizontal: screenWidth(context, dividedBy: 70),
                              ),
                          child:

                              // networkPlayerWidget,
                              Theme(
                                  data: Theme.of(context).copyWith(
                                    dialogBackgroundColor: Constants
                                        .kitGradients[0]
                                        .withOpacity(0.35),
                                    // Colors.transparent,
                                    accentColor: _chewieController.isPlaying
                                        ? Constants.kitGradients[0]
                                        : Constants.kitGradients[2],
                                    iconTheme: IconThemeData(
                                      color: Constants.kitGradients[2],
                                    ),
                                  ),
                                  child: Chewie(controller: _chewieController)),
                        ),
                      )
                    : Container(),

                // isPlaying == true
                //     ? GestureDetector(
                //         onTap: () {
                //           _chewieController.isPlaying
                //               ? setState(() {
                //                   _chewieController.pause();
                //                   isPlaying = false;
                //                 })
                //               : isComplete == true
                //                   ? setState(() {
                //                       widget.videoPlayerController.seekTo(
                //                           Duration(
                //                               seconds: 0,
                //                               minutes: 0,
                //                               hours: 0));
                //                       _chewieController.play();
                //                       isPlaying = true;
                //                     })
                //                   : setState(() {
                //                       _chewieController.play();
                //                       isPlaying = true;
                //                     });
                //         },
                //         child: Container(
                //           width: screenWidth(context, dividedBy: 1),
                //           height: screenHeight(context, dividedBy: 3.4),
                //           decoration: BoxDecoration(
                //             color: Colors.transparent,
                //             // border: Border.all(color: Colors.white, width: 10.0),
                //             borderRadius: BorderRadius.all(
                //               Radius.circular(15),
                //             ),
                //           ),
                //         ))
                //     :
                // showControl == true
                //     ? Container()
                //     :
                // if()
                // Center(
                //   child: GestureDetector(
                //     onTap: () {
                //       showControl == false
                //           ? setState(() {
                //         showControl = true;
                //         print("_______________________________");
                //         print(showControl);
                //       })
                //           : setState(() {
                //         showControl = false;
                //         print("_______________________________");
                //         print(showControl);
                //       });
                //     },
                //     child: Container(
                //       width: screenWidth(context, dividedBy: 1),
                //       height: screenHeight(context, dividedBy: 3.4),
                //       decoration: BoxDecoration(
                //         color: Colors.transparent,
                //         // border: Border.all(color: Colors.white, width: 10.0),
                //         borderRadius: BorderRadius.all(
                //           Radius.circular(15),
                //         ),
                //       ),
                //       child: GestureDetector(
                //         onTap: () {
                //           _chewieController.isPlaying
                //               ? setState(() {
                //                   _chewieController.pause();
                //                   isPlaying = false;
                //                   print("Video paused");
                //                 })
                //               : isComplete == true
                //                   ? setState(() {
                //                       widget.videoPlayerController.seekTo(
                //                           Duration(
                //                               seconds: 0, minutes: 0, hours: 0));
                //                       _chewieController.play();
                //                       isPlaying = true;
                //                       print("Video reply");
                //                     })
                //                   : setState(
                //                       () {
                //                         _chewieController.play();
                //                         isPlaying = true;
                //                         print("Video playing");
                //                       },
                //                     );
                //         },
                //         child: Center(
                //           child: Container(
                //             width: screenWidth(context, dividedBy: 7),
                //             height: screenWidth(context, dividedBy: 7),
                //             decoration: BoxDecoration(
                //                 color: _chewieController.isPlaying
                //                     ? Colors.transparent
                //                     : Constants.kitGradients[0].withOpacity(0.35),
                //                 // border: Border.all(color: Colors.white, width: 10.0),
                //                 shape: BoxShape.circle),
                //             child: Center(
                //               child: Icon(
                //                   isPlaying == false
                //                       ? Icons.play_arrow
                //                       : isComplete == true
                //                           ? Icons.refresh
                //                           : Icons.pause_outlined,
                //                   size: 30,
                //                   color: _chewieController.isPlaying
                //                       ? Colors.transparent
                //                       : Constants.kitGradients[0]),
                //             ),
                //           ),
                //         ),
                //       ),
                //     ),
                //   ),
                // ),
                Positioned(
                  // top: screenHeight(context, dividedBy: 11),
                  // left: screenWidth(context, dividedBy: 7),
                  // right: screenWidth(context, dividedBy: 7),
                  // top: screenHeight(context, dividedBy: 15),
                  // left: screenWidth(context, dividedBy: 7),
                  // right: screenWidth(context, dividedBy: 7),
                  child: GestureDetector(
                    onTap: () {
                      print("Tapped");
                      ObjectFactory().appHive.getOrderStatus() == true
                          ? print("Subscribed")
                          : widget.videoIndex == 0
                              ? print("Vide Index is 0")
                              : widget.videoIndex == 1
                                  ? print("Vide Index is 1")
                                  : showSubscription();
                      setState(() {
                        ObjectFactory().appHive.getOrderStatus() == true
                            ? _chewieController.play()
                            : _chewieController.pause();
                        isPlaying = true;
                      });
                    },
                    child: isPlaying == true
                        ? Container()
                        : Container(
                            child: Center(
                              child: Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 3.4),
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  // border: Border.all(color: Colors.white, width: 10.0),
                                  // shape: BoxShape.circle
                                ),
                                // child: Center(
                                //   child: Icon(
                                //        Icons.play_arrow,
                                //           // : isComplete == true
                                //           //     ? Icons.refresh
                                //           //     : Icons.pause_outlined,
                                //       size: 30,
                                //       color:  Constants.kitGradients[0]),
                                // ),
                              ),
                            ),
                          ),
                  ),
                ),
                // Positioned(
                //   // bottom: screenHeight(context, dividedBy: 100),
                //   // left: screenWidth(context, dividedBy: 70),
                //   bottom: screenHeight(context, dividedBy: 90),
                //   left: screenWidth(context, dividedBy: 25),
                //   // right: screenWidth(context, dividedBy: 7),
                //   child: GestureDetector(
                //     onTap: () {
                //       print("Tapped");
                //       ObjectFactory().appHive.getOrderStatus() == true
                //           ? print("Subscribed")
                //           : showSubscriptionAlertDialog(context);
                //       setState(() {
                //         ObjectFactory().appHive.getOrderStatus() == true
                //             ? _chewieController.play()
                //             : _chewieController.pause();
                //         isPlaying = true;
                //       });
                //     },
                //     child: isPlaying == true
                //         ? Container()
                //         : Container(
                //             child: Center(
                //               child: Container(
                //                 width: screenWidth(context, dividedBy: 10),
                //                 height: screenWidth(context, dividedBy: 10),
                //                 decoration: BoxDecoration(
                //                     color: Colors.blue.withOpacity(0.5),
                //                     // border: Border.all(color: Colors.white, width: 10.0),
                //                     shape: BoxShape.circle
                //                 ),
                //                 // child: Center(
                //                 //   child: Icon(
                //                 //        Icons.play_arrow,
                //                 //           // : isComplete == true
                //                 //           //     ? Icons.refresh
                //                 //           //     : Icons.pause_outlined,
                //                 //       size: 30,
                //                 //       color:  Constants.kitGradients[0]),
                //                 // ),
                //               ),
                //             ),
                //           ),
                //   ),
                // ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          GestureDetector(
            onTap: widget.onTap,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30),
              ),
              child: Text(
                widget.description,
                textAlign: TextAlign.justify,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontFamily: 'SofiaProSemiBold'),
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
        ],
      ),
    );
    // : Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     children: const [
    //       CircularProgressIndicator(),
    //       SizedBox(height: 20),
    //       Text('Loading'),
    //     ],
    //   );
  }
}
