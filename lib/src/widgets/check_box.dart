import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CCheckBox extends StatefulWidget {
  final String label;
  final Function onPressed;
  final int id;
  final ValueChanged<int> onChanged;
  CCheckBox({this.label, this.onPressed, this.onChanged, this.id});
  @override
  _CCheckBoxState createState() => _CCheckBoxState();
}

class _CCheckBoxState extends State<CCheckBox> {
  bool value = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          this.value = value;
        });
      },
      child: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 20),
          //color: Colors.red,
          child: Row(
            children: [
              Checkbox(
                value: value,
                onChanged: (value) {
                  widget.onPressed();
                  setState(() {
                    this.value = value;
                  });
                },
                activeColor: Colors.blue,
                checkColor: Colors.white,
                tristate: false,
              ),
              Text(widget.label,
                  style: TextStyle(
                      fontFamily: "SofiaProRegular",
                      fontSize: 15,
                      color: Color(0xff2C2E39)))
            ],
          )),
    );
  }
}
