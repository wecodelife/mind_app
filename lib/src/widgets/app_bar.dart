import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CAppBar extends StatefulWidget {
  final String title;
  final Color color;
  final bool isWhite;
  final Function onPressedLeftIcon;
  CAppBar({this.title, this.color, this.isWhite, this.onPressedLeftIcon});
  @override
  _CAppBarState createState() => _CAppBarState();
}

class _CAppBarState extends State<CAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 16),
        color: widget.color,
        alignment: Alignment.centerLeft,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: screenWidth(context, dividedBy: 30)),
            GestureDetector(
              onTap: widget.onPressedLeftIcon,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  Icon(
                    Icons.arrow_back,
                    size:screenHeight(context, dividedBy: 30),
                    color: widget.isWhite ? Colors.white : Colors.black,
                  ),
                ],
              ),
            ),
            SizedBox(width: screenWidth(context, dividedBy: 20)),
            Text(
              widget.title,
              style: TextStyle(
                  fontSize: screenHeight(context, dividedBy: 40),
                  color: widget.isWhite ? Colors.white : Colors.black,
                  fontWeight: FontWeight.w400,
                  fontFamily: "SofiaProRegular"),
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),);
  }
}
