import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AppListTile extends StatefulWidget {
  final String text;
  final Function onTap;
  final bool divider;
  //final String songCount;
  AppListTile({this.text, this.onTap, this.divider
      // this.songCount
      });
  @override
  _AppListTileState createState() => _AppListTileState();
}

class _AppListTileState extends State<AppListTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Column(children: [
        Container(
            width: screenWidth(context, dividedBy: 1),
            color: Colors.transparent,
            // height:
            // screenHeight(context, dividedBy: 15),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 50),
              vertical: screenHeight(context, dividedBy: 70),
            ),
            alignment: Alignment.centerLeft,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Container(
                //     //width: screenWidth(context, dividedBy: 40),
                //     child: widget.icon),
                // SizedBox(
                //   width: screenWidth(context, dividedBy: 15),
                // ),
                Text(
                  widget.text,
                  style: TextStyle(
                    fontSize: 15,
                    fontFamily: "Prompt-Light",
                    fontWeight: FontWeight.w400,
                    color: Constants.kitGradients[2],
                  ),
                ),
                Spacer(),
                // Text(
                //   widget.songCount,
                //   style: TextStyle(
                //     fontSize: 18,
                //     fontFamily: "SofiaProRegular",
                //     fontWeight: FontWeight.w400,
                //     color: Constants.kitGradients[19],
                //   ),
                // ),
                // SizedBox(
                //   width: screenWidth(context, dividedBy: 40),
                // ),
                Icon(
                  Icons.keyboard_arrow_right_rounded,
                  size: 20,
                  color: Constants.kitGradients[2].withOpacity(0.4),
                ),
              ],
            )),
        widget.divider == false
            ? Container()
            : Divider(
                color: Constants.kitGradients[2].withOpacity(0.2),
              ),
      ]),
    );
  }
}
