import 'dart:async';

import 'package:app_template/src/models/book_service_request.dart';
import 'package:app_template/src/models/header_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/order_plan_request_model.dart';
import 'package:app_template/src/models/update_answer_request.dart';
import 'package:app_template/src/models/update_user_details_request_model.dart';
import 'package:app_template/src/models/user_subscription_plan_request.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:dio/dio.dart';

import 'object_factory.dart';

enum AccessMode { READ, WRITE }
HeaderModel authHeaderModel = new HeaderModel();

void setAuthHeaderModel({var accessMode, String xReferenceId}) {
  if (ObjectFactory().appHive != null &&
      ObjectFactory().appHive.getToken() != null &&
      ObjectFactory().appHive.getToken().trim().length > 0)
    authHeaderModel.authorization =
        "Token " + ObjectFactory().appHive.getToken();
  // authHeaderModel.xAccessMode = accessMode.toString();
  // authHeaderModel.xUdid = "";
  // authHeaderModel.xRequestTime = "";
  // authHeaderModel.xReferenceId = xReferenceId;
  // authHeaderModel.xSession = "";
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getXUser() != null &&
  //     ObjectFactory().appHive.getXUser().trim().length > 0)
  //   authHeaderModel.xUser = ObjectFactory().appHive.getXUser();
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getUserId() != null &&
  //     ObjectFactory().appHive.getUserId().trim().length > 0)
  //   authHeaderModel.xUserId = ObjectFactory().appHive.getUserId();
  // print("Token "+authHeaderModel.xTenantId);
  // print("print" + authHeaderModel.authorization);
}

class ApiClient {
  HeaderModel loginModel = new HeaderModel();

  ///  user login
  Future<Response> loginRequest(LoginRequestModel loginRequestModel) {
    // print(loginRequest.toString());

    return ObjectFactory()
        .appDio
        .loginPost(url: Urls.loginUrl, data: loginRequestModel);
  }

  Future<Response> logoutRequest() {
    // print(loginRequest.toString());
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.logoutUrl, header: authHeaderModel);
  }

  Future<Response> getQuestionType(int id) {
    // print(loginRequest.toString());
    setAuthHeaderModel();
    return ObjectFactory().appDio.get(
        url: Urls.questionTypeUrl + id.toString(), header: authHeaderModel);
  }

  Future<Response> getQuestions(int id) {
    print("sjs" + id.toString());
    setAuthHeaderModel();
    print(Urls.questionsUrl +
        id.toString() +
        "&exam_id=" +
        ObjectFactory().appHive.getExamId());
    return ObjectFactory().appDio.get(
        url: Urls.questionsUrl +
            id.toString() +
            "&exam_id=" +
            ObjectFactory().appHive.getExamId(),
        header: authHeaderModel);
  }

  Future<Response> updateAnswer(UpdateAnswerRequest updateAnswerRequest) {
    // print(id.toString());
    setAuthHeaderModel();
    return ObjectFactory().appDio.post(
        url: Urls.questionAnswerUrl,
        header: authHeaderModel,
        data: updateAnswerRequest);
  }

  Future<Response> getServices() {
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.servicesUrl, header: authHeaderModel);
  }

  Future<Response> getResume(String videoListId) {
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    print(Urls.resumePreparationUrl + videoListId);
    return ObjectFactory().appDio.get(
        url: Urls.resumePreparationUrl + videoListId, header: authHeaderModel);
  }

  Future<Response> bookService(BookServiceRequest bookServiceRequest) {
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    return ObjectFactory().appDio.post(
        url: Urls.bookServiceUrl,
        header: authHeaderModel,
        data: bookServiceRequest);
  }

  Future<Response> getInterviewQuestionAnswer(String type) {
    setAuthHeaderModel();
    print("Api Called");
    return ObjectFactory()
        .appDio
        .get(url: Urls.interviewQuestionAnswerUrl, header: authHeaderModel);
  }

  Future<Response> getJobList() {
    setAuthHeaderModel();
    print("Api Called");
    return ObjectFactory()
        .appDio
        .get(url: Urls.jobListUrl, header: authHeaderModel);
  }

  Future<Response> getJobRead(
      String involvement, String type, List<String> category, String next) {
    setAuthHeaderModel();
    print(Urls.jobReadUrl +
        "?category=" +
        category.toString() +
        "&"
            "?involvement=$involvement&" +
        "/job_type=$type" +
        "&?next=$next");

    return ObjectFactory().appDio.get(
        url: Urls.jobReadUrl +
            "?category=" +
            category.toString() +
            "&"
                "?involvement=$involvement&" +
            "/job_type=$type" +
            "&?next=$next",
        header: authHeaderModel);
  }

  Future<Response> getResult() {
    setAuthHeaderModel();
    print(Urls.result + "?exam_id=" + ObjectFactory().appHive.getExamId());
    return ObjectFactory().appDio.get(
          url: Urls.result + "?exam_id=" + ObjectFactory().appHive.getExamId(),
          header: authHeaderModel,
        );
  }

  Future<Response> deleteQuestionAnswer() {
    setAuthHeaderModel();
    print(
        Urls.deleteQuetsionAnswer + ObjectFactory().appHive.getExamId() + "/");
    return ObjectFactory().appDio.delete(
          url: Urls.deleteQuetsionAnswer +
              ObjectFactory().appHive.getExamId() +
              "/",
          header: authHeaderModel,
        );
  }

  Future<Response> getResourceType() {
    setAuthHeaderModel();
    print(Urls.getResourceType);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getResourceType, header: authHeaderModel);
  }

  Future<Response> updateUserDetail(
      UpdateUserDetailsRequestModel updateUserDetailsRequest) async {
    setAuthHeaderModel();
    FormData formDataUpdateUserDetails = FormData.fromMap({
      "first_name": updateUserDetailsRequest.firstName,
      "last_name": updateUserDetailsRequest.lastName,
      "profile_image": await MultipartFile.fromFile(
          updateUserDetailsRequest.profileImage.path,
          filename: updateUserDetailsRequest.profileImage.path.split('/').last),
      "phone_number": updateUserDetailsRequest.phoneNumber
    });
    // print(Urls.updateUserDetail +
    //     ObjectFactory().appHive.getId().toString() +
    //     "/");
    return ObjectFactory().appDio.patch(
        url: Urls.updateUserDetail +
            ObjectFactory().appHive.getId().toString() +
            "/",
        header: authHeaderModel,
        data: formDataUpdateUserDetails);
  }

  Future<Response> getSliderImages() {
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.getSliderImageUrl, header: authHeaderModel);
  }
  Future<Response> getSubscriptionPlans() {
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.getSubscriptionPlansUrl, header: authHeaderModel);
  }

  Future<Response> orderPlans(OrderPlanRequest orderPlanRequest) {
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    return ObjectFactory().appDio.post(
        url: Urls.orderPlansUrl,
        header: authHeaderModel,
        data: orderPlanRequest);
  }
  Future<Response> getOrderPayments({String orderId}) {
    // print("sjs" + id.toString());
    print("Order Payment Details");
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.orderPaymentUrl+"$orderId", header: authHeaderModel);
  }

  Future<Response> userSubscriptionPlan(UserSubscriptionRequest userSubscriptionRequest) {
    print("UserSubscriptionPlan api client ok");
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    return ObjectFactory().appDio.post(
        url: Urls.userSubscriptionPlanUrl,
        header: authHeaderModel,
        data: userSubscriptionRequest);
  }

  Future<Response> getSingleUserSubscriptionPlan({int userId}) {
    // print("sjs" + id.toString());
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.singleUserSubscriptionUrl+"$userId", header: authHeaderModel);
  }

}
