class Urls {
  static final baseUrl = "http://3.142.247.21:8000/api/v1/";
  // "https://api.upyoulearnings.com/api/v1/";
  static final imageBaseUrl = "http://3.142.247.21:8000";
  static final loginUrl = baseUrl + "auth/login/";
  static final logoutUrl = baseUrl + "auth/logout/";
  static final questionTypeUrl = baseUrl + "question-type/?category=";
  static final questionsUrl = baseUrl + "question/?question_type=";
  static final questionAnswerUrl = baseUrl + "question-answer/";
  static final servicesUrl = baseUrl + "services/";
  static final resumePreparationUrl =
      baseUrl + "resume-preparation/?resume_preparation_type=";

  static final bookServiceUrl = baseUrl + "services-booking/";
  static final interviewQuestionAnswerUrl = baseUrl + "resources/";
  static final jobListUrl = baseUrl + "job-category/";
  static final jobReadUrl = baseUrl + "job/";
  static final result = baseUrl + "question-answer/result/";
  static final deleteQuetsionAnswer = baseUrl + "question-answer/";
  static final getResourceType = baseUrl + "resource-type/";
  static final updateUserDetail = baseUrl + "users/";
  static final getSliderImageUrl = baseUrl + "slider/";
  static final getSubscriptionPlansUrl = baseUrl + "subscription-plan/";
  static final orderPlansUrl = baseUrl + "order/";
  static final orderPaymentUrl = baseUrl + "order/?order_id=";
  static final userSubscriptionPlanUrl = baseUrl + "user-subscription-plan/";
  static final singleUserSubscriptionUrl =
      baseUrl + "user-subscription-plan/?user=";
}
