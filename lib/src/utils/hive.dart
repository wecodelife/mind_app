import 'package:hive/hive.dart';

import 'constants.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }
  static const String _USER_ID = "user_id";
  static const String _NAME = "name";
  static const String _TOKEN = "token";
  static const String _EMAIL = "email";
  static const String _PHOTOURL = "photourl";
  static const String _XUSER = "newuserid";
  static const String _PASSWORD = "userpassword";
  static const String _QUESTION_USER_ID = "question_user_id";
  static const String _SIGNED_IN = "sign in";
  static const String _VIDEOLIST_ID = "videolistid";
  static const String _RESPONSE_STATUS = "responsestatus";
  static const String _RESPONSE_MESSAGE = "responsemessage";
  static const String _EXAM_COMPLETED = "examcompleted";
  static const String _ID = "ID";
  static const String _PHONENUMBER = "phoneNumber";
  static const String _ORDERSTATUS = "order_status";

  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  void hivePutBool({String key, bool value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  bool hiveGetBool({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  void hivePutInt({String key, int value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  int hiveGetInt({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  putUserId({String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  String getUserId() {
    return hiveGet(key: _USER_ID);
  }

  putId({int id}) {
    hivePutInt(key: _ID, value: id);
  }

  int getId() {
    return hiveGetInt(key: _ID);
  }

  putOrderStatus({bool status}) {
    hivePutBool(key: _ORDERSTATUS, value: status);
  }

  bool getOrderStatus() {
    return hiveGetBool(key: _ORDERSTATUS);
  }

  putPhoneNumber({String phoneNumber}) {
    hivePut(key: _PHONENUMBER, value: phoneNumber);
  }

  String getPhoneNumber() {
    return hiveGet(key: _PHONENUMBER);
  }

  putExamId({String examId}) {
    hivePut(key: _QUESTION_USER_ID, value: examId);
  }

  String getExamId() {
    return hiveGet(key: _QUESTION_USER_ID);
  }

  putUserPassword({String email}) {
    hivePut(key: _PASSWORD, value: email);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  putName({String name}) {
    hivePut(key: _NAME, value: name);
  }

  String getName() {
    return hiveGet(key: _NAME);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  String getEmail() {
    return hiveGet(key: _EMAIL);
  }

  putEmail(String value) {
    return hivePut(key: _EMAIL, value: value);
  }

  bool getSignedInCheck() {
    return hiveGetBool(key: _SIGNED_IN);
  }

  putSignedInCheck(bool value) {
    return hivePutBool(key: _SIGNED_IN, value: value);
  }

  String getVideoListId() {
    return hiveGet(key: _VIDEOLIST_ID);
  }

  putVideoListId({String videoListId}) {
    return hivePut(key: _VIDEOLIST_ID, value: videoListId);
  }

  putPhotoUrl(String value) {
    return hivePut(key: _PHOTOURL, value: value);
  }

  String getPhotoUrl() {
    return hiveGet(key: _PHOTOURL);
  }

  putResponseStatusJobList({bool value}) {
    return hivePutBool(key: _RESPONSE_STATUS, value: value);
  }

  bool getResponseStatusJobList() {
    return hiveGetBool(key: _RESPONSE_STATUS);
  }

  putResponseMessage(String value) {
    return hivePut(key: _RESPONSE_MESSAGE, value: value);
  }

  String getResponseMessage() {
    return hiveGet(key: _RESPONSE_MESSAGE);
  }

  putExamCompletedStatus(int value) {
    return hivePutInt(key: _EXAM_COMPLETED, value: value);
  }

  int getExamCompletedStatus() {
    return hiveGetInt(key: _EXAM_COMPLETED);
  }

  AppHive();
}
