import 'dart:async';

import 'package:app_template/src/models/book_service_request.dart';
import 'package:app_template/src/models/book_service_response.dart';
import 'package:app_template/src/models/delete_answer_response.dart';
import 'package:app_template/src/models/get_question_type_response.dart';
import 'package:app_template/src/models/get_questions.dart';
import 'package:app_template/src/models/get_resouce_type_response_model.dart';
import 'package:app_template/src/models/get_result_response_model.dart';
import 'package:app_template/src/models/get_resume_preparation.dart';
import 'package:app_template/src/models/get_service_response.dart';
import 'package:app_template/src/models/get_single_user_subscription_plan.dart';
import 'package:app_template/src/models/interview_question_answer_response_model.dart';
import 'package:app_template/src/models/job_list_response_model.dart';
import 'package:app_template/src/models/job_read_response_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/logout_response_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_answer_request.dart';
import 'package:app_template/src/models/update_answer_response.dart';
import 'package:app_template/src/models/update_user_details_request_model.dart';
import 'package:app_template/src/models/update_user_details_response_model.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/validators.dart';
import 'package:app_template/src/models/get_slider_image_response.dart';
import 'package:app_template/src/models/get_subscription_plans_response.dart';
import 'package:app_template/src/models/order_plan_request_model.dart';
import 'package:app_template/src/models/order_plan_response_model.dart';
import 'package:app_template/src/models/get_order_payment_response.dart';
import 'package:app_template/src/models/user_subscription_plan_request.dart';
import 'package:app_template/src/models/user_subscription_plan_response.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<LoginResponse> _login =
      new StreamController<LoginResponse>.broadcast();

  StreamController<LogoutResponse> _logout =
      new StreamController<LogoutResponse>.broadcast();

  StreamController<GetQuestionTypeResponse> _getQuestionType =
      new StreamController<GetQuestionTypeResponse>.broadcast();

  StreamController<GetQuestionResponse> _getQuestion =
      new StreamController<GetQuestionResponse>.broadcast();

  StreamController<UpdateAnswerResponse> _updateAnswer =
      new StreamController<UpdateAnswerResponse>.broadcast();

  StreamController<GetServicesResponse> _getService =
      new StreamController<GetServicesResponse>.broadcast();

  StreamController<GetResumePreparationResponse> _getResumePreparation =
      new StreamController<GetResumePreparationResponse>.broadcast();

  StreamController<BookServiceResponse> _bookService =
      new StreamController<BookServiceResponse>.broadcast();

  StreamController<InterviewQuestionAnswerResponseModel>
      _getInterviewQuestionAnswers =
      new StreamController<InterviewQuestionAnswerResponseModel>.broadcast();

  StreamController<JobListResponseModel> _getJobListResponse =
      new StreamController<JobListResponseModel>.broadcast();

  StreamController<JobReadResponseModel> _getJobRead =
      new StreamController<JobReadResponseModel>.broadcast();

  StreamController<GetResultResponse> _getResult =
      new StreamController<GetResultResponse>.broadcast();
  StreamController<DeleteAnswerResponse> _deleteQuestionAnswer =
      new StreamController<DeleteAnswerResponse>.broadcast();
  StreamController<GetResouceTypeResponse> _getResourceType =
      new StreamController<GetResouceTypeResponse>.broadcast();
  StreamController<UpdateUserDetailsResponseModel> _updateUserDetails =
      new StreamController<UpdateUserDetailsResponseModel>.broadcast();

  StreamController<GetSliderImageResponse> _getSliderImages =
  new StreamController<GetSliderImageResponse>.broadcast();

  StreamController<GetSubscriptionPlansResponse> _getSubscriptionPlans =
  new StreamController<GetSubscriptionPlansResponse>.broadcast();

  StreamController<OrderPlanResponse> _orderPlan =
  new StreamController<OrderPlanResponse>.broadcast();

  StreamController<GetOrderPaymentResponse> _getOrderPayment = new StreamController<GetOrderPaymentResponse>.broadcast();

  StreamController<UserSubscriptionResponse> _userSubscriptionPlan =
  new StreamController<UserSubscriptionResponse>.broadcast();

  StreamController<GetSingleUserSubscriptionPlan> _getSingleUserSubscriptionPlan = new StreamController<GetSingleUserSubscriptionPlan>.broadcast();

  // ignore: close_sinks

  // stream controller is broadcasting the  details

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  Stream<LoginResponse> get loginResponse => _login.stream;

  Stream<LogoutResponse> get logoutResponse => _logout.stream;

  Stream<GetQuestionTypeResponse> get getQuestionTypeResponse =>
      _getQuestionType.stream;

  Stream<GetQuestionResponse> get getQuestionsResponse => _getQuestion.stream;

  Stream<UpdateAnswerResponse> get updateAnswerResponse => _updateAnswer.stream;

  Stream<GetServicesResponse> get getServiceResponse => _getService.stream;

  Stream<GetResumePreparationResponse> get getGetResumePreparationResponse =>
      _getResumePreparation.stream;
  Stream<GetResultResponse> get getResultResponse => _getResult.stream;

  Stream<BookServiceResponse> get bookServiceResponse => _bookService.stream;

  Stream<InterviewQuestionAnswerResponseModel>
      get getInterviewQuestionAnswerResponse =>
          _getInterviewQuestionAnswers.stream;
  Stream<JobListResponseModel> get getJobListResponse =>
      _getJobListResponse.stream;
  Stream<JobReadResponseModel> get getJobReadResponse => _getJobRead.stream;
  Stream<DeleteAnswerResponse> get deleteQuestionAnswerResponse =>
      _deleteQuestionAnswer.stream;
  Stream<GetResouceTypeResponse> get getResourceTypeResponse =>
      _getResourceType.stream;
  Stream<UpdateUserDetailsResponseModel> get updateUserDetailsResponse =>
      _updateUserDetails.stream;
  Stream<GetSliderImageResponse> get getSliderImageResponse => _getSliderImages.stream;

  Stream<GetSubscriptionPlansResponse> get getSubscriptionPlans => _getSubscriptionPlans.stream;

  Stream<OrderPlanResponse> get orderPlanResponse => _orderPlan.stream;

  Stream<GetOrderPaymentResponse> get getOrderPaymentResponse => _getOrderPayment.stream;

  Stream<UserSubscriptionResponse> get userSubscriptionPlan => _userSubscriptionPlan.stream;

  Stream<GetSingleUserSubscriptionPlan> get getSingleUserSubscriptionPlan => _getSingleUserSubscriptionPlan.stream;


  StreamSink<bool> get loadingSink => _loading.sink;

  login({LoginRequestModel loginRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .login(loginRequest: LoginRequestModel(uid: loginRequest.uid));

    if (state is SuccessState) {
      loadingSink.add(false);
      _login.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _login.sink.addError(state.msg);
    }
  }

  logout() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.logout();

    if (state is SuccessState) {
      loadingSink.add(false);
      _logout.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _logout.sink.addError(state.msg);
    }
  }

  getQuestionType({int id}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getQuestionType(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getQuestionType.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getQuestionType.sink.addError(state.msg);
    }
  }

  getQuestions({int id}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getQuestion(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getQuestion.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      print(state.msg.errorMessage.toString());
      _getQuestion.sink.addError(state.msg);
    }
  }

  updateAnswer({UpdateAnswerRequest updateAnswerRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.updateAnswer(updateAnswerRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateAnswer.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateAnswer.sink.addError(state.msg.error);
    }
  }

  getServices() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getService();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getService.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getService.sink.addError(state.msg);
    }
  }

  getResumePreparation({String videoId}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.getResumePreparation(videoId: videoId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getResumePreparation.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getResumePreparation.sink.addError(state.msg);
    }
  }

  bookService({BookServiceRequest bookServiceRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.bookService(bookServiceRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _bookService.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _bookService.sink.addError(state.msg);
    }
  }

  getInterviewQuestionsAnswer(String type) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .getInterviewQuestionAnswers(type: type);
    if (state is SuccessState) {
      loadingSink.add(false);
      _getInterviewQuestionAnswers.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getInterviewQuestionAnswers.sink.addError(state.msg);
    }
  }

  getJobList() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getJobList();
    if (state is SuccessState) {
      loadingSink.add(false);
      _getJobListResponse.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getJobListResponse.sink.addError(state.msg);
    }
  }

  getJobRead(
      {String involvement,
      String jobType,
      List<String> category,
      String next}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getJobRead(
        jobType: jobType,
        involvement: involvement,
        category: category,
        next: next);
    if (state is SuccessState) {
      loadingSink.add(false);
      _getJobRead.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getJobRead.sink.addError(state.msg);
    }
  }

  getResult() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getResult();
    if (state is SuccessState) {
      loadingSink.add(false);
      _getResult.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getResult.sink.addError(state.msg);
    }
  }

  deleteQuestionAnswer() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.deleteQuestionAnswer();
    if (state is SuccessState) {
      loadingSink.add(false);
      _deleteQuestionAnswer.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _deleteQuestionAnswer.sink.addError(state.msg);
    }
  }

  getResourceType() async {
    loadingSink.add(true);
    print("UserBloc");
    State state = await ObjectFactory().repository.getResourceType();
    print("state" + state.toString());
    if (state is SuccessState) {
      loadingSink.add(false);
      _getResourceType.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getResourceType.sink.addError(state.msg);
    }
  }

  updateUserDetails(
      {UpdateUserDetailsRequestModel updateUserDetailsRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .updateUserDetail(updateUserDetailsRequest: updateUserDetailsRequest);
    if (state is SuccessState) {
      print("State is Succes");
      loadingSink.add(false);
      _updateUserDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateUserDetails.sink.addError(state.msg);
    }
  }
  getSliderImages() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getSliderImages();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getSliderImages.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getSliderImages.sink.addError(state.msg);
    }
  }
  getSubscriptionPlan() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getSubscriptionPlans();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getSubscriptionPlans.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getSubscriptionPlans.sink.addError(state.msg);
    }
  }

  orderPlans({OrderPlanRequest orderPlanRequest}) async {
    loadingSink.add(true);

    State state =
    await ObjectFactory().repository.orderPlans(orderPlanRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _orderPlan.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _orderPlan.sink.addError(state.msg);
    }
  }

  getOrderPayments({String orderId}) async {
    print("User bloc Order Payment Details");
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getOrderPayments(orderId: orderId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getOrderPayment.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getOrderPayment.sink.addError(state.msg);
    }
  }

  userSubscriptionPlans({UserSubscriptionRequest userSubscriptionRequest}) async {
    loadingSink.add(true);
    print("UserSubscriptionPlan userbloc ok");
    State state =
    await ObjectFactory().repository.userSubscriptionPlan(userSubscriptionRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _userSubscriptionPlan.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _userSubscriptionPlan.sink.addError(state.msg);
    }
  }

  getSingleUserSubscriptionPlans({int userId}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getSingleUserSubscriptionPlan(userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getSingleUserSubscriptionPlan.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getSingleUserSubscriptionPlan.sink.addError(state.msg);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _login?.close();
    _logout?.close();
    _getQuestionType?.close();
    _getQuestion?.close();
    _updateAnswer?.close();
    _getService?.close();
    _getResumePreparation?.close();
    _bookService?.close();
    _getInterviewQuestionAnswers?.close();
    _getJobListResponse?.close();
    _getJobRead?.close();
    _getResult?.close();
    _deleteQuestionAnswer.close();
    _getResourceType.close();
    _updateUserDetails.close();
    _getSliderImages?.close();
    _getSubscriptionPlans?.close();
    _orderPlan?.close();
    _getOrderPayment?.close();
    _userSubscriptionPlan?.close();
  }
}
