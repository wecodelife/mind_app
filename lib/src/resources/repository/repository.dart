import 'package:app_template/src/models/book_service_request.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_answer_request.dart';
import 'package:app_template/src/models/update_user_details_request_model.dart';
import 'package:app_template/src/resources/api_providers/user_api_provider.dart';
import 'package:app_template/src/models/get_slider_image_response.dart';
import 'package:app_template/src/models/order_plan_request_model.dart';
import 'package:app_template/src/models/user_subscription_plan_request.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequestModel loginRequest}) =>
      UserApiProvider().loginCall(loginRequest);

  Future<State> logout() => UserApiProvider().logoutCall();

  Future<State> getQuestionType(int id) =>
      UserApiProvider().getQuestionType(id);

  Future<State> getQuestion(int id) => UserApiProvider().getQuestion(id);

  Future<State> updateAnswer(UpdateAnswerRequest updateAnswerRequest) =>
      UserApiProvider().updateAnswer(updateAnswerRequest);

  Future<State> getService() => UserApiProvider().getServices();

  Future<State> getResumePreparation({String videoId}) =>
      UserApiProvider().getResumePreparation(videoId: videoId);

  Future<State> bookService(BookServiceRequest bookServiceRequest) =>
      UserApiProvider().bookService(bookServiceRequest);

  Future<State> getInterviewQuestionAnswers({String type}) =>
      UserApiProvider().getInterviewQuestionAnswer(type: type);

  Future<State> getJobList() => UserApiProvider().getJobList();

  Future<State> getJobRead(
          {String involvement,
          String jobType,
          List<String> category,
          String next}) =>
      UserApiProvider().getJobRead(
          involvement: involvement,
          jobType: jobType,
          category: category,
          next: next);

  Future<State> getResult() => UserApiProvider().getResult();
  Future<State> deleteQuestionAnswer() =>
      UserApiProvider().deleteQuestionAnswer();
  Future<State> getResourceType() => UserApiProvider().getResourceType();
  Future<State> updateUserDetail(
          {UpdateUserDetailsRequestModel updateUserDetailsRequest}) =>
      UserApiProvider().updateUserDetails(updateUserDetailsRequest);

  Future<State> getSliderImages() => UserApiProvider().getSliderImages();
  Future<State> getSubscriptionPlans() => UserApiProvider().getSubscriptionPlans();
  Future<State> orderPlans(OrderPlanRequest orderPlanRequest) =>
      UserApiProvider().orderPlans(orderPlanRequest);
  Future<State> getOrderPayments({String orderId}) => UserApiProvider().getOrderPayments(orderId: orderId);
  Future<State> userSubscriptionPlan(UserSubscriptionRequest userSubscriptionRequest) =>
      UserApiProvider().userSubscriptionPlan(userSubscriptionRequest);

  Future<State> getSingleUserSubscriptionPlan({int userId}) => UserApiProvider().getSingleUserSubscriptionPlan(userId: userId);
}
