import 'package:app_template/src/models/book_service_request.dart';
import 'package:app_template/src/models/book_service_response.dart';
import 'package:app_template/src/models/delete_answer_response.dart';
import 'package:app_template/src/models/get_order_payment_response.dart';
import 'package:app_template/src/models/get_question_type_response.dart';
import 'package:app_template/src/models/get_questions.dart';
import 'package:app_template/src/models/get_resouce_type_response_model.dart';
import 'package:app_template/src/models/get_result_response_model.dart';
import 'package:app_template/src/models/get_resume_preparation.dart';
import 'package:app_template/src/models/get_service_response.dart';
import 'package:app_template/src/models/get_single_user_subscription_plan.dart';
import 'package:app_template/src/models/get_slider_image_response.dart';
import 'package:app_template/src/models/get_subscription_plans_response.dart';
import 'package:app_template/src/models/interview_question_answer_response_model.dart';
import 'package:app_template/src/models/job_list_response_model.dart';
import 'package:app_template/src/models/job_read_response_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/logout_response_model.dart';
import 'package:app_template/src/models/order_plan_request_model.dart';
import 'package:app_template/src/models/order_plan_response_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_answer_request.dart';
import 'package:app_template/src/models/update_answer_response.dart';
import 'package:app_template/src/models/update_user_details_request_model.dart';
import 'package:app_template/src/models/update_user_details_response_model.dart';
import 'package:app_template/src/models/user_subscription_plan_request.dart';
import 'package:app_template/src/models/user_subscription_plan_response.dart';
import 'package:app_template/src/utils/network.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:dio/dio.dart';

class UserApiProvider {
  Future<State> loginCall(LoginRequestModel loginRequest) async {
    try {
      final response =
          await ObjectFactory().apiClient.loginRequest(loginRequest);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<LoginResponse>.success(
            LoginResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> logoutCall() async {
    try {
      final response = await ObjectFactory().apiClient.logoutRequest();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<LogoutResponse>.success(
            LogoutResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getQuestionType(int id) async {
    try {
      final response = await ObjectFactory().apiClient.getQuestionType(id);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetQuestionTypeResponse>.success(
            GetQuestionTypeResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getQuestion(int id) async {
    try {
      final response = await ObjectFactory().apiClient.getQuestions(id);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetQuestionResponse>.success(
            GetQuestionResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> updateAnswer(UpdateAnswerRequest updateAnswerRequest) async {
    final response =
        await ObjectFactory().apiClient.updateAnswer(updateAnswerRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<UpdateAnswerResponse>.success(
          UpdateAnswerResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getServices() async {
    try {
      final response = await ObjectFactory().apiClient.getServices();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetServicesResponse>.success(
            GetServicesResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getResumePreparation({String videoId}) async {
    try {
      final response = await ObjectFactory().apiClient.getResume(videoId);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetResumePreparationResponse>.success(
            GetResumePreparationResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> bookService(BookServiceRequest bookServiceRequest) async {
    try {
      final response =
          await ObjectFactory().apiClient.bookService(bookServiceRequest);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<BookServiceResponse>.success(
            BookServiceResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getInterviewQuestionAnswer({String type}) async {
    try {
      final response =
          await ObjectFactory().apiClient.getInterviewQuestionAnswer(type);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<InterviewQuestionAnswerResponseModel>.success(
            InterviewQuestionAnswerResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getJobList() async {
    try {
      final response = await ObjectFactory().apiClient.getJobList();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<JobListResponseModel>.success(
            JobListResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getJobRead(
      {String involvement,
      String jobType,
      List<String> category,
      String next}) async {
    try {
      final response = await ObjectFactory()
          .apiClient
          .getJobRead(involvement, jobType, category, next);

      print(response.toString());
      if (response.statusCode == 200) {
        ObjectFactory().appHive.putResponseStatusJobList(value: true);
        ObjectFactory().appHive.putResponseMessage(response.statusMessage);
        return State<JobReadResponseModel>.success(
            JobReadResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getResult() async {
    try {
      final response = await ObjectFactory().apiClient.getResult();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetResultResponse>.success(
            GetResultResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> deleteQuestionAnswer() async {
    try {
      final response = await ObjectFactory().apiClient.deleteQuestionAnswer();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<DeleteAnswerResponse>.success(
            DeleteAnswerResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getResourceType() async {
    try {
      final response = await ObjectFactory().apiClient.getResourceType();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetResouceTypeResponse>.success(
            GetResouceTypeResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> updateUserDetails(
      UpdateUserDetailsRequestModel updateUserDetailsRequest) async {
    try {
      final response = await ObjectFactory()
          .apiClient
          .updateUserDetail(updateUserDetailsRequest);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<UpdateUserDetailsResponseModel>.success(
            UpdateUserDetailsResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getSliderImages() async {
    try {
      final response = await ObjectFactory().apiClient.getSliderImages();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetSliderImageResponse>.success(
            GetSliderImageResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getSubscriptionPlans() async {
    try {
      final response = await ObjectFactory().apiClient.getSubscriptionPlans();
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetSubscriptionPlansResponse>.success(
            GetSubscriptionPlansResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> orderPlans(OrderPlanRequest orderPlanRequest) async {
    try {
      final response =
          await ObjectFactory().apiClient.orderPlans(orderPlanRequest);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<OrderPlanResponse>.success(
            OrderPlanResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getOrderPayments({String orderId}) async {
    print("api provider Order Payment Details");
    try {
      final response =
          await ObjectFactory().apiClient.getOrderPayments(orderId: orderId);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetOrderPaymentResponse>.success(
            GetOrderPaymentResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> userSubscriptionPlan(
      UserSubscriptionRequest userSubscriptionRequest) async {
    try {
      final response = await ObjectFactory()
          .apiClient
          .userSubscriptionPlan(userSubscriptionRequest);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<UserSubscriptionResponse>.success(
            UserSubscriptionResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getSingleUserSubscriptionPlan({int userId}) async {
    try {
      final response = await ObjectFactory()
          .apiClient
          .getSingleUserSubscriptionPlan(userId: userId);
      print(response.toString());
      if (response.statusCode == 200) {
        return State<GetSingleUserSubscriptionPlan>.success(
            GetSingleUserSubscriptionPlan.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }
}
