// To parse this JSON data, do
//
//     final getSliderImageResponse = getSliderImageResponseFromJson(jsonString);

import 'dart:convert';

GetSliderImageResponse getSliderImageResponseFromJson(String str) => GetSliderImageResponse.fromJson(json.decode(str));

String getSliderImageResponseToJson(GetSliderImageResponse data) => json.encode(data.toJson());

class GetSliderImageResponse {
  GetSliderImageResponse({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final List<Datum> data;

  factory GetSliderImageResponse.fromJson(Map<String, dynamic> json) => GetSliderImageResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.image,
  });

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String title;
  final String image;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    title: json["title"] == null ? null : json["title"],
    image: json["image"] == null ? null : json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "title": title == null ? null : title,
    "image": image == null ? null : image,
  };
}
