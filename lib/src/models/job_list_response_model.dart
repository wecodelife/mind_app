// To parse this JSON data, do
//
//     final jobListResponseModel = jobListResponseModelFromJson(jsonString);

import 'dart:convert';

JobListResponseModel jobListResponseModelFromJson(String str) =>
    JobListResponseModel.fromJson(json.decode(str));

String jobListResponseModelToJson(JobListResponseModel data) =>
    json.encode(data.toJson());

class JobListResponseModel {
  JobListResponseModel(
      {this.count, this.next, this.previous, this.results, this.errorMessage});

  int count;
  dynamic next;
  dynamic previous;
  String errorMessage;
  List<Result> results;

  factory JobListResponseModel.fromJson(Map<String, dynamic> json) =>
      JobListResponseModel(
        count: json["count"],
        next: json["next"],
        errorMessage: json["error_message"],
        previous: json["previous"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "error_message": errorMessage,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Result {
  Result({
    this.id,
    this.title,
    this.description,
    this.postedOn,
    this.company,
    this.endDate,
    this.applicationUrl,
    this.jobType,
    this.involvement,
    this.image,
    this.imageUrl,
  });

  int id;
  String title;
  String description;
  DateTime postedOn;
  String company;
  dynamic endDate;
  String applicationUrl;
  int jobType;
  int involvement;
  String image;
  String imageUrl;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        postedOn: DateTime.parse(json["posted_on"]),
        company: json["company"],
        endDate: json["end_date"],
        applicationUrl:
            json["application_url"] == null ? null : json["application_url"],
        jobType: json["job_type"],
        involvement: json["involvement"],
        image: json["image"],
        imageUrl: json["image_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "posted_on":
            "${postedOn.year.toString().padLeft(4, '0')}-${postedOn.month.toString().padLeft(2, '0')}-${postedOn.day.toString().padLeft(2, '0')}",
        "company": company,
        "end_date": endDate,
        "application_url": applicationUrl == null ? null : applicationUrl,
        "job_type": jobType,
        "involvement": involvement,
        "image": image,
        "image_url": imageUrl,
      };
}
