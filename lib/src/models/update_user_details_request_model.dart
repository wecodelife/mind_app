// To parse this JSON data, do
//
//     final updateUserDetailsRequestModel = updateUserDetailsRequestModelFromJson(jsonString);

import 'dart:convert';

UpdateUserDetailsRequestModel updateUserDetailsRequestModelFromJson(
        String str) =>
    UpdateUserDetailsRequestModel.fromJson(json.decode(str));

String updateUserDetailsRequestModelToJson(
        UpdateUserDetailsRequestModel data) =>
    json.encode(data.toJson());

class UpdateUserDetailsRequestModel {
  UpdateUserDetailsRequestModel({
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
    this.profileImage,
  });

  final String firstName;
  final String lastName;
  final String email;
  final String phoneNumber;
  final dynamic profileImage;

  factory UpdateUserDetailsRequestModel.fromJson(Map<String, dynamic> json) =>
      UpdateUserDetailsRequestModel(
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        profileImage: json["profile_image"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "profile_image": profileImage,
      };
}
