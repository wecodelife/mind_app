// To parse this JSON data, do
//
//     final getResouceTypeResponse = getResouceTypeResponseFromJson(jsonString);

import 'dart:convert';

GetResouceTypeResponse getResouceTypeResponseFromJson(String str) =>
    GetResouceTypeResponse.fromJson(json.decode(str));

String getResouceTypeResponseToJson(GetResouceTypeResponse data) =>
    json.encode(data.toJson());

class GetResouceTypeResponse {
  GetResouceTypeResponse(
      {this.status, this.message, this.data, this.error, this.errorMessage});

  int status;
  String message;
  String error;
  String errorMessage;
  List<Datum> data;

  factory GetResouceTypeResponse.fromJson(Map<String, dynamic> json) =>
      GetResouceTypeResponse(
        status: json["status"],
        message: json["message"],
        error: json["error"],
        errorMessage: json["error_message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "error": error,
        "error_message": errorMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.name,
  });

  int id;
  bool isActive;
  DateTime createdAt;
  DateTime updatedAt;
  String name;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        isActive: json["is_active"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "is_active": isActive,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "name": name,
      };
}
