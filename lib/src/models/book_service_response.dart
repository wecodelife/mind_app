// To parse this JSON data, do
//
//     final bookServiceResponse = bookServiceResponseFromJson(jsonString);

import 'dart:convert';

BookServiceResponse bookServiceResponseFromJson(String str) =>
    BookServiceResponse.fromJson(json.decode(str));

String bookServiceResponseToJson(BookServiceResponse data) =>
    json.encode(data.toJson());

class BookServiceResponse {
  BookServiceResponse(
      {this.status, this.message, this.data, this.errorMessage});

  final int status;
  final String message;
  final String errorMessage;
  final Data data;

  factory BookServiceResponse.fromJson(Map<String, dynamic> json) =>
      BookServiceResponse(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        errorMessage: json["error"] == null ? null : json["error"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "error": errorMessage == null ? null : errorMessage,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.firstName,
    this.lastName,
    this.currentStatus,
    this.whatsappNumber,
    this.bookingPrice,
    this.bookingDate,
    this.user,
    this.service,
  });

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String firstName;
  final String lastName;
  final String currentStatus;
  final String whatsappNumber;
  final String bookingPrice;
  final DateTime bookingDate;
  final int user;
  final List<int> service;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        currentStatus:
            json["current_status"] == null ? null : json["current_status"],
        whatsappNumber:
            json["whatsapp_number"] == null ? null : json["whatsapp_number"],
        bookingPrice:
            json["booking_price"] == null ? null : json["booking_price"],
        bookingDate: json["booking_date"] == null
            ? null
            : DateTime.parse(json["booking_date"]),
        user: json["user"] == null ? null : json["user"],
        service: json["service"] == null
            ? null
            : List<int>.from(json["service"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "current_status": currentStatus == null ? null : currentStatus,
        "whatsapp_number": whatsappNumber == null ? null : whatsappNumber,
        "booking_price": bookingPrice == null ? null : bookingPrice,
        "booking_date":
            bookingDate == null ? null : bookingDate.toIso8601String(),
        "user": user == null ? null : user,
        "service":
            service == null ? null : List<dynamic>.from(service.map((x) => x)),
      };
}
