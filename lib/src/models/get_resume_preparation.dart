// To parse this JSON data, do
//
//     final getResumePreparationResponse = getResumePreparationResponseFromJson(jsonString);

import 'dart:convert';

GetResumePreparationResponse getResumePreparationResponseFromJson(String str) =>
    GetResumePreparationResponse.fromJson(json.decode(str));

String getResumePreparationResponseToJson(GetResumePreparationResponse data) =>
    json.encode(data.toJson());

class GetResumePreparationResponse {
  GetResumePreparationResponse(
      {this.status, this.message, this.data, this.errorMessage});

  int status;
  String message;
  String errorMessage;
  List<Datum> data;

  factory GetResumePreparationResponse.fromJson(Map<String, dynamic> json) =>
      GetResumePreparationResponse(
        status: json["status"],
        message: json["message"],
        errorMessage: json["error_message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "error": errorMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.description,
    this.videoUrl,
    this.resumePreparationType,
  });

  int id;
  bool isActive;
  DateTime createdAt;
  DateTime updatedAt;
  String title;
  String description;
  String videoUrl;
  int resumePreparationType;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        isActive: json["is_active"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        title: json["title"],
        description: json["description"],
        videoUrl: json["video_url"],
        resumePreparationType: json["resume_preparation_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "is_active": isActive,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "title": title,
        "description": description,
        "video_url": videoUrl,
        "resume_preparation_type": resumePreparationType,
      };
}
