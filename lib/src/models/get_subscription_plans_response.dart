// // To parse this JSON data, do
// //
// //     final getSubscriptionPlansResponse = getSubscriptionPlansResponseFromJson(jsonString);
//
// import 'dart:convert';
//
// GetSubscriptionPlansResponse getSubscriptionPlansResponseFromJson(String str) => GetSubscriptionPlansResponse.fromJson(json.decode(str));
//
// String getSubscriptionPlansResponseToJson(GetSubscriptionPlansResponse data) => json.encode(data.toJson());
//
// class GetSubscriptionPlansResponse {
//   GetSubscriptionPlansResponse({
//     this.status,
//     this.message,
//     this.data,
//   });
//
//   final int status;
//   final String message;
//   final List<Datum> data;
//
//   factory GetSubscriptionPlansResponse.fromJson(Map<String, dynamic> json) => GetSubscriptionPlansResponse(
//     status: json["status"] == null ? null : json["status"],
//     message: json["message"] == null ? null : json["message"],
//     data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "status": status == null ? null : status,
//     "message": message == null ? null : message,
//     "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
//   };
// }
//
// class Datum {
//   Datum({
//     this.id,
//     this.isActive,
//     this.createdAt,
//     this.updatedAt,
//     this.title,
//     this.price,
//     this.offerPrice,
//     this.packageInclude,
//     this.compliments,
//   });
//
//   final int id;
//   final bool isActive;
//   final DateTime createdAt;
//   final DateTime updatedAt;
//   final String title;
//   final int price;
//   final int offerPrice;
//   final List<String> packageInclude;
//   final List<String> compliments;
//
//   factory Datum.fromJson(Map<String, dynamic> json) => Datum(
//     id: json["id"] == null ? null : json["id"],
//     isActive: json["is_active"] == null ? null : json["is_active"],
//     createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
//     updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
//     title: json["title"] == null ? null : json["title"],
//     price: json["price"] == null ? null : json["price"],
//     offerPrice: json["offer_price"] == null ? null : json["offer_price"],
//     packageInclude: json["package_include"] == null ? null : List<String>.from(json["package_include"].map((x) => x)),
//     compliments: json["compliments"] == null ? null : List<String>.from(json["compliments"].map((x) => x)),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id": id == null ? null : id,
//     "is_active": isActive == null ? null : isActive,
//     "created_at": createdAt == null ? null : createdAt.toIso8601String(),
//     "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
//     "title": title == null ? null : title,
//     "price": price == null ? null : price,
//     "offer_price": offerPrice == null ? null : offerPrice,
//     "package_include": packageInclude == null ? null : List<dynamic>.from(packageInclude.map((x) => x)),
//     "compliments": compliments == null ? null : List<dynamic>.from(compliments.map((x) => x)),
//   };
// }


// To parse this JSON data, do
//
//     final getSubscriptionPlansResponse = getSubscriptionPlansResponseFromJson(jsonString);

import 'dart:convert';

GetSubscriptionPlansResponse getSubscriptionPlansResponseFromJson(String str) => GetSubscriptionPlansResponse.fromJson(json.decode(str));

String getSubscriptionPlansResponseToJson(GetSubscriptionPlansResponse data) => json.encode(data.toJson());

class GetSubscriptionPlansResponse {
  GetSubscriptionPlansResponse({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final List<GetSubscriptionPlansResponseDatum> data;

  factory GetSubscriptionPlansResponse.fromJson(Map<String, dynamic> json) => GetSubscriptionPlansResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : List<GetSubscriptionPlansResponseDatum>.from(json["data"].map((x) => GetSubscriptionPlansResponseDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class GetSubscriptionPlansResponseDatum {
  GetSubscriptionPlansResponseDatum({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.price,
    this.offerPrice,
    this.packageInclude,
    this.compliments,
  });

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String title;
  final int price;
  final int offerPrice;
  final PackageInclude packageInclude;
  final Compliments compliments;

  factory GetSubscriptionPlansResponseDatum.fromJson(Map<String, dynamic> json) => GetSubscriptionPlansResponseDatum(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    title: json["title"] == null ? null : json["title"],
    price: json["price"] == null ? null : json["price"],
    offerPrice: json["offer_price"] == null ? null : json["offer_price"],
    packageInclude: json["package_include"] == null ? null : PackageInclude.fromJson(json["package_include"]),
    compliments: json["compliments"] == null ? null : Compliments.fromJson(json["compliments"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "title": title == null ? null : title,
    "price": price == null ? null : price,
    "offer_price": offerPrice == null ? null : offerPrice,
    "package_include": packageInclude == null ? null : packageInclude.toJson(),
    "compliments": compliments == null ? null : compliments.toJson(),
  };
}

class Compliments {
  Compliments({
    this.data,
    this.notes,
  });

  final List<ComplimentsDatum> data;
  final List<String> notes;

  factory Compliments.fromJson(Map<String, dynamic> json) => Compliments(
    data: json["data"] == null ? null : List<ComplimentsDatum>.from(json["data"].map((x) => ComplimentsDatum.fromJson(x))),
    notes: json["notes"] == null ? null : List<String>.from(json["notes"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    "notes": notes == null ? null : List<dynamic>.from(notes.map((x) => x)),
  };
}

class ComplimentsDatum {
  ComplimentsDatum({
    this.title,
    this.details,
  });

  final String title;
  final List<String> details;

  factory ComplimentsDatum.fromJson(Map<String, dynamic> json) => ComplimentsDatum(
    title: json["title"] == null ? null : json["title"],
    details: json["details"] == null ? null : List<String>.from(json["details"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "title": title == null ? null : title,
    "details": details == null ? null : List<dynamic>.from(details.map((x) => x)),
  };
}

class PackageInclude {
  PackageInclude({
    this.data,
    this.description,
  });

  final List<ComplimentsDatum> data;
  final String description;

  factory PackageInclude.fromJson(Map<String, dynamic> json) => PackageInclude(
    data: json["data"] == null ? null : List<ComplimentsDatum>.from(json["data"].map((x) => ComplimentsDatum.fromJson(x))),
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    "description": description == null ? null : description,
  };
}
