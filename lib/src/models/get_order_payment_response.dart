// To parse this JSON data, do
//
//     final getOrderPaymentResponse = getOrderPaymentResponseFromJson(jsonString);

import 'dart:convert';

GetOrderPaymentResponse getOrderPaymentResponseFromJson(String str) => GetOrderPaymentResponse.fromJson(json.decode(str));

String getOrderPaymentResponseToJson(GetOrderPaymentResponse data) => json.encode(data.toJson());

class GetOrderPaymentResponse {
  GetOrderPaymentResponse({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final Data data;

  factory GetOrderPaymentResponse.fromJson(Map<String, dynamic> json) => GetOrderPaymentResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.orderStatus,
    this.orderAmount,
    this.status,
    this.txStatus,
    this.txTime,
    this.txMsg,
    this.referenceId,
    this.paymentMode,
    this.orderCurrency,
  });

  final String orderStatus;
  final String orderAmount;
  final String status;
  final String txStatus;
  final DateTime txTime;
  final String txMsg;
  final String referenceId;
  final String paymentMode;
  final String orderCurrency;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    orderStatus: json["orderStatus"] == null ? null : json["orderStatus"],
    orderAmount: json["orderAmount"] == null ? null : json["orderAmount"],
    status: json["status"] == null ? null : json["status"],
    txStatus: json["txStatus"] == null ? null : json["txStatus"],
    txTime: json["txTime"] == null ? null : DateTime.parse(json["txTime"]),
    txMsg: json["txMsg"] == null ? null : json["txMsg"],
    referenceId: json["referenceId"] == null ? null : json["referenceId"],
    paymentMode: json["paymentMode"] == null ? null : json["paymentMode"],
    orderCurrency: json["orderCurrency"] == null ? null : json["orderCurrency"],
  );

  Map<String, dynamic> toJson() => {
    "orderStatus": orderStatus == null ? null : orderStatus,
    "orderAmount": orderAmount == null ? null : orderAmount,
    "status": status == null ? null : status,
    "txStatus": txStatus == null ? null : txStatus,
    "txTime": txTime == null ? null : txTime.toIso8601String(),
    "txMsg": txMsg == null ? null : txMsg,
    "referenceId": referenceId == null ? null : referenceId,
    "paymentMode": paymentMode == null ? null : paymentMode,
    "orderCurrency": orderCurrency == null ? null : orderCurrency,
  };
}
