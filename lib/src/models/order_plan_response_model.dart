// To parse this JSON data, do
//
//     final orderPlanResponse = orderPlanResponseFromJson(jsonString);

import 'dart:convert';

OrderPlanResponse orderPlanResponseFromJson(String str) => OrderPlanResponse.fromJson(json.decode(str));

String orderPlanResponseToJson(OrderPlanResponse data) => json.encode(data.toJson());

class OrderPlanResponse {
  OrderPlanResponse({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final Data data;

  factory OrderPlanResponse.fromJson(Map<String, dynamic> json) => OrderPlanResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.status,
    this.paymentLink,
    this.orderId,
  });

  final String status;
  final String paymentLink;
  final String orderId;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    status: json["status"] == null ? null : json["status"],
    paymentLink: json["paymentLink"] == null ? null : json["paymentLink"],
    orderId: json["order_id"] == null ? null : json["order_id"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "paymentLink": paymentLink == null ? null : paymentLink,
    "order_id": orderId == null ? null : orderId,
  };
}
