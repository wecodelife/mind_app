// To parse this JSON data, do
//
//     final getResultResponse = getResultResponseFromJson(jsonString);

import 'dart:convert';

GetResultResponse getResultResponseFromJson(String str) =>
    GetResultResponse.fromJson(json.decode(str));

String getResultResponseToJson(GetResultResponse data) =>
    json.encode(data.toJson());

class GetResultResponse {
  GetResultResponse({this.status, this.message, this.data, this.errorMessage});

  int status;
  String message;
  Data data;
  String errorMessage;

  factory GetResultResponse.fromJson(Map<String, dynamic> json) =>
      GetResultResponse(
        status: json["status"],
        message: json["message"],
        errorMessage: json["error_message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "error": errorMessage,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.qaCount,
    this.answerCount,
    this.questionAnswersMarked,
    this.totalMarks,
    this.marksScored,
    this.grade,
  });

  int qaCount;
  int answerCount;
  List<QuestionAnswersMarked> questionAnswersMarked;
  int totalMarks;
  int marksScored;
  String grade;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        qaCount: json["qa_count"],
        answerCount: json["answer_count"],
        questionAnswersMarked: List<QuestionAnswersMarked>.from(
            json["question_answers_marked"]
                .map((x) => QuestionAnswersMarked.fromJson(x))),
        totalMarks: json["total_marks"],
        marksScored: json["marks_scored"],
        grade: json["grade"],
      );

  Map<String, dynamic> toJson() => {
        "qa_count": qaCount,
        "answer_count": answerCount,
        "question_answers_marked":
            List<dynamic>.from(questionAnswersMarked.map((x) => x.toJson())),
        "total_marks": totalMarks,
        "marks_scored": marksScored,
        "grade": grade,
      };
}

class QuestionAnswersMarked {
  QuestionAnswersMarked({
    this.question,
    this.user,
    this.answer,
    this.examId,
    this.isAnswer,
    this.questionName,
    this.optionName,
    this.marks,
  });

  int question;
  int user;
  int answer;
  ExamId examId;
  bool isAnswer;
  String questionName;
  String optionName;
  int marks;

  factory QuestionAnswersMarked.fromJson(Map<String, dynamic> json) =>
      QuestionAnswersMarked(
        question: json["question"],
        user: json["user"],
        answer: json["answer"],
        examId: examIdValues.map[json["exam_id"]],
        isAnswer: json["is_answer"],
        questionName: json["question_name"],
        optionName: json["option_name"],
        marks: json["marks"],
      );

  Map<String, dynamic> toJson() => {
        "question": question,
        "user": user,
        "answer": answer,
        "exam_id": examIdValues.reverse[examId],
        "is_answer": isAnswer,
        "question_name": questionName,
        "option_name": optionName,
        "marks": marks,
      };
}

enum ExamId { THE_4_GJ1_ZZ7_K_V5_G_X0_W1_F_TWJ_UYNGS_OQ_E34 }

final examIdValues = EnumValues({
  "4Gj1ZZ7kV5gX0w1FTwjUYNGSOqE34":
      ExamId.THE_4_GJ1_ZZ7_K_V5_G_X0_W1_F_TWJ_UYNGS_OQ_E34
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
