// To parse this JSON data, do
//
//     final updateUserDetailsResponseModel = updateUserDetailsResponseModelFromJson(jsonString);

import 'dart:convert';

UpdateUserDetailsResponseModel updateUserDetailsResponseModelFromJson(
        String str) =>
    UpdateUserDetailsResponseModel.fromJson(json.decode(str));

String updateUserDetailsResponseModelToJson(
        UpdateUserDetailsResponseModel data) =>
    json.encode(data.toJson());

class UpdateUserDetailsResponseModel {
  UpdateUserDetailsResponseModel(
      {this.id,
      this.lastLogin,
      this.isSuperuser,
      this.firstName,
      this.lastName,
      this.isStaff,
      this.isActive,
      this.dateJoined,
      this.uid,
      this.email,
      this.phoneNumber,
      this.profileImage,
      this.role,
      this.status,
      this.groups,
      this.errorMessage,
      this.userPermissions,
      this.profileImageUrl});

  final int id;
  final dynamic lastLogin;

  final bool isSuperuser;
  final String firstName;
  final String lastName;
  final bool isStaff;
  final bool isActive;
  final String errorMessage;
  final DateTime dateJoined;
  final String uid;
  final String email;
  final int status;
  final String phoneNumber;
  final dynamic profileImage;
  final dynamic profileImageUrl;
  final int role;
  final List<dynamic> groups;
  final List<dynamic> userPermissions;

  factory UpdateUserDetailsResponseModel.fromJson(Map<String, dynamic> json) =>
      UpdateUserDetailsResponseModel(
        id: json["id"] == null ? null : json["id"],
        lastLogin: json["last_login"],
        isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        isStaff: json["is_staff"] == null ? null : json["is_staff"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        dateJoined: json["date_joined"] == null
            ? null
            : DateTime.parse(json["date_joined"]),
        uid: json["uid"] == null ? null : json["uid"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        status: json["status"] == null ? null : json["status"],
        profileImage: json["profile_image"],
        profileImageUrl: json["profile_image_url"],
        errorMessage:
            json["error_message"] == null ? null : json["error_message"],
        role: json["role"] == null ? null : json["role"],
        groups: json["groups"] == null
            ? null
            : List<dynamic>.from(json["groups"].map((x) => x)),
        userPermissions: json["user_permissions"] == null
            ? null
            : List<dynamic>.from(json["user_permissions"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "last_login": lastLogin,
        "is_superuser": isSuperuser == null ? null : isSuperuser,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "is_staff": isStaff == null ? null : isStaff,
        "is_active": isActive == null ? null : isActive,
        "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
        "uid": uid == null ? null : uid,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "profile_image": profileImage,
        "profile_img_url": profileImageUrl,
        "role": role == null ? null : role,
        "status": status == null ? null : status,
        "error_message": errorMessage == null ? null : errorMessage,
        "groups":
            groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
        "user_permissions": userPermissions == null
            ? null
            : List<dynamic>.from(userPermissions.map((x) => x)),
      };
}
