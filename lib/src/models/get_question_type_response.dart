// To parse this JSON data, do
//
//     final getQuestionTypeResponse = getQuestionTypeResponseFromJson(jsonString);

import 'dart:convert';

GetQuestionTypeResponse getQuestionTypeResponseFromJson(String str) =>
    GetQuestionTypeResponse.fromJson(json.decode(str));

String getQuestionTypeResponseToJson(GetQuestionTypeResponse data) =>
    json.encode(data.toJson());

class GetQuestionTypeResponse {
  GetQuestionTypeResponse({
    this.status,
    this.message,
    this.errorMessage,
    this.data,
  });

  final int status;
  final String message;
  final String errorMessage;
  final List<Datum> data;

  factory GetQuestionTypeResponse.fromJson(Map<String, dynamic> json) =>
      GetQuestionTypeResponse(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        errorMessage:
            json["error_message"] == null ? null : json["error_message"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "error_message": errorMessage == null ? null : errorMessage,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.videoUrl,
    this.title,
    this.category,
  });

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final String videoUrl;
  final String title;
  final int category;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        videoUrl: json["video_url"] == null ? null : json["video_url"],
        title: json["title"] == null ? null : json["title"],
        category: json["category"] == null ? null : json["category"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "video_url": videoUrl == null ? null : videoUrl,
        "title": title == null ? null : title,
        "category": category == null ? null : category,
      };
}
