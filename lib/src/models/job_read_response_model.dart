// To parse this JSON data, do
//
//     final jobReadResponseModel = jobReadResponseModelFromJson(jsonString);

import 'dart:convert';

JobReadResponseModel jobReadResponseModelFromJson(String str) =>
    JobReadResponseModel.fromJson(json.decode(str));

String jobReadResponseModelToJson(JobReadResponseModel data) =>
    json.encode(data.toJson());

class JobReadResponseModel {
  JobReadResponseModel(
      {this.count, this.next, this.previous, this.results, this.errorMessage});

  final int count;
  final String errorMessage;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;

  factory JobReadResponseModel.fromJson(Map<String, dynamic> json) =>
      JobReadResponseModel(
        count: json["count"] == null ? null : json["count"],
        next: json["next"],
        errorMessage: json["error_message"],
        previous: json["previous"],
        results: json["results"] == null
            ? null
            : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "next": next,
        "errorMessage": errorMessage,
        "previous": previous,
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Result {
  Result({
    this.id,
    this.imageUrl,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.description,
    this.postedOn,
    this.company,
    this.experience,
    this.endDate,
    this.applicationUrl,
    this.jobType,
    this.involvement,
    this.image,
    this.jobCategory,
    this.phoneNumber,
  });

  final int id;
  final String imageUrl;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String title;
  final String description;
  final DateTime postedOn;
  final String company;
  final dynamic experience;
  final dynamic endDate;
  final String applicationUrl;
  final int jobType;
  final String phoneNumber;
  final int involvement;
  final String image;
  final int jobCategory;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"] == null ? null : json["id"],
        imageUrl: json["image_url"] == null ? null : json["image_url"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        postedOn: json["posted_on"] == null
            ? null
            : DateTime.parse(json["posted_on"]),
        company: json["company"] == null ? null : json["company"],
        experience: json["experience"],
        endDate: json["end_date"],
        applicationUrl:
            json["application_url"] == null ? null : json["application_url"],
        jobType: json["job_type"] == null ? null : json["job_type"],
        involvement: json["involvement"] == null ? null : json["involvement"],
        image: json["image"] == null ? null : json["image"],
        jobCategory: json["job_category"] == null ? null : json["job_category"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "image_url": imageUrl == null ? null : imageUrl,
        "is_active": isActive == null ? null : isActive,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "posted_on": postedOn == null
            ? null
            : "${postedOn.year.toString().padLeft(4, '0')}-${postedOn.month.toString().padLeft(2, '0')}-${postedOn.day.toString().padLeft(2, '0')}",
        "company": company == null ? null : company,
        "experience": experience,
        "end_date": endDate,
        "application_url": applicationUrl == null ? null : applicationUrl,
        "job_type": jobType == null ? null : jobType,
        "involvement": involvement == null ? null : involvement,
        "image": image == null ? null : image,
        "job_category": jobCategory == null ? null : jobCategory,
      };
}
