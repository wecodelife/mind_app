// To parse this JSON data, do
//
//     final userSubscriptionRequest = userSubscriptionRequestFromJson(jsonString);

import 'dart:convert';

UserSubscriptionRequest userSubscriptionRequestFromJson(String str) => UserSubscriptionRequest.fromJson(json.decode(str));

String userSubscriptionRequestToJson(UserSubscriptionRequest data) => json.encode(data.toJson());

class UserSubscriptionRequest {
  UserSubscriptionRequest({
    this.isActive,
    this.orderId,
    this.status,
    this.compliments,
    this.subscription,
    this.user,
  });

  final bool isActive;
  final String orderId;
  final String status;
  final List<String> compliments;
  final int subscription;
  final int user;

  factory UserSubscriptionRequest.fromJson(Map<String, dynamic> json) => UserSubscriptionRequest(
    isActive: json["is_active"] == null ? null : json["is_active"],
    orderId: json["order_id"] == null ? null : json["order_id"],
    status: json["status"] == null ? null : json["status"],
    compliments: json["compliments"] == null ? null : List<String>.from(json["compliments"].map((x) => x)),
    subscription: json["subscription"] == null ? null : json["subscription"],
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "is_active": isActive == null ? null : isActive,
    "order_id": orderId == null ? null : orderId,
    "status": status == null ? null : status,
    "compliments": compliments == null ? null : List<dynamic>.from(compliments.map((x) => x)),
    "subscription": subscription == null ? null : subscription,
    "user": user == null ? null : user,
  };
}
