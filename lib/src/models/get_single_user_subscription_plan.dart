// To parse this JSON data, do
//
//     final getSingleUserSubscriptionPlan = getSingleUserSubscriptionPlanFromJson(jsonString);

import 'dart:convert';

GetSingleUserSubscriptionPlan getSingleUserSubscriptionPlanFromJson(String str) => GetSingleUserSubscriptionPlan.fromJson(json.decode(str));

String getSingleUserSubscriptionPlanToJson(GetSingleUserSubscriptionPlan data) => json.encode(data.toJson());

class GetSingleUserSubscriptionPlan {
  GetSingleUserSubscriptionPlan({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.orderId,
    this.status,
    this.compliments,
    this.subscription,
    this.user,
  });

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String orderId;
  final String status;
  final List<String> compliments;
  final int subscription;
  final int user;

  factory GetSingleUserSubscriptionPlan.fromJson(Map<String, dynamic> json) => GetSingleUserSubscriptionPlan(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    orderId: json["order_id"] == null ? null : json["order_id"],
    status: json["status"] == null ? null : json["status"],
    compliments: json["compliments"] == null ? null : List<String>.from(json["compliments"].map((x) => x)),
    subscription: json["subscription"] == null ? null : json["subscription"],
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "order_id": orderId == null ? null : orderId,
    "status": status == null ? null : status,
    "compliments": compliments == null ? null : List<dynamic>.from(compliments.map((x) => x)),
    "subscription": subscription == null ? null : subscription,
    "user": user == null ? null : user,
  };
}
