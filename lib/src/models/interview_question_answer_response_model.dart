// To parse this JSON data, do
//
//     final interviewQuestionAnswerResponseModel = interviewQuestionAnswerResponseModelFromJson(jsonString);

import 'dart:convert';

InterviewQuestionAnswerResponseModel
    interviewQuestionAnswerResponseModelFromJson(String str) =>
        InterviewQuestionAnswerResponseModel.fromJson(json.decode(str));

String interviewQuestionAnswerResponseModelToJson(
        InterviewQuestionAnswerResponseModel data) =>
    json.encode(data.toJson());

class InterviewQuestionAnswerResponseModel {
  InterviewQuestionAnswerResponseModel({this.data, this.errorMessage});

  List<Datum> data;
  String errorMessage;

  factory InterviewQuestionAnswerResponseModel.fromJson(
          Map<String, dynamic> json) =>
      InterviewQuestionAnswerResponseModel(
        errorMessage: json["error_message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "error_message": errorMessage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.qa,
  });

  int id;
  bool isActive;
  DateTime createdAt;
  DateTime updatedAt;
  String name;
  Qa qa;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        isActive: json["is_active"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        name: json["name"],
        qa: Qa.fromJson(json["qa"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "is_active": isActive,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "name": name,
        "qa": qa.toJson(),
      };
}

class Qa {
  Qa({
    this.questions,
  });

  List<Question> questions;

  factory Qa.fromJson(Map<String, dynamic> json) => Qa(
        questions: List<Question>.from(
            json["questions"].map((x) => Question.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "questions": List<dynamic>.from(questions.map((x) => x.toJson())),
      };
}

class Question {
  Question({
    this.answers,
    this.question,
  });

  List<AnswerElement> answers;
  String question;

  factory Question.fromJson(Map<String, dynamic> json) => Question(
        answers: List<AnswerElement>.from(
            json["answers"].map((x) => AnswerElement.fromJson(x))),
        question: json["question"],
      );

  Map<String, dynamic> toJson() => {
        "answers": List<dynamic>.from(answers.map((x) => x.toJson())),
        "question": question,
      };
}

class AnswerElement {
  AnswerElement({
    this.answer,
  });

  String answer;

  factory AnswerElement.fromJson(Map<String, dynamic> json) => AnswerElement(
        answer: json["answer"],
      );

  Map<String, dynamic> toJson() => {
        "answer": answer,
      };
}

// enum AnswerEnum { HAI_TEST_ANSWER }
//
// final answerEnumValues =
//     EnumValues({"hai test answer": AnswerEnum.HAI_TEST_ANSWER});
//
// class EnumValues<T> {
//   Map<String, T> map;
//   Map<T, String> reverseMap;
//
//   EnumValues(this.map);
//
//   Map<T, String> get reverse {
//     if (reverseMap == null) {
//       reverseMap = map.map((k, v) => new MapEntry(v, k));
//     }
//     return reverseMap;
//   }
// }
