// To parse this JSON data, do
//
//     final getServicesResponse = getServicesResponseFromJson(jsonString);

import 'dart:convert';

GetServicesResponse getServicesResponseFromJson(String str) =>
    GetServicesResponse.fromJson(json.decode(str));

String getServicesResponseToJson(GetServicesResponse data) =>
    json.encode(data.toJson());

class GetServicesResponse {
  GetServicesResponse(
      {this.status, this.message, this.data, this.errorMessage});

  final int status;
  final String message;
  final String errorMessage;
  final List<Datum> data;

  factory GetServicesResponse.fromJson(Map<String, dynamic> json) =>
      GetServicesResponse(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        errorMessage:
            json["error_message"] == null ? null : json["error_message"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "error_message": errorMessage == null ? null : errorMessage,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.imageUrl,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.description,
    this.plan,
    this.content,
    this.price,
    this.offerPrice,
    this.image,
  });

  final int id;
  final dynamic imageUrl;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final String description;
  final String plan;
  final Content content;
  final String price;
  final String offerPrice;
  final dynamic image;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        imageUrl: json["image_url"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        plan: json["plan"] == null ? null : json["plan"],
        content:
            json["content"] == null ? null : Content.fromJson(json["content"]),
        price: json["price"] == null ? null : json["price"],
        offerPrice: json["offer_price"] == null ? null : json["offer_price"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "image_url": imageUrl,
        "is_active": isActive == null ? null : isActive,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "plan": plan == null ? null : plan,
        "content": content == null ? null : content.toJson(),
        "price": price == null ? null : price,
        "offer_price": offerPrice == null ? null : offerPrice,
        "image": image,
      };
}

class Content {
  Content({
    this.data,
  });

  final List<Map<String, String>> data;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        data: json["data"] == null
            ? null
            : List<Map<String, String>>.from(json["data"].map((x) =>
                Map.from(x).map((k, v) => MapEntry<String, String>(k, v)))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) =>
                Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))),
      };
}
