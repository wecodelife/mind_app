// To parse this JSON data, do
//
//     final updateAnswerResponse = updateAnswerResponseFromJson(jsonString);

import 'dart:convert';

UpdateAnswerResponse updateAnswerResponseFromJson(String str) =>
    UpdateAnswerResponse.fromJson(json.decode(str));

String updateAnswerResponseToJson(UpdateAnswerResponse data) =>
    json.encode(data.toJson());

class UpdateAnswerResponse {
  UpdateAnswerResponse({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final Data data;

  factory UpdateAnswerResponse.fromJson(Map<String, dynamic> json) =>
      UpdateAnswerResponse(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.question,
    this.user,
    this.answer,
  });

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int question;
  final int user;
  final int answer;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        question: json["question"] == null ? null : json["question"],
        user: json["user"] == null ? null : json["user"],
        answer: json["answer"] == null ? null : json["answer"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "question": question == null ? null : question,
        "user": user == null ? null : user,
        "answer": answer == null ? null : answer,
      };
}
