// To parse this JSON data, do
//
//     final bookServiceRequest = bookServiceRequestFromJson(jsonString);

import 'dart:convert';

BookServiceRequest bookServiceRequestFromJson(String str) =>
    BookServiceRequest.fromJson(json.decode(str));

String bookServiceRequestToJson(BookServiceRequest data) =>
    json.encode(data.toJson());

class BookServiceRequest {
  BookServiceRequest({
    this.firstName,
    this.lastName,
    this.currentStatus,
    this.whatsappNumber,
    this.bookingPrice,
    this.bookingDate,
    this.service,
  });

  final String firstName;
  final String lastName;
  final String currentStatus;
  final String whatsappNumber;
  final String bookingPrice;
  final DateTime bookingDate;
  final List<int> service;

  factory BookServiceRequest.fromJson(Map<String, dynamic> json) =>
      BookServiceRequest(
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        currentStatus:
            json["current_status"] == null ? null : json["current_status"],
        whatsappNumber:
            json["whatsapp_number"] == null ? null : json["whatsapp_number"],
        bookingPrice:
            json["booking_price"] == null ? null : json["booking_price"],
        bookingDate: json["booking_date"] == null
            ? null
            : DateTime.parse(json["booking_date"]),
        service: json["service"] == null
            ? null
            : List<int>.from(json["service"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "current_status": currentStatus == null ? null : currentStatus,
        "whatsapp_number": whatsappNumber == null ? null : whatsappNumber,
        "booking_price": bookingPrice == null ? null : bookingPrice,
        "booking_date":
            bookingDate == null ? null : bookingDate.toIso8601String(),
        "service":
            service == null ? null : List<dynamic>.from(service.map((x) => x)),
      };
}
