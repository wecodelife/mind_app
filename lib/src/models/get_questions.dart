// To parse this JSON data, do
//
//     final getQuestionResponse = getQuestionResponseFromJson(jsonString);

import 'dart:convert';

GetQuestionResponse getQuestionResponseFromJson(String str) =>
    GetQuestionResponse.fromJson(json.decode(str));

String getQuestionResponseToJson(GetQuestionResponse data) =>
    json.encode(data.toJson());

class GetQuestionResponse {
  GetQuestionResponse(
      {this.status, this.message, this.data, this.errorMessage});

  final int status;
  final String message;
  final Data data;
  final String errorMessage;

  factory GetQuestionResponse.fromJson(Map<String, dynamic> json) =>
      GetQuestionResponse(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        errorMessage:
            json["error_message"] == null ? null : json["error_message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
        "error_message": errorMessage == null ? null : errorMessage,
      };
}

class Data {
  Data({
    this.id,
    this.options,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.question,
    this.questionType,
    this.questionNo,
  });

  final int id;
  final List<Option> options;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String question;
  final int questionType;
  final int questionNo;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        options: json["options"] == null
            ? null
            : List<Option>.from(json["options"].map((x) => Option.fromJson(x))),
        isActive: json["is_active"] == null ? null : json["is_active"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        question: json["question"] == null ? null : json["question"],
        questionType:
            json["question_type"] == null ? null : json["question_type"],
        questionNo: json["question_no"] == null ? null : json["question_no"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "options": options == null
            ? null
            : List<dynamic>.from(options.map((x) => x.toJson())),
        "is_active": isActive == null ? null : isActive,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "question": question == null ? null : question,
        "question_type": questionType == null ? null : questionType,
        "question_no": questionNo == null ? null : questionNo,
      };
}

class Option {
  Option(
      {this.id,
      this.isActive,
      this.createdAt,
      this.updatedAt,
      this.option,
      this.isAnswer,
      this.marks,
      this.question,
      this.answerExplanation});

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String option;
  final bool isAnswer;
  final int marks;
  final int question;
  final String answerExplanation;

  factory Option.fromJson(Map<String, dynamic> json) => Option(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        option: json["option"] == null ? null : json["option"],
        isAnswer: json["is_answer"] == null ? null : json["is_answer"],
        marks: json["marks"] == null ? null : json["marks"],
        question: json["question"] == null ? null : json["question"],
        answerExplanation: json["answer_explanation"] == null
            ? null
            : json["answer_explanation"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "option": option == null ? null : option,
        "is_answer": isAnswer == null ? null : isAnswer,
        "marks": marks == null ? null : marks,
        "question": question == null ? null : question,
      };
}
