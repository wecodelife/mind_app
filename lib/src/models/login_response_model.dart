// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

// import 'dart:convert';
//
// import 'package:dio/dio.dart';
//
// LoginResponse loginResponseFromJson(String str) =>
//     LoginResponse.fromJson(json.decode(str));
//
// String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());
//
// class LoginResponse {
//   LoginResponse(
//       {this.status, this.message, this.data, this.errorMessage, this.dioError});
//
//   final int status;
//   final String message;
//   final Data data;
//   final String errorMessage;
//   final DioError dioError;
//
//   factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
//         status: json["status"] == null ? null : json["status"],
//         message: json["message"] == null ? null : json["message"],
//         data: json["data"] == null ? null : Data.fromJson(json["data"]),
//         errorMessage:
//             json["error_message"] == null ? null : json["error_message"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "status": status == null ? null : status,
//         "message": message == null ? null : message,
//         "data": data == null ? null : data.toJson(),
//         "error_message": errorMessage == null ? null : errorMessage
//       };
// }
//
// class Data {
//   Data({
//     this.token,
//     this.userData,
//   });
//
//   final String token;
//   final UserData userData;
//
//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//         token: json["token"] == null ? null : json["token"],
//         userData: json["user_data"] == null
//             ? null
//             : UserData.fromJson(json["user_data"]),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "token": token == null ? null : token,
//         "user_data": userData == null ? null : userData.toJson(),
//       };
// }
//
// class UserData {
//   UserData({
//     this.id,
//     this.lastLogin,
//     this.isSuperuser,
//     this.firstName,
//     this.lastName,
//     this.isStaff,
//     this.isActive,
//     this.dateJoined,
//     this.uid,
//     this.email,
//     this.password,
//     this.role,
//     this.groups,
//     this.userPermissions,
//     this.profileImage,
//   });
//
//   final int id;
//   final dynamic lastLogin;
//   final bool isSuperuser;
//   final String firstName;
//   final String lastName;
//   final String profileImage;
//   final bool isStaff;
//   final bool isActive;
//   final DateTime dateJoined;
//   final String uid;
//   final dynamic email;
//   final dynamic password;
//   final int role;
//   final List<dynamic> groups;
//   final List<dynamic> userPermissions;
//
//   factory UserData.fromJson(Map<String, dynamic> json) => UserData(
//         id: json["id"] == null ? null : json["id"],
//         lastLogin: json["last_login"],
//         isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
//         firstName: json["first_name"] == null ? null : json["first_name"],
//         lastName: json["last_name"] == null ? null : json["last_name"],
//         isStaff: json["is_staff"] == null ? null : json["is_staff"],
//         isActive: json["is_active"] == null ? null : json["is_active"],
//         dateJoined: json["date_joined"] == null
//             ? null
//             : DateTime.parse(json["date_joined"]),
//         uid: json["uid"] == null ? null : json["uid"],
//         email: json["email"],
//         password: json["password"],
//         profileImage:
//             json["profile_image"] == null ? null : json["profile_image"],
//         role: json["role"] == null ? null : json["role"],
//         groups: json["groups"] == null
//             ? null
//             : List<dynamic>.from(json["groups"].map((x) => x)),
//         userPermissions: json["user_permissions"] == null
//             ? null
//             : List<dynamic>.from(json["user_permissions"].map((x) => x)),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id == null ? null : id,
//         "last_login": lastLogin,
//         "is_superuser": isSuperuser == null ? null : isSuperuser,
//         "first_name": firstName == null ? null : firstName,
//         "last_name": lastName == null ? null : lastName,
//         "is_staff": isStaff == null ? null : isStaff,
//         "is_active": isActive == null ? null : isActive,
//         "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
//         "uid": uid == null ? null : uid,
//         "email": email,
//         "password": password,
//         "role": role == null ? null : role,
//         "groups":
//             groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
//         "user_permissions": userPermissions == null
//             ? null
//             : List<dynamic>.from(userPermissions.map((x) => x)),
//       };
// }


// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final Data data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.token,
    this.userData,
  });

  final String token;
  final UserData userData;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    token: json["token"] == null ? null : json["token"],
    userData: json["user_data"] == null ? null : UserData.fromJson(json["user_data"]),
  );

  Map<String, dynamic> toJson() => {
    "token": token == null ? null : token,
    "user_data": userData == null ? null : userData.toJson(),
  };
}

class UserData {
  UserData({
    this.id,
    this.profileImageUrl,
    this.userSubscriptionPlan,
    this.subscriptionPlanName,
    this.lastLogin,
    this.isSuperuser,
    this.firstName,
    this.lastName,
    this.isStaff,
    this.dateJoined,
    this.uid,
    this.email,
    this.phoneNumber,
    this.profileImage,
    this.isActive,
    this.role,
    this.groups,
    this.userPermissions,
  });

  final int id;
  final dynamic profileImageUrl;
  final int userSubscriptionPlan;
  final String subscriptionPlanName;
  final dynamic lastLogin;
  final bool isSuperuser;
  final String firstName;
  final String lastName;
  final bool isStaff;
  final DateTime dateJoined;
  final String uid;
  final dynamic email;
  final dynamic phoneNumber;
  final dynamic profileImage;
  final bool isActive;
  final int role;
  final List<dynamic> groups;
  final List<dynamic> userPermissions;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
    id: json["id"] == null ? null : json["id"],
    profileImageUrl: json["profile_image_url"],
    userSubscriptionPlan: json["user_subscription_plan"] == null ? null : json["user_subscription_plan"],
    subscriptionPlanName: json["subscription_plan_name"] == null ? null : json["subscription_plan_name"],
    lastLogin: json["last_login"],
    isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    isStaff: json["is_staff"] == null ? null : json["is_staff"],
    dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
    uid: json["uid"] == null ? null : json["uid"],
    email: json["email"],
    phoneNumber: json["phone_number"],
    profileImage: json["profile_image"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    role: json["role"] == null ? null : json["role"],
    groups: json["groups"] == null ? null : List<dynamic>.from(json["groups"].map((x) => x)),
    userPermissions: json["user_permissions"] == null ? null : List<dynamic>.from(json["user_permissions"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "profile_image_url": profileImageUrl,
    "user_subscription_plan": userSubscriptionPlan == null ? null : userSubscriptionPlan,
    "subscription_plan_name": subscriptionPlanName == null ? null : subscriptionPlanName,
    "last_login": lastLogin,
    "is_superuser": isSuperuser == null ? null : isSuperuser,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "is_staff": isStaff == null ? null : isStaff,
    "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
    "uid": uid == null ? null : uid,
    "email": email,
    "phone_number": phoneNumber,
    "profile_image": profileImage,
    "is_active": isActive == null ? null : isActive,
    "role": role == null ? null : role,
    "groups": groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
    "user_permissions": userPermissions == null ? null : List<dynamic>.from(userPermissions.map((x) => x)),
  };
}


