// To parse this JSON data, do
//
//     final userSubscriptionResponse = userSubscriptionResponseFromJson(jsonString);

import 'dart:convert';

UserSubscriptionResponse userSubscriptionResponseFromJson(String str) => UserSubscriptionResponse.fromJson(json.decode(str));

String userSubscriptionResponseToJson(UserSubscriptionResponse data) => json.encode(data.toJson());

class UserSubscriptionResponse {
  UserSubscriptionResponse({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final Data data;

  factory UserSubscriptionResponse.fromJson(Map<String, dynamic> json) => UserSubscriptionResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.orderId,
    this.status,
    this.compliments,
    this.subscription,
    this.user,
  });

  final int id;
  final bool isActive;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String orderId;
  final String status;
  final List<String> compliments;
  final int subscription;
  final int user;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    orderId: json["order_id"] == null ? null : json["order_id"],
    status: json["status"] == null ? null : json["status"],
    compliments: json["compliments"] == null ? null : List<String>.from(json["compliments"].map((x) => x)),
    subscription: json["subscription"] == null ? null : json["subscription"],
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "order_id": orderId == null ? null : orderId,
    "status": status == null ? null : status,
    "compliments": compliments == null ? null : List<dynamic>.from(compliments.map((x) => x)),
    "subscription": subscription == null ? null : subscription,
    "user": user == null ? null : user,
  };
}
