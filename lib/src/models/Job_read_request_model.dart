// To parse this JSON data, do
//
//     final jobReadRequestModel = jobReadRequestModelFromJson(jsonString);

import 'dart:convert';

JobReadRequestModel jobReadRequestModelFromJson(String str) =>
    JobReadRequestModel.fromJson(json.decode(str));

String jobReadRequestModelToJson(JobReadRequestModel data) =>
    json.encode(data.toJson());

class JobReadRequestModel {
  JobReadRequestModel({
    this.category,
  });

  List<int> category;

  factory JobReadRequestModel.fromJson(Map<String, dynamic> json) =>
      JobReadRequestModel(
        category: List<int>.from(json["category"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "category": List<dynamic>.from(category.map((x) => x)),
      };
}
