// To parse this JSON data, do
//
//     final updateAnswerRequest = updateAnswerRequestFromJson(jsonString);

import 'dart:convert';

UpdateAnswerRequest updateAnswerRequestFromJson(String str) =>
    UpdateAnswerRequest.fromJson(json.decode(str));

String updateAnswerRequestToJson(UpdateAnswerRequest data) =>
    json.encode(data.toJson());

class UpdateAnswerRequest {
  UpdateAnswerRequest({
    this.question,
    this.answer,
    this.examId,
  });

  int question;
  int answer;
  String examId;

  factory UpdateAnswerRequest.fromJson(Map<String, dynamic> json) =>
      UpdateAnswerRequest(
        question: json["question"],
        answer: json["answer"],
        examId: json["exam_id"],
      );

  Map<String, dynamic> toJson() => {
        "question": question,
        "answer": answer,
        "exam_id": examId,
      };
}
