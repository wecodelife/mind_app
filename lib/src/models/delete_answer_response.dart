// To parse this JSON data, do
//
//     final deleteAnswerResponse = deleteAnswerResponseFromJson(jsonString);

import 'dart:convert';

DeleteAnswerResponse deleteAnswerResponseFromJson(String str) =>
    DeleteAnswerResponse.fromJson(json.decode(str));

String deleteAnswerResponseToJson(DeleteAnswerResponse data) =>
    json.encode(data.toJson());

class DeleteAnswerResponse {
  DeleteAnswerResponse({
    this.status,
    this.message,
    this.error,
  });

  int status;
  String message;
  String error;

  factory DeleteAnswerResponse.fromJson(Map<String, dynamic> json) =>
      DeleteAnswerResponse(
        status: json["status"],
        message: json["message"],
        error: json["error"],
      );

  Map<String, dynamic> toJson() =>
      {"status": status, "message": message, "error": error};
}
